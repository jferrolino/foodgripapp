//
//  Location.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class MenuEntree, ReferencePhotoData;

@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * latMax;
@property (nonatomic, retain) NSNumber * latMin;
@property (nonatomic, retain) NSNumber * lonMax;
@property (nonatomic, retain) NSNumber * lonMin;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSString * phoneNumber;
@property (nonatomic, retain) NSString * restaurantManager;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSOrderedSet *containPhotos;
@property (nonatomic, retain) NSOrderedSet *offerEntrees;

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context;

@end

@interface Location (CoreDataGeneratedAccessors)

- (void)insertObject:(ReferencePhotoData *)value inContainPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainPhotosAtIndex:(NSUInteger)idx;
- (void)insertContainPhotos:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainPhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainPhotosAtIndex:(NSUInteger)idx withObject:(ReferencePhotoData *)value;
- (void)replaceContainPhotosAtIndexes:(NSIndexSet *)indexes withContainPhotos:(NSArray *)values;
- (void)addContainPhotosObject:(ReferencePhotoData *)value;
- (void)removeContainPhotosObject:(ReferencePhotoData *)value;
- (void)addContainPhotos:(NSOrderedSet *)values;
- (void)removeContainPhotos:(NSOrderedSet *)values;
- (void)insertObject:(MenuEntree *)value inOfferEntreesAtIndex:(NSUInteger)idx;
- (void)removeObjectFromOfferEntreesAtIndex:(NSUInteger)idx;
- (void)insertOfferEntrees:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeOfferEntreesAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInOfferEntreesAtIndex:(NSUInteger)idx withObject:(MenuEntree *)value;
- (void)replaceOfferEntreesAtIndexes:(NSIndexSet *)indexes withOfferEntrees:(NSArray *)values;
- (void)addOfferEntreesObject:(MenuEntree *)value;
- (void)removeOfferEntreesObject:(MenuEntree *)value;
- (void)addOfferEntrees:(NSOrderedSet *)values;
- (void)removeOfferEntrees:(NSOrderedSet *)values;
@end
