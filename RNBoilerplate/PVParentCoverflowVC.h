//
//  PVLocationsCoverflowVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDSyncEngine.h"
#import "SDCoreDataController.h"
#import "iCarousel.h"

@interface PVParentCoverflowVC : UIViewController



@property (nonatomic, strong) iCarousel *carousel;
@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;

-(id)initWithFrame:(CGRect) frame forEntity:(NSString *) entityName;

@end
