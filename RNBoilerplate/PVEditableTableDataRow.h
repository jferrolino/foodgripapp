//
//  PVEditableTableDataRow.h
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditableTableDataRow.h"
#import "TDBadgedCell.h"

@interface PVEditableTableDataRow : EditableTableDataRow

@property (nonatomic, strong) NSArray *arrObjects;


// Adding badge to this cell
@property (nonatomic, strong)    NSString *badgeString;
@property (readonly,  strong)    TDBadgeView *badge;
@property (nonatomic, strong)    UIColor *badgeColor;
@property (nonatomic, strong)    UIColor *badgeTextColor;
@property (nonatomic, strong)    UIColor *badgeColorHighlighted;
@property (nonatomic, assign)    BOOL showShadow;

- (void)applyEditingModeAs:(BOOL)toEdit;

- (void)applyEditingModeAsWithOptions:(BOOL)toEdit;

- (void) applyEditingModeAs:(BOOL)toEdit To:(NSArray *)controls;

- (void) applyNonEditingModeToItems;

@end
