//
//  PVAssetsManTVC.h
//  NewAssetsMan
//
//  Created by jon.ferrolino on 10/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <Parse/Parse.h>
#import "ECSlidingViewController.h"

@interface PVAssetsManTVC : PFQueryTableViewController


- (IBAction)revealMenu:(id)sender;
@end
