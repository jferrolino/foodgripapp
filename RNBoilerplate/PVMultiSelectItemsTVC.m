//
//  PVMultiSelectionItemsTVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/14/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMultiSelectItemsTVC.h"
#import "CRTableViewCell.h"

@interface PVMultiSelectItemsTVC ()

@end

@implementation PVMultiSelectItemsTVC

@synthesize delegate;

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self.tableView setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate: self];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleDone
                                                                  target:self
                                                                  action:@selector(done:)];
    
    [self.navigationItem setRightBarButtonItem:doneButton];
    
    if(!self.selectedMarks){
        self.selectedMarks = [NSMutableArray new];
    }
    
    [super loadRecordsFromCoreData];
}

- (void) done:(id)sender
{
    if([self delegate]){
        
        if(self.selectedMarks && [self.selectedMarks count] > 0){
            [self.delegate caller:sender
                withSelectedItems:self.selectedMarks];
        } else {
            [self.delegate caller:sender
                withSelectedItems:[NSMutableArray new]];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CRTableViewCellIdentifier = @"cellIdentifier";
    
    // init the CRTableViewCell
    CRTableViewCell *cell = (CRTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CRTableViewCellIdentifier];
    
    if (cell == nil) {
        cell = [[CRTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CRTableViewCellIdentifier];
    }
    
    Item *item = [self.dataSource objectAtIndex:[indexPath row]];
    
    //[self alreadyInSelectedMarks:item];

    cell.isSelected = [self isInSelectedMarks:item];
    
    [cell.textLabel setText:[item name]];
    [cell setBackgroundView: [PVGlobals commonCellBackgroundView:cell]];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Item *item = [self.dataSource objectAtIndex:[indexPath row]];
    
    [self alreadyInSelectedMarks:item];
    
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

@end
