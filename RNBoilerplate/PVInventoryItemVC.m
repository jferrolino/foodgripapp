//
//  PVInventoryItemVC.m
//  FoodGrip
//
//  Created by ENG002 on 12/19/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVInventoryItemVC.h"
#import "SDCoreDataController.h"
#import "Item.h"
#import "SDSyncEngine.h"

@interface PVInventoryItemVC ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation PVInventoryItemVC

@synthesize managedObjectContext = _managedObjectContext;
@synthesize invItem = _invItem;
@synthesize entityName = _entityName;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    self.managedObjectContext = [[SDCoreDataController sharedInstance]newManagedObjectContext];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)saveBulk:(NSArray *)invItems
{
    int count = 0;
    for(NSDictionary *item in invItems ){
        
        if(self.managedObjectContext == nil) {
            self.managedObjectContext = [[SDCoreDataController sharedInstance]newManagedObjectContext];
        }
        
        Item *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext];
        
        [newItem setValue:[NSNumber numberWithInt:SDObjectCreated] forKey:@"syncStatus"];
        
        [newItem setValue:[item objectForKey:@"Item Name"] forKey:@"name"];
        //[objItem setValue:[item objectForKey:@"Num"] forKey:@"id"];
        
        //NSNumber *itemNumber = ([item objectForKey:@"Num"] isKindofclass:[NSNumber class])? [item objectForKey:@"Num"] : NSNumber.on
        
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber * itemNumber = [f numberFromString:[item objectForKey:@"Num"]];
        
        [newItem setValue:itemNumber forKey:@"itemNumber"];
        
        NSNumber *amount = [f numberFromString:[item objectForKey:@"Amount"]];
        
        [newItem setValue:amount forKey:@"amount"];
        
        [newItem setValue:[item objectForKey:@"Category"] forKey:@"category"];
        
        NSNumber *cost = [f numberFromString:[item objectForKey:@"Cost"]];
        [newItem setValue:cost forKey:@"cost"];
        
        NSNumber *costPercentage = [f numberFromString:[item objectForKey:@"Cost %"]];
        [newItem setValue:costPercentage forKey:@"costPercentage"];
        
        NSNumber *rank = [f numberFromString:[item objectForKey:@"Rank"]];
        [newItem setValue:rank forKey:@"rank"];
        
        NSNumber *sales = [f numberFromString:[item objectForKey:@"Sales"]];
        [newItem setValue:sales forKey:@"sales"];
        
        NSNumber *numberSold = [f numberFromString:[item objectForKey:@"numberSold"]];
        [newItem setValue:numberSold forKey:@"numberSold"];
        
        if(count > 10) break;
        
        count ++;
        
        
    }
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL saved = [self.managedObjectContext save:&error];
        if (!saved) {
            // do some real error handling
            NSLog(@"Could not save Date due to %@", error);
        }
        [[SDCoreDataController sharedInstance] saveMasterContext];
        [[SDSyncEngine sharedEngine]startSync];
    }];
}


@end
