//
//  PVTouchAwareParallaxPicturesVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/10/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVTouchAwareParallaxPicturesVC.h"
#import "UIView+Glow.h"

#define MENU_ENTREE_DEFAULT_IMAGE @"entree-default.jpg"

@interface PVTouchAwareParallaxPicturesVC ()

@end

@implementation PVTouchAwareParallaxPicturesVC

@synthesize navigationController = _navigationController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self = [self initWithImages:@[[UIImage imageNamed:MENU_ENTREE_DEFAULT_IMAGE]] andContentView:nil];
        // Custom initialization
    }
    return self;
}

- (id)initWithImages:(NSArray *)images andContentViewController:(UIViewController *)viewController
withNavigationController:(UINavigationController *)navigationController
{
    self = [super initWithImages:images andContentView:viewController.view];
    if(self){
        
        self.navigationController = navigationController;
        DDLogVerbose(@"The navigation controller from Parallax view controller %@" , self.navigationController);

    }
    return self;
}

- (id)initWithImages:(NSArray *)images andContentView:(UIView *)contentView
{
    self = [super initWithImages:images andContentView:contentView];
    if(self){
        
        if(self.navigationController){
           
        }
        
        self.view.userInteractionEnabled = YES;
        
        // Reset geometry to identities
        self.view.transform = CGAffineTransformIdentity;
        tx = 0.0f; ty = 0.0f; scale = 1.0f; theta = 0.0f;
        
        // Add gesture recognizer suite
        
        
        //Add gesture recognizer to be used for deletion of conference.
        UILongPressGestureRecognizer *pahGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognizerStateChanged:)];
        
        pahGestureRecognizer.minimumPressDuration = 1.0;
        
        [self->_transparentScroller addGestureRecognizer:pahGestureRecognizer];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setImages:(NSArray *)newImages
{
    _imageViews = [NSMutableArray array];
    
    [super addImages:newImages];
}

@end
