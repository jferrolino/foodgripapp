//  PVGlobals.h
//
//  FoodGrip
//
//  Created by ENG002 on 1/23/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PVEditableTableDataRow.h"

#import "Location.h"
#import "Item.h"
#import "MenuEntree.h"
#import "ReferencePhotoData.h"

@interface PVGlobals : NSObject

+ (id)initialize;
+ (NSArray *) retrieveAllLocations;
+ (void) reloadToGetAllLocations;
+ (NSArray *) retrieveCategories;
+ (void) reloadToGetRecentCategories;

+ (NSString *) formatDateToString:(NSDate *)date;

+ (UIView *) commonCellBackgroundView:(UITableViewCell *)cell;
+ (UIColor *) commonViewBackgroundColor;

+ (UIButton *) menuBarButtonThemeFromTarget:(id)target withAction:(SEL)action;

+ (NSDictionary *)divideEvenlyWithRemainder:(NSDecimalNumber *)numerator
                                         by:(NSDecimalNumber *)denominator
                                forCurrency:(NSString *)currencyCode;

+ (NSString *) formatCurrencyToString:(NSDecimalNumber *)value;

+ (NSString *) retrieveCurrencyCodeBasedOnCurrentLocale;

+ (NSDictionary *) retrieveAppCurrentLocaleInfo;

+ (NSArray * ) retrieveOrderUnits;

+ (NSArray * ) retrieveItemUnits;

+ (NSArray *) customTableColumnHeader:(PVEditableTableDataRow *)container withSize:(CGSize)size andLabels:(NSArray *)labels withCorrespondingWidth:(NSDictionary *)labelkeyWithWidths;

+ (NSDecimalNumber *) numberToDecimalNumber:(NSNumber *) number;

+ (NSString *) convertDecimalDictionaryToString:(NSDictionary *)decimalDictionary;

+ (NSString *) randStringUniqueInDBWithLength:(int)len forEntityName:(NSString *)entityName;

+ (NSString *) generateHashForImageData:(NSData *) imageData;

+ (UIView *) generateGridContentView:(CGRect)bounds withLabels:(NSArray *)labels;

+ (NSArray *) deduplicateImages:(NSArray *)allImages;

# pragma mark - This is for Restaurant Location and Menu entree only
+ (NSArray *) prepareBannerWith:(NSArray *)currentData forView:(UIView *)thisView;

@end
