//
//  Item.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Item.h"
#import "CyclePerInventoryItem.h"
#import "MenuEntree.h"
#import "ReferencePhotoData.h"


@implementation Item

@dynamic category;
@dynamic createdAt;
@dynamic extension;
@dynamic inventoryCount;
@dynamic inventoryPrice;
@dynamic inventoryUnit;
@dynamic itemNumber;
@dynamic name;
@dynamic objectId;
@dynamic orderPrice;
@dynamic orderToInventory;
@dynamic orderUnit;
@dynamic syncStatus;
@dynamic type;
@dynamic updatedAt;
@dynamic containPhotos;
@dynamic inCycles;
@dynamic usedInMenuEntree;

- (id)initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = (Item *)entity;
    if(self){
        if(context){
            [context deleteObject:self];
        }
    }
    return self;
}

- (NSDictionary *)JSONToCreateObjectOnServer
{
    NSString *jsonString = nil;
    
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.orderUnit, @"orderUnit",
                                    self.itemNumber, @"itemNumber",
                                    self.orderPrice, @"orderPrice",
                                    self.inventoryUnit, @"inventoryUnit",
                                    self.inventoryPrice, @"inventoryPrice",
                                    self.inventoryCount, @"inventoryCount",
                                    self.extension,@"extension",
                                    self.category, @"category",
                                    self.orderToInventory, @"orderToInventory",
                                    self.syncStatus, @"syncStatus",nil];
    
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:jsonDictionary
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    if (!jsonData) {
        NSLog(@"Error creating jsonData: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonDictionary;
}


@end
