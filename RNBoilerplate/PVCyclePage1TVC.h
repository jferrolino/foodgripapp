//
//  PVCyclePage1TVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/22/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FKFormModel.h"
#import "Cycle.h"

@protocol CyclePage1Delegate <NSObject>

//- (Cycle *) retrieveModel:(id)sender;
- (void) onFormSubmission:(id)sender withObject:(id)object;

@end

@protocol CyclePage1DataSource<NSObject>

- (NSManagedObjectContext *) useThisManagedObjectContextForPage1;

- (Cycle *) useThisCycleInstanceForPage1;

@end

@interface PVCyclePage1TVC : UITableViewController

@property  (nonatomic, strong)  NSManagedObjectContext *managedObjectContext;

@property (nonatomic, strong) FKFormModel *formModel;
@property (nonatomic, strong) Cycle *cycle;

@property (nonatomic, weak)id<CyclePage1DataSource> dataSource;
@property (nonatomic,weak) id<CyclePage1Delegate> delegate;

@end
