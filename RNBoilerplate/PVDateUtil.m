//
//  PVDateUtil.m
//  FoodGrip
//
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVDateUtil.h"

@implementation PVDateUtil

+ (NSString *)strInStandardDateFormat:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    
    return [dateFormatter stringFromDate:date];
}

+ (NSString *) strFromDateForJSONEncoding:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    return [dateFormatter stringFromDate:date];
}

@end
