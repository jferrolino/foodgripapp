//
//  ReferencePhotoData.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "ReferencePhotoData.h"
#import "Item.h"
#import "Location.h"
#import "MenuEntree.h"


@implementation ReferencePhotoData

@dynamic latMax;
@dynamic latMin;
@dynamic lonMax;
@dynamic lonMin;
@dynamic photoData;
@dynamic photoDigest;
@dynamic bannerPhotoData;
@dynamic forItem;
@dynamic forLocation;
@dynamic forMenuEntree;

@end
