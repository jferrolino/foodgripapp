//
//  PVLocationsCoverflowVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVParentCoverflowVC.h"

@interface PVParentCoverflowVC () <iCarouselDataSource, iCarouselDelegate>

@property (nonatomic) BOOL wrap;
@end

@implementation PVParentCoverflowVC

@synthesize wrap;
@synthesize carousel = _carousel;
@synthesize items;
@synthesize tapGestureRecognizer = _tapGestureRecognizer;
@synthesize panGestureRecognizer = _panGestureRecognizer;


- (void) loadFromCoreDataWithEntity:(NSString *) entityName
{
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:entityName];
        [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        
        self.items = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    }];
}


-(id)initWithFrame:(CGRect) frame forEntity:(NSString *) entityName
{
    if( (self = [super init] ))
    {
        self.managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
        
        //[self loadFromCoreDataWithEntity:entityName];
        self.items = [NSMutableArray array];
        for (int i = 0; i < 1000; i++)
        {
            [items addObject:[NSNumber numberWithInt:i]];
        }
        [self.view setFrame:frame];
        

    }
    return self;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.wrap = YES;
    
    self.carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
    self.carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.carousel.type = iCarouselTypeCoverFlow2;
	self.carousel.dataSource = self;
    self.carousel.contentOffset = CGSizeMake(0.0f, 150.0f);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self.carousel action:@selector(didTap:)];
    tapGesture.delegate = (id <UIGestureRecognizerDelegate>)self;    
    self.tapGestureRecognizer = tapGesture;
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.carousel action:@selector(didPan:)];
    panGesture.delegate = (id<UIGestureRecognizerDelegate>)self;
    
    self.panGestureRecognizer = panGesture;

    [self.view addSubview:self.carousel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [self.items count];
}

- (CATransform3D)carousel:(iCarousel *)_carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform
{
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0f, 0.0f, 1.0f, 0.0f);
    return CATransform3DTranslate(transform, 0.0f, 0.0f, offset * self.carousel.itemWidth);
}

- (CGFloat)carousel:(iCarousel *)_carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return 0.10f;//value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        default:
        {
            return value;
        }
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    return nil;
}


@end
