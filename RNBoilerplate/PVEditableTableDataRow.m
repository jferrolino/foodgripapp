//
//  PVEditableTableDataRow.m
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVEditableTableDataRow.h"
#import "PVEditableTableDataRowItem.h"


@implementation PVEditableTableDataRow

@synthesize badgeString, badge=__badge, badgeColor, badgeTextColor, badgeColorHighlighted, showShadow;

- (void)configureSelf
{
    // Initialization code
    __badge = [[TDBadgeView alloc] initWithFrame:CGRectZero];
    self.badge.parent = self;
    
    [self.contentView addSubview:self.badge];
    [self.badge setNeedsDisplay];
    
    
    /*
    [cell.textLabel setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [cell.textLabel setShadowColor:[UIColor whiteColor]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
     */
    
    //[self setBackgroundColor:[UIColor clearColor]];
    //[self setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    UIView *bgView = [[UIView alloc] initWithFrame:self.frame];
    
    [bgView setBackgroundColor:[UIColor clearColor]];
    [self setBackgroundView:bgView];
    
    
    [self setIndentationWidth:0.0];


}

- (id)initWithCoder:(NSCoder *)decoder
{
    if ((self = [super initWithCoder:decoder]))
    {
        [self configureSelf];
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]))
    {
        [self configureSelf];
    }
    return self;
}

- (void)applyEditingModeAs:(BOOL)toEdit
{
    for(PVEditableTableDataRowItem *i in self.rowItems){
        [i toggleEditingMode:toEdit];
    }
}

- (void)applyEditingModeAsWithOptions:(BOOL)toEdit
{
    for(PVEditableTableDataRowItem *item in self.rowItems){
        if([item toEdit] == NO){
            [item toggleEditingMode:NO];
        } else {
            [item toggleEditingMode:toEdit];
        }
    }
}

- (void) applyNonEditingModeToItems
{
    for(PVEditableTableDataRowItem *item in self.rowItems){
        if([item toEdit] == NO){
            [item toggleEditingMode:NO];
        } else {
            [item toggleEditingMode:YES];
        }
    }
}

- (void) applyEditingModeAs:(BOOL)toEdit To:(NSArray *)controls
{
	if( controls != nil && [controls count] > 0){
		for(NSObject *control in controls){
			if([control isKindOfClass:[PVEditableTableDataRowItem class]]){
				//[self.rowItems mutableCopy];
                //[self.rowItems getObject]
                for(PVEditableTableDataRowItem *item in self.rowItems){
                    if([control isEqual:item]){
                        [item toggleEditingMode:toEdit];
                    }
                }
			}
		}
	}
}

- (void)setRowItems:(NSArray *)RowItems
{
    
	for (UIView *subview in self.contentView.subviews){
		[subview removeFromSuperview];
    }
    
	rowItems = RowItems;
	for (PVEditableTableDataRowItem *rowItem in rowItems){
		rowItem.delegate = self;
		[self.contentView addSubview:rowItem.control];
        
        [rowItem.controlAlt setHidden:YES];
        [self.contentView addSubview:rowItem.controlAlt];
    }
}


//  Overridden methods to accomodate UILabel we've added..
#pragma mark - Business Logic

- (void)adjustItemSizes
{
    
	CGRect			frame = self.contentView.frame;
	int				totalItemWidth = 0;
    
	// First deal with adjusting the width
	for (EditableTableDataRowItem *nextItem in rowItems)
    {
		CGSize size = nextItem.originalBaseSize;
		totalItemWidth += size.width;
    }
	if (!padStartItem && !padEndItem)
    {
		totalItemWidth += self.itemPadding * ([self.rowItems count] - 1) + DEFAULT_MIN_START_PADDING + DEFAULT_MIN_END_PADDING;
    }
	else if (padStartItem && padEndItem)
    {
		totalItemWidth += self.itemPadding * ([self.rowItems count] + 1);
    }
	else if (padStartItem || padEndItem)
    {
		totalItemWidth += self.itemPadding * [self.rowItems count];
		if (!padStartItem)
        {
			totalItemWidth += DEFAULT_MIN_START_PADDING;
        }
		else if (!padEndItem)
        {
			totalItemWidth += DEFAULT_MIN_END_PADDING;
        }
    }
    
	if (totalItemWidth > frame.size.width || self.scaleToFillRow == TRUE)
    {
		for (PVEditableTableDataRowItem *nextItem in rowItems)
        {
			if (nextItem.resizeable == TRUE)
            {
				CGSize size = nextItem.originalBaseSize;
				nextItem.baseSize = CGSizeMake((int)((size.width / totalItemWidth) * frame.size.width), size.height);		// Maintains proportional size of all resizeable items
            }
        }
    }
    
	// Then deal with adjusting the height
	for (EditableTableDataRowItem *nextItem in rowItems)
    {
		if (nextItem.baseSize.height == 0)
        {
			CGSize size = nextItem.originalBaseSize;
			switch (nextItem.itemControlType)
            {
				case ControlTypeTextField:
					size.height = TEXTFIELD_DEFAULT_HEIGHT;
					break;
				case ControlTypePopup:
				case ControlTypeButton:
				case ControlTypeToggleButton:
				default:
					size.height = BUTTON_DEFAULT_HEIGHT;
					break;
            }
			nextItem.baseSize = size;
        }
    }
}

- (void)layoutSubviews
{
	int				start_x = DEFAULT_MIN_START_PADDING;
    
	if (padStartItem)
    {
		start_x = self.itemPadding;
    }
	[super layoutSubviews];
	[self adjustItemSizes];
	for (PVEditableTableDataRowItem *nextItem in rowItems)
    {
		CGRect frame = nextItem.control.frame;
		frame.origin.x = start_x;
		frame.size.width = nextItem.baseSize.width;
		start_x += (frame.size.width + self.itemPadding);
		frame.size.height = nextItem.baseSize.height;
		if (nextItem.baseSize.height < self.frame.size.height)
        {
			frame.origin.y = (self.frame.size.height - nextItem.baseSize.height) / 2;
        }
		else
        {
			frame.origin.y = 0;
			frame.size.height = self.frame.size.height;
        }
		nextItem.control.frame = frame;
        nextItem.controlAlt.frame = frame;
    }
    
    
    
    // JOHN: Code below doesn't work..
    
    
    // This is for the Badge Cell
    if(self.badgeString)
	{
		//force badges to hide on edit.
		if(self.editing)
			[self.badge setHidden:YES];
		else
			[self.badge setHidden:NO];
		
		
		CGSize badgeSize = [self.badgeString sizeWithFont:[UIFont boldSystemFontOfSize: 11]];
		CGRect badgeframe = CGRectMake(self.contentView.frame.size.width - (badgeSize.width + 25),
                                       (CGFloat)round((self.contentView.frame.size.height - 18) / 2),
                                       badgeSize.width + 13,
                                       18);
		
        if(self.showShadow)
            [self.badge setShowShadow:YES];
        else
            [self.badge setShowShadow:NO];
        
		[self.badge setFrame:badgeframe];
		[self.badge setBadgeString:self.badgeString];
		
		if ((self.textLabel.frame.origin.x + self.textLabel.frame.size.width) >= badgeframe.origin.x)
		{
			CGFloat badgeWidth = self.textLabel.frame.size.width - badgeframe.size.width - 10.0f;
			
			self.textLabel.frame = CGRectMake(self.textLabel.frame.origin.x, self.textLabel.frame.origin.y, badgeWidth, self.textLabel.frame.size.height);
		}
		
		if ((self.detailTextLabel.frame.origin.x + self.detailTextLabel.frame.size.width) >= badgeframe.origin.x)
		{
			CGFloat badgeWidth = self.detailTextLabel.frame.size.width - badgeframe.size.width - 10.0f;
			
			self.detailTextLabel.frame = CGRectMake(self.detailTextLabel.frame.origin.x, self.detailTextLabel.frame.origin.y, badgeWidth, self.detailTextLabel.frame.size.height);
		}
        
		//set badge highlighted colours or use defaults
		if(self.badgeColorHighlighted)
			self.badge.badgeColorHighlighted = self.badgeColorHighlighted;
		else
			self.badge.badgeColorHighlighted = [UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:1.000f];
		
		//set badge colours or impose defaults
		if(self.badgeColor)
			self.badge.badgeColor = self.badgeColor;
		else
			self.badge.badgeColor = [UIColor colorWithRed:0.530f green:0.600f blue:0.738f alpha:1.000f];
        
		if(self.badgeTextColor)
			self.badge.badgeTextColor = self.badgeTextColor;
        
	}
	else
	{
		[self.badge setHidden:YES];
	}

    
     
}


#pragma mark - From TDBadgeCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
	[super setHighlighted:highlighted animated:animated];
	[self.badge setNeedsDisplay];
    
    if(self.showShadow)
    {
        [[[self textLabel] layer] setShadowOffset:CGSizeMake(0, 1)];
        [[[self textLabel] layer] setShadowRadius:1];
        [[[self textLabel] layer] setShadowOpacity:0.8];
        
        [[[self detailTextLabel] layer] setShadowOffset:CGSizeMake(0, 1)];
        [[[self detailTextLabel] layer] setShadowRadius:1];
        [[[self detailTextLabel] layer] setShadowOpacity:0.8];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
	[self.badge setNeedsDisplay];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[super setEditing:editing animated:animated];
	
	if (editing)
    {
		self.badge.hidden = YES;
		[self.badge setNeedsDisplay];
		[self setNeedsDisplay];
	}
	else
	{
		self.badge.hidden = NO;
		[self.badge setNeedsDisplay];
		[self setNeedsDisplay];
	}
}


@end
