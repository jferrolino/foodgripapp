//
//  PVFoodGripCSV.h
//  PVTestUploadCSV
//
//  Created by John Ferrolino on 11/16/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CHCSV.h"
@protocol FoodGripDelegate;

@interface PVFoodGripCSV : NSObject <CHCSVParserDelegate>

@property (weak,nonatomic) id<FoodGripDelegate> delegate;

- (PVFoodGripCSV *)initWithPathForResource:(NSString*)name
                                  delegate:(id<FoodGripDelegate>)yourDelegate;

- (PVFoodGripCSV *)initWithPath:(NSString *)filePath delegate:(id<FoodGripDelegate>)yourDelegate;

- (void)extract;

// - (<>) exportToCSV

@end

@protocol FoodGripDelegate <NSObject>


- (void)extractEnded:(PVFoodGripCSV *)foodGripCSV withColumns:(NSArray *)columns;

- (void)extractEnded:(PVFoodGripCSV *)foodGripCSV withCategories:(NSArray *)categories;

- (void)extractEnded:(PVFoodGripCSV *)foodGripCSV withData:(NSArray *)data;

@optional

- (void)extractStarting:(id)sender;


@end
