//
//  PVMultiSelectionTVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/13/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMultiSelectionTVC.h"
#import "CRTableViewCell.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"

@interface PVMultiSelectionTVC ()

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end

@implementation PVMultiSelectionTVC

@synthesize dataSource = _dataSource;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize entityName = _entityName;

@synthesize delegate;

- (void) setSelectedMarks:(NSMutableArray *)selectedMarks
{
    _selectedMarks = selectedMarks;
    [self.tableView reloadData];
}


- (NSMutableArray *)selectedMarks
{
    if(!_selectedMarks){
        _selectedMarks = [NSMutableArray new];
    }
    return _selectedMarks;
}


- (NSManagedObjectContext *) managedObjectContext
{
    if(!_managedObjectContext){
        _managedObjectContext = [[SDCoreDataController sharedInstance] backgroundManagedObjectContext];
    }
    return _managedObjectContext;
}

- (void) loadRecordsFromCoreData
{
    [[self managedObjectContext] performBlockAndWait:^{
        
        [self.managedObjectContext reset];
        
        NSError *error = nil;
        if(self.entityName){
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:self.entityName];
            
            [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
            
            self.dataSource = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
            
            if(error){
                DDLogError(@"%s Error: %@", __PRETTY_FUNCTION__, error);
            }
        }
    }];
}

#pragma mark - Lifecycle
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])){
      
    }
    return self;
}


- (void) initTestValues
{
    self.dataSource = @[
                  @"Lorem ipsum dolor",
                  @"consectetur adipisicing",
                  @"Sed do eiusmod tempor",
                  @"incididunt ut labore",
                  @"et dolore magna aliqua",
                  @"Ut enim ad minim",
                  @"quis nostrud exercitation",
                  @"ullamco laboris nisi ut",
                  @"Duis aute irure dolor in reprehenderit",
                  @"in voluptate velit esse cillum",
                  @"dolore eu fugiat nulla pariatur",
                  @"2Lorem ipsum dolor",
                  @"2consectetur adipisicing",
                  @"2Sed do eiusmod tempor",
                  @"2incididunt ut labore",
                  @"2et dolore magna aliqua",
                  @"2Ut enim ad minim",
                  @"2quis nostrud exercitation",
                  @"2ullamco laboris nisi ut",
                  @"2Duis aute irure dolor in reprehenderit",
                  @"2in voluptate velit esse cillum",
                  @"2dolore eu fugiat nulla pariatur"];
    
    self.selectedMarks = [NSMutableArray new];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark - Methods
- (void)done:(id)sender
{
    NSLog(@"%@", self.selectedMarks);
}

#pragma mark - UITableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (void) alreadyInSelectedMarks:(NSManagedObject *)managedObject
{
    if(self.selectedMarks && [self.selectedMarks count] > 0){
        int indexToRemove = -1;
        for(int index = 0; index < [self.selectedMarks count]; index++){
            NSURL *managedMarkURL = [[self.selectedMarks[index] objectID] URIRepresentation];
            if( [[managedObject.objectID URIRepresentation] isEqual:managedMarkURL]){
                indexToRemove = index;
                break;
            }
        }
        if(indexToRemove > -1){
            [self.selectedMarks removeObjectAtIndex:indexToRemove];
            return;
        }
    }
    [self.selectedMarks addObject:managedObject];
}

- (BOOL) isInSelectedMarks:(NSManagedObject *)managedObject
{
    if(self.selectedMarks && [self.selectedMarks count] > 0){
        for(NSManagedObject *managedMark in self.selectedMarks){
            NSURL *managedMarkURL = [[managedMark objectID] URIRepresentation];
            if( [[managedObject.objectID URIRepresentation] isEqual:managedMarkURL]){
                return YES;
            }
            DDLogVerbose(@"managedMark %@", managedMarkURL);
        }
    }
    return NO;
}


@end
