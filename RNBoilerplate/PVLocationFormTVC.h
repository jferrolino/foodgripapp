//
//  PVLocationFormTVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/4/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FKFormModel.h"
#import "Location.h"

@protocol LocationFormDelegate <NSObject>

-(void) saveButtonIsClicked:(id)sender withObject:(id)location;

@end

@interface PVLocationFormTVC : UITableViewController

@property (nonatomic,weak) id<LocationFormDelegate> delegate;

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) FKFormModel *formModel;
@property (nonatomic,strong) Location *location;

@end
