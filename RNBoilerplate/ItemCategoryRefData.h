//
//  ItemCategoryRefData.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ReferenceData.h"


@interface ItemCategoryRefData : ReferenceData

@property (nonatomic, retain) NSData * photo;

@end
