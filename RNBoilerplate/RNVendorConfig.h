/*
 * RNBoilerplate
 *
 * Created by Ryan Nystrom on 10/2/12.
 * Copyright (c) 2012 Ryan Nystrom. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef RNBoilerplate_RNVendorConfig_h
#define RNBoilerplate_RNVendorConfig_h

//#error "Add Flurry API Key or comment out"
#define kFlurryAPIKey @"W83W97V973H98KP5847Y" // this is for food grip

//#error "Add Testflight API Key or comment out"
#define kTestFlightAPIKey @"9e3aed6297a114873c826071d4c92bdf_NjQwNzA4MjAxMi0wOS0yMiAwODo0MzozMi4yMjM1MzQ" // this is for assetsman

//#error "Add Parse API Key or comment out"
//#define kParseAPIKey @"87cIwjNoVdrpXXf1pJRwnMYO4UODUcW58jVbTohb" // this is for assetsman
//#define kParseClientKey @"uTY9qF7qJ0eBvXSWErUjP2eYvvR0ooI5gAsImHXz" // this is for assetsman

#ifdef DEBUG
    #define kParseAppId @"gJb0zCMZ1O2sRgLkPRGWBkEih9oUyf90DNWhgs4I"
    #define kParseAPIKey @"5cjAMKOKNuXELabjnBHNQMLBFJIlRMNvuj17U12g"
#else
    #define kParseAppId @"I3oVROYXGqOFV79aQ8vo2llqYoxzXHbuc58bzLeE"
    #define kParseAPIKey @"HqtSbmZLFcU6mURzGZzlQq5AMe8S14Nb4N3o9SYH"
#endif

//#error "Add Dropbox API Key or comment out"
#define kDropboxAPPKey @"2rwrvo0f0ruyk8u"
#define kDropboxAPPSecret @"faspaf3kdi22rsw"

//#ifdef DEBUG
#define kBumpAPPDevKey @"e717aac85ff247819c295733da7b1fc8"
/*
 #else
 
    #define kBumpAPPRelKey @""
 #endif
 */

#endif



