//
//  PVSettingsTVC.h
//  FoodGrip
//
//  Created by ENG002 on 12/28/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "StringInputTableViewCell.h"
#import "PickerInputTableViewCell.h"
#import "PVToggleButtonTVCell.h"
#import "FormKit.h"

@interface PVSettingsTVC : UITableViewController

@property (weak, nonatomic) IBOutlet StringInputTableViewCell  *restaurantName;
@property (weak, nonatomic) IBOutlet PickerInputTableViewCell *inventoryCycle;
@property (weak, nonatomic) IBOutlet PickerInputTableViewCell *currency;
@property (weak, nonatomic) IBOutlet PVToggleButtonTVCell *showTotal;
@property (weak, nonatomic) IBOutlet PVToggleButtonTVCell *syncToDropbox;

@property (nonatomic, strong) FKFormModel *formModel;

- (IBAction)revealMenu:(id)sender;

@end
