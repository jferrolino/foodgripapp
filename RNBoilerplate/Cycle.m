//
//  Cycle.m
//  FoodGrip
//
//  Created by ENG002-JRF on 2/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Cycle.h"
#import "CyclePerInventoryItem.h"
#import "Location.h"
#import "PVDateUtil.h"
#import "SDSyncEngine.h"


@implementation Cycle

@dynamic createdAt;
@dynamic cycleStatus;
@dynamic endedAt;
@dynamic locationName;
@dynamic objectId;
@dynamic startedAt;
@dynamic syncStatus;
@dynamic updatedAt;
@dynamic forLocation;
@dynamic hasItems;


- (void) awakeFromInsert
{
    [super awakeFromInsert];
    //self = [NSDate date];
    
    if(![self createdAt]){
        [self setCreatedAt:[NSDate date]];
        
    }
    
    [self setUpdatedAt:[NSDate date]];
    // or [self setPrimitiveDate:[NSDate date]];
    // to avoid triggering KVO notifications
    
}

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    self = (Cycle *)entity;
    if(self){
        if(context){
            [context deleteObject:self];
        }
    }
    return self;
}

- (NSDictionary *)JSONToCreateObjectOnServer
{
    NSDictionary *startedAt = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"Date", @"__type",
                          [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:self.startedAt], @"iso" , nil];

    NSDictionary *endedAt = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"Date", @"__type",
                          [[SDSyncEngine sharedEngine] dateStringForAPIUsingDate:self.endedAt], @"iso" , nil];

    
    NSString *jsonString = nil;
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    startedAt, @"startedAt",
                                    endedAt, @"endedAt",
                                    self.cycleStatus, @"cycleStatus",
                                    self.syncStatus, @"syncStatus", nil];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:jsonDictionary
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    
    if(!jsonData){
        NSLog(@"Error creating jsonData: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonDictionary;
}


@end
