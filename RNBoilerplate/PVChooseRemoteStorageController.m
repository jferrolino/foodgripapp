//
//  PVChooseRemoteStorageController.m
//  FoodGrip
//
//  Created by ENG002 on 12/13/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVChooseRemoteStorageController.h"
#import "PVAlertTableView.h"
#import "PVDropboxAPIClient.h"
#import "PVDropboxFileSelectionTVC.h"
#import "PVDropboxSelectionAlertTableView.h"
#import "PVInventoryItemsTVC.h"
#import "PVSimpleHUDController.h"

@interface PVChooseRemoteStorageController () <AlertTableViewDelegate,DropboxSelectionAlertDelegate>{
    PVSimpleHUDController *HUDController;
}
@property (nonatomic,strong) IBOutlet  UIButton* linkButton;
@property (weak, nonatomic) IBOutlet UIButton *showFiles;
@property (nonatomic,strong) PVDropboxAPIClient *client;
@end

@implementation PVChooseRemoteStorageController

@synthesize client = _client;
@synthesize showFiles;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (PVDropboxAPIClient *)client
{
    if(!_client){
        _client = [[PVDropboxAPIClient alloc]init];
        _client.delegate = self;
    }
    return _client;
}

- (void)viewDidLoad
{
    
    HUDController = [[PVSimpleHUDController alloc]init];
    [self.view setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateButtons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)didPressLink
{
    [self.client didPressLink:self];
}

- (IBAction)loadFiles:(id)sender{
    if([[DBSession sharedSession]isLinked]){
        DDLogWarn(@" Let's start loding files to Memory!");
        [self.client startLoadingFilesToMemory:self];
        DDLogWarn(@"Done... loading files to mem");
        /*
        MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading";
        hud.mode = MBProgressHUDModeAnnularDeterminate;
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        });*/
    }
}

- (void)updateButtons {
    NSString* title = [[DBSession sharedSession] isLinked] ? @"Unlink Dropbox" : @"Link Dropbox";
    [showFiles setHidden:NO];
    [self.linkButton setTitle:title forState:UIControlStateNormal];
}

#pragma Delegate methods

- (void)updateControls:(PVDropboxAPIClient *)sender
{
    [self updateButtons];
}

- (void) filesRetrieved:(NSMutableArray *)files
{
    
    if(files.count <= 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No files in folder"
                                                        message:@"There should be at least one file with .csv extension"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    } else{
        [[[PVDropboxSelectionAlertTableView alloc] initWithCaller:self data:[NSArray arrayWithArray:files] title:@"Select your .csv file" andContext:nil] show];
        DDLogWarn(@"The list of files are: %@",files);
    }
}

///////////////////////////////////////////////////////////////////
#pragma From PVDropboxSelectionAlertTableView
//////////////////////////////////////////////////////////////////
- (void)didSelectRowAtIndex:(NSInteger)row withContext:(id)context
{
    [[[UIAlertView alloc] initWithTitle:@"Selection Made" message:[NSString stringWithFormat:@"Index %d clicked", row] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    
}

- (void)didSelectRowWithDBMetadata:(DBMetadata *)file withContext:(id)context
{
    NSString *documentsDirectory = [PVDropboxAPIClient dbArchivePath]; // Get documents directory
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:file.filename];

    [[self.client restClient] loadFile:file.path intoPath:filePath];
    
    
    // Close the window after a file is clicked..
    [self close:self];
}
/////////////////////////////////////////////////////
#pragma mark - Starts data extraction.
/////////////////////////////////////////////////////
- (void) loadedFile:(NSString *)filepath by:(DBRestClient *)client{
    PVFoodGripCSV *fgCSV = [[PVFoodGripCSV alloc] initWithPath:filepath delegate:self];
    [fgCSV extract];
    
    
    DDLogWarn(@"Extracting content from %@",filepath);
}

/////////////////////////////////////////////////////
#pragma mark -  FoodGripCSVDelegate
/////////////////////////////////////////////////////
- (void) extractStarting:(id)sender
{
    //[HUDController showWithLabelMixed:self clipTo:self withLabel:@"Extracting"];
}

- (void) extractEnded:(PVFoodGripCSV *)foodGripCSV withCategories:(NSArray *)categories
{
  //  NSLog(@"All categories: %@",categories);
    
}

- (void)extractEnded:(PVFoodGripCSV *)foodGripCSV withColumns:(NSArray *)columns
{
  //  NSLog(@"The columns are %@",columns);
}

- (void)extractEnded:(PVFoodGripCSV *)foodGripCSV withData:(NSArray *)data
{
    DDLogWarn(@"at 1: %@", [data objectAtIndex:1]); // this is a dictionary. basically all item inside data are dictionary instances
    //NSLog(@"the Data should be like this %@",  data);
    
    UIStoryboard *storyboard;
    // check the paradigm
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil];
    } else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil];
    }
    
    
    PVInventoryItemsTVC *invItemController = [storyboard instantiateViewControllerWithIdentifier:@"ItemsView"];
    
    [invItemController saveBulk:data];
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error
{
    DDLogWarn(@"There was an error loading the file - %@", error);
}

- (void)reportFailedProcessWithError:(NSError *)error
{
    // 
}

@end
