//
//  PVCyclePageVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/22/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CyclePageContainerDelegate <NSObject>

- (void) objectIDForCreatedInventoryCycle:(NSManagedObjectID *) cycleInventoryID;
- (void) objectIDsToDelete:(NSSet *)cyclePerItemObjectIDs;
- (void) objectToUpdate:(NSManagedObject *) cycleObject withCycleStatus:(NSInteger) cycleStatus;

@end

@interface PVCyclePageContainerVC : UIViewController <UIScrollViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

// JOHN : user of this delegate is PVCyclesTVC 
@property (weak,nonatomic) id<CyclePageContainerDelegate> cycleContainerDelegate;

- (void) reloadPages;

- (IBAction)changePage:(id)sender;

- (void) previousPage;
- (void) nextPage;

- (void) manuallyRequestToGetTheseObjects;

@end
 