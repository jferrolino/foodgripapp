//
//  ReferenceData.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "ReferenceData.h"


@implementation ReferenceData

@dynamic code;
@dynamic desc;
@dynamic type;

@end
