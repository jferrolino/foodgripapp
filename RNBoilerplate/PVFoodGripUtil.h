//
//  PVFoodGripUtil.h
//  FoodGrip
//
//  Created by ENG002 on 1/18/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PVFoodGripUtil : NSObject


+ (void) initItemCategoriesToDefaults:(NSManagedObjectContext *)context;

@end
