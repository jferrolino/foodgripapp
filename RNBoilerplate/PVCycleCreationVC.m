//
//  PVCycleCreationVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/17/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCycleCreationVC.h"
#import "KLHorizontalSelect.h"
#import "PVHorizontalSelect.h"

@interface PVCycleCreationVC () <KLHorizontalSelectDelegate>{
}

@property (nonatomic, strong) PVHorizontalSelect *selection;
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation PVCycleCreationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //Initialize the informtion to feed the control
    NSString* plistPath = [[NSBundle mainBundle] pathForResource: @"Inventory-Item-Categories"
                                                          ofType: @"plist"];
    // Build the array from the plist
    NSArray* controlData = [[NSArray alloc] initWithContentsOfFile:plistPath];
    
	// Do any additional setup after loading the view, typically from a nib.
    self.selection = [[PVHorizontalSelect alloc] initWithFrame:self.view.bounds];
    [self.selection setTop:self.view.top];
    [self.selection setTableData: controlData];
    [self.selection setDelegate:self];
    
    //Customize the initially selected index - Note section is redundant but should always be 0
    [self.selection setCurrentIndex:[NSIndexPath indexPathForRow:4 inSection:0]];
    
    
    self.tableView = [[UITableView alloc]initWithFrame:self.view.bounds];
    [self.tableView setBottom:self.view.bottom];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.selection];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
