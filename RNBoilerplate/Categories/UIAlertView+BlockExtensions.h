//
//  UIAlertView+BlockExtensions.h
//  FoodGrip
//
//  Created by ENG002-JRF on 2/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//
//
//  UIAlertView+BlockExtensions.h
//
//  Created by Tom Fewster on 07/10/2011.
//



@interface UIAlertView (BlockExtensions) <UIAlertViewDelegate>

- (id)initWithTitle:(NSString *)title message:(NSString *)message completionBlock:(void (^)(NSUInteger buttonIndex, UIAlertView *alertView))block cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

@end
