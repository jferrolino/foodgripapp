//
//  PVCycleDetailsVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCycleDetailsVC.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "PVEditableTableDataRow.h"
#import "PVEditableTableDataRowItem.h"
#import "Item.h"
#import "PVGlobals.h"

#define ITEM_NAME_HEADER_LABEL          @"Description"
#define INVENTORY_COUNT_HEADER_LABEL    @"Inventory Count"
#define PURCHASE_COUNT_HEADER_LABEL     @"Purchase Count"
#define DEFAULT_ROW_ITEM_PADDING            20
#define ITEM_NAME_HEADER_LABEL_WIDTH        150
#define INVENTORY_COUNT_HEADER_LABEL_WIDTH  150
#define PURCHASE_COUNT_HEADER_LABEL_WIDTH   150
#define DEFAULT_TEXTFIELD_HEIGHT        25
#define DEFAULT_COMBO_HEIGHT            25

enum {
    //return [NSArray arrayWithObjects:nameItem,quantity, orderUnitItem, orderPriceItem, purchase,totalPurchasePriceItem, nil];
    CYCLE_COLUMN_PURCHASE_COUNT = 4,
} CYCLE_DETAILS;

@interface PVCycleDetailsVC() <UITableViewDataSource, UITableViewDelegate, EditableTableDataRowDelegate>

@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSMutableArray *cyclePerItemIDs;

@property (nonatomic, strong) UIView *tableHeader;
@property (nonatomic, strong) NSDecimalNumber *totalPurchasePrice;
@end

@implementation PVCycleDetailsVC 

@synthesize horizontalSelector;

@synthesize categories;
@synthesize cyclePerItems = _cyclePerItems;
@synthesize cyclePerItemIDs = _cyclePerItemIDs;
@synthesize totalPurchasePrice = _totalPurchasePrice;
@synthesize cycleDetailsDataSource;
@synthesize managedObjectContext;

@synthesize tableHeader = _tableHeader;

- (UIView *)tableHeader
{
    if(_tableHeader){
        _tableHeader = [[UIView alloc] init];
    }
    return _tableHeader;
}

- (NSDecimalNumber *) totalPurchasePrice
{
    if(!_totalPurchasePrice){
        _totalPurchasePrice = [NSDecimalNumber zero];
    }
    return _totalPurchasePrice;
}

- (NSMutableArray *) cyclePerItemIDs
{
    if(!_cyclePerItemIDs){
        _cyclePerItemIDs = [NSMutableArray array];
    }
    return _cyclePerItemIDs;
}

- (void) loadCycleItemsFromCoreDataWithCategory:(NSString *)categoryName
{

    if([self cycleDetailsDataSource]){
        
        [[self managedObjectContext] performBlockAndWait:^{
            
            if([self.cyclePerItemIDs count] == 0){
                self.cycle = (Cycle *)[self.managedObjectContext objectWithID:[[self.cycleDetailsDataSource useThisInstanceOfCycle] objectID]];                
                if(self.cycle)
                    for(NSManagedObject *instance in self.cycle.hasItems){
                        NSManagedObjectID *id = instance.objectID;
                        if(id) [[self cyclePerItemIDs] addObject:id];
                    }
            }
                        
            NSError *error = nil;
            NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CyclePerInventoryItem"];
            
            NSPredicate *fromThisList = [NSPredicate predicateWithFormat:@"self IN %@", self.cyclePerItemIDs ];
            
            if( categoryName != nil && ![[categoryName lowercaseString] isEqualToString:@"all items"]){
                
                NSPredicate *withCategory = [NSPredicate predicateWithFormat:@"forItem.category like %@",categoryName];
                [request setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[withCategory,fromThisList]]];
                
            } else {
                if(fromThisList)[request setPredicate:fromThisList];
            }
            
            
            if(self.horizontalSelector)
            [self.horizontalSelector setSelectionViewToIndex:3];

            
            self.cyclePerItems = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        }];
    }
}

- (void) viewDidLoad
{
    [self.tableHeader setBackgroundColor:[UIColor blueColor]];
    [self.tableHeader sizeToFit];
    
    [self.view setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    
    // Do any additional setup after loading the view.
    //[self.navigationController.navigationBar setTintColor:[UIColor colorWithRed:10/255.0 green:37/255.0 blue:70/255.0 alpha:1]];
    
    
    [self setManagedObjectContext:[[SDCoreDataController sharedInstance]newManagedObjectContext]];
    
    [[self managedObjectContext] setUndoManager:[[NSUndoManager alloc]init]];
    
    /*
    self.horizontalSelector.dataSource = self;
    self.horizontalSelector.delegate = self;
    self.horizontalSelector.shouldBeTransparent = YES;
    self.horizontalSelector.horizontalScrolling = YES;
    [self.horizontalSelector setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
     */
    
    [self.itemTableView setDataSource:self];
    [self.itemTableView setDelegate:self];
    [self.itemTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.itemTableView setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    
    
    //storyboard ini
    
    
    CGRect selectorFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height / 12);
    self.horizontalSelector = [[IZValueSelectorView alloc] initWithFrame:selectorFrame asHorizontalScrolling:YES];
    self.horizontalSelector.dataSource = self;
    self.horizontalSelector.delegate = self;
    self.horizontalSelector.shouldBeTransparent = YES;
    [self.horizontalSelector setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
     
    
    [self.itemTableView setTableHeaderView:self.horizontalSelector];
    
    //self.horizontalSelector = selectorView;
    
    
    // JOHN: This doesn't work.
    //[self.itemTableView setTableHeaderView:self.tableHeader];
    
    [self setCategories:[PVGlobals retrieveCategories]];
    [self loadCycleItemsFromCoreDataWithCategory:nil];
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    if([self.managedObjectContext undoManager]){
        [[self.managedObjectContext undoManager] beginUndoGrouping];
    }

  
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self loadCycleItemsFromCoreDataWithCategory:@"All Items"];
    [[self itemTableView] reloadData];
    [[self.managedObjectContext undoManager] endUndoGrouping];
    [[self.managedObjectContext undoManager] undo];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
	if (fromInterfaceOrientation == UIInterfaceOrientationPortrait || fromInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
		//[self.tableHeader setText:TABLE_HEADER_LABEL_LANDSCAPE];
    }
    
    self.itemTableView.tableHeaderView = [self tableHeader];
	[self.itemTableView setNeedsLayout];
}

//
#pragma mark - Value Selector dataSource
//

- (NSInteger)numberOfRowsInSelector:(IZValueSelectorView *)valueSelector {
    return [self.categories count];
}



//ONLY ONE OF THESE WILL GET CALLED (DEPENDING ON the horizontalScrolling property Value)
- (CGFloat)rowHeightInSelector:(IZValueSelectorView *)valueSelector {
    return 75.0;
}

- (CGFloat)rowWidthInSelector:(IZValueSelectorView *)valueSelector {
    return (self.horizontalSelector.frame.size.height * 1.25);
}

//
#pragma mark - Value Selector delegate
//
- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index {
    
    //CGSize targetWidth = [self.horizontalSelector.frame.size height];
    //CGLOt
    
    CGFloat targetHeight = self.horizontalSelector.frame.size.height;
    CGFloat targetWidth = targetHeight * 1.25;
    
    UIView *catView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, targetWidth,  targetHeight )];
    
    NSString *category = [self.categories objectAtIndex:index];
    NSString *imgCatName = [category lowercaseString];
    
    UIGraphicsBeginImageContext(catView.frame.size);
    
    [[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imgCatName]] drawInRect:catView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    catView.backgroundColor = [UIColor colorWithPatternImage:image];

    
    //[catView setBackgroundColor:[UIColor redColor]];

   
    UILabel * label = nil;
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, (catView.height/3) * 2, targetWidth, targetHeight /3)];
    
        //NSLog(@"The categories size: %i", [self.categories count]);
    //NSLog(@"The category selected: %@", [self.categories objectAtIndex:index]);
    
    label.text = [NSString stringWithFormat:@"%@", [self.categories objectAtIndex:index]];
    label.textAlignment =  NSTextAlignmentCenter;
    
    [label setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0, 1)];
    [label setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.75f]];
    
    [catView addSubview:label];

    return catView;
}

- (CGRect)rectForSelectionInSelector:(IZValueSelectorView *)valueSelector {
    //Just return a rect in which you want the selector image to appear
    //Use the IZValueSelector coordinates
    //Basically the x will be 0
    //y will be the origin of your image
    //width and height will be the same as in your selector image
    if (valueSelector == self.horizontalSelector) {
        return CGRectMake(self.horizontalSelector.frame.size.width/2 - 35.0, 0.0, self.horizontalSelector.frame.size.height * 1.25, valueSelector.frame.size.height);
    }
    return CGRectMake(0,0,0,0);
}

- (void)selector:(IZValueSelectorView *)_valueSelector didSelectRowAtIndex:(NSInteger)index {
    
    //DDLogVerbose(@"%s => Selected index %d", __func__, index);
    [self loadCycleItemsFromCoreDataWithCategory:(NSString *)[self.categories objectAtIndex:index]];
    [self.itemTableView reloadData];
}

- (NSArray *)defineTableRowItems:(CyclePerInventoryItem *) cycleItem
{
    NSString *itemName = cycleItem.forItem.name;//(cycleItem.forItem.name == nil)? @"" : cycleItem.forItem.name;// .forThis.name;
    NSString *orderUnit = cycleItem.forItem.orderUnit;
    NSString *orderPrice = [NSString stringWithFormat:@"%@",cycleItem.forItem.orderPrice ];
    NSNumber *quantityCount = cycleItem.quantityCount;
    NSNumber *purchaseCount = cycleItem.purchaseCount;
    
    PVEditableTableDataRowItem *nameItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(ITEM_NAME_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:itemName
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil];
    [nameItem toggleEditingMode:NO];
    
    PVEditableTableDataRowItem *quantity = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(INVENTORY_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:[NSString stringWithFormat:@"%@",quantityCount]
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil];
    [quantity toggleEditingMode:NO];
    
    PVEditableTableDataRowItem *purchase = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(PURCHASE_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:[NSString stringWithFormat:@"%@",purchaseCount]
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil usingNumberedKeyboard:YES];
    
    PVEditableTableDataRowItem *orderUnitItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(PURCHASE_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:orderUnit
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil];
    [quantity toggleEditingMode:NO];
    
    PVEditableTableDataRowItem *orderPriceItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(PURCHASE_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:orderPrice
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil
                                                                                             asCurrency:YES usingNumberedKeyboard:YES];
    [quantity toggleEditingMode:NO];
    
    
   
    [self totalPriceMultipliedBy:purchaseCount withSeed:cycleItem.forItem.orderPrice];
    
    PVEditableTableDataRowItem *totalPurchasePriceItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                     selections:nil
                                                                                               selectionListKey:nil
                                                                                                       baseSize:CGSizeMake(PURCHASE_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                      canResize:NO
                                                                                                    normalImage:nil
                                                                                                  selectedImage:nil
                                                                                                   controlLabel:[NSString stringWithFormat:@"%@", [self totalPurchasePrice]]
                                                                                                   buttonTarget:nil
                                                                                                   buttonAction:nil
                                                                                                     asCurrency:YES usingNumberedKeyboard:YES];
    [quantity toggleEditingMode:NO];
    
    if([cycleItem.by cycleStatus] == [NSNumber numberWithInt:2]){
        /* Only when cycle status is ACTIVE that this control is editable */
        [purchase toggleEditingMode:YES];
    } else {
        [purchase toggleEditingMode:NO];
    }
    
    // This should be aligned with the switch control in method: dataRow delegate method of EditableDataRow Class
    return [NSArray arrayWithObjects:nameItem,quantity, orderUnitItem, orderPriceItem, purchase,totalPurchasePriceItem, nil];
}

- (void) totalPriceMultipliedBy:(NSNumber *)purchaseCount withSeed:(NSDecimalNumber *)seeded
{
    [self setTotalPurchasePrice:seeded];
    self.totalPurchasePrice = [self.totalPurchasePrice decimalNumberByMultiplyingBy:[PVGlobals numberToDecimalNumber:purchaseCount]];
}

#pragma mark - Table View Data Source delegate methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.cyclePerItems count];
}

- (PVEditableTableDataRow *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    PVEditableTableDataRow *cell = (PVEditableTableDataRow *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){        
        cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier
                                                 itemPadding:DEFAULT_ROW_ITEM_PADDING
                                                 scaleToFill:YES
                                                    forTable:self.itemTableView];
    }
    cell.delegate = self;
    cell.tag = [indexPath row];
    
    cell.backgroundView = [PVGlobals commonCellBackgroundView:cell];
    
    // Configure the cell...
    CyclePerInventoryItem *cycleItem = [self.cyclePerItems objectAtIndex:[indexPath row]];
    
    cell.rowItems = [self defineTableRowItems:(CyclePerInventoryItem *)cycleItem];
    [cell applyNonEditingModeToItems];
    
    return cell;
}


#define ORDER_UNIT_HEADER_LABEL @"Order Unit: Unit"
#define ORDER_PRICE_HEADER_LABEL @"Order Unit: Price"
#define TOTAL_PRICE_HEADER_LABEL @"Total Price"

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width, tableView.frame.size.height / 12)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    PVEditableTableDataRow *cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil itemPadding:DEFAULT_ROW_ITEM_PADDING scaleToFill:NO forTable:tableView];
    
    //cell.rowItems = [self defineTableHeaderViewColumnLabelsWithHeight:headerView.frame.size.height];
    
    CGSize size = CGSizeMake(cell.frame.size.width, cell.frame.size.height);
    
    NSArray *labels = @[ITEM_NAME_HEADER_LABEL,
                        INVENTORY_COUNT_HEADER_LABEL,
                        ORDER_UNIT_HEADER_LABEL,
                        ORDER_PRICE_HEADER_LABEL,
                        PURCHASE_COUNT_HEADER_LABEL,
                        TOTAL_PRICE_HEADER_LABEL];
    
    NSDictionary *labelsWithTargetedWidth = @{
                                              ITEM_NAME_HEADER_LABEL        : @ITEM_NAME_HEADER_LABEL_WIDTH,
                                              INVENTORY_COUNT_HEADER_LABEL  : @INVENTORY_COUNT_HEADER_LABEL_WIDTH,
                                              ORDER_UNIT_HEADER_LABEL       : @PURCHASE_COUNT_HEADER_LABEL_WIDTH,
                                              ORDER_PRICE_HEADER_LABEL      : @PURCHASE_COUNT_HEADER_LABEL_WIDTH,
                                              PURCHASE_COUNT_HEADER_LABEL   : @PURCHASE_COUNT_HEADER_LABEL_WIDTH,
                                              TOTAL_PRICE_HEADER_LABEL      : @PURCHASE_COUNT_HEADER_LABEL_WIDTH
                                              };
    
    cell.rowItems = [PVGlobals customTableColumnHeader:cell withSize:size andLabels:labels withCorrespondingWidth:labelsWithTargetedWidth];
    
    [cell applyEditingModeAs:NO];
    [cell setFrame:headerView.frame];
    [headerView addSubview:cell];
    
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section
{
    return tableView.frame.size.height / 12;
}

-(IBAction) saveChanges
{
    // Do saving of the cycle per Items here..
    [self.managedObjectContext performBlockAndWait:^{
        
        NSError *error = nil;
        
        [self.managedObjectContext save:&error];
        
        if(error){
            DDLogVerbose(@"Error => %@", error);
        }
        
        [[SDCoreDataController sharedInstance] saveMasterContext];
        //[[SDSyncEngine sharedEngine]startSync];
        [self.navigationController popViewControllerAnimated:YES];

    }];
}
- (void) willMoveToParentViewController:(UIViewController *)parent
{
    DDLogWarn(@" %s" , __PRETTY_FUNCTION__);
}

// ====================================================
#pragma mark - EditableTableDataRowDelegate methods
// ====================================================

BOOL reloadRowNow = NO;
NSIndexPath *rowToReload = nil;

- (void)didSelectDataRow:(EditableTableDataRow *)dataRow inTable:(UITableView *)table
{
    NSIndexPath *selectedRow = [table indexPathForCell:dataRow];
    
    if(reloadRowNow){
        if(rowToReload){
            DDLogVerbose(@" Reloading the row : %i", [rowToReload row]);
            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            [self.itemTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
            rowToReload = nil;
            reloadRowNow = NO;
        }
    }

    [self.itemTableView selectRowAtIndexPath:selectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];
}


- (void)dataRow:(PVEditableTableDataRow *)dataRow didSetValue:(id)newValue forColumn:(int)column inTable:(UITableView *)table
{
    DDLogWarn(@"%s : Invoking column %i", __PRETTY_FUNCTION__, column );
    NSIndexPath *selectedRow = [table indexPathForCell:dataRow];
    
    if([selectedRow row] >= [self.cyclePerItems count]) return;
    
    CyclePerInventoryItem *toEditCycleItem = [self.cyclePerItems objectAtIndex:[selectedRow row]];
        if([[dataRow.rowItems objectAtIndex:column] isKindOfClass:[PVEditableTableDataRowItem class]]){
            
            PVEditableTableDataRowItem *updateCycleItem = [dataRow.rowItems objectAtIndex:column];
            
            [updateCycleItem changeControlLabelTo:newValue];
            
            NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
            [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
            switch (column){
                case CYCLE_COLUMN_PURCHASE_COUNT :{
                    
                    //newValue stringByRepl
                    
                    NSNumber *purchaseCount = [f numberFromString:newValue];
                    rowToReload = selectedRow;
                    reloadRowNow = YES;
                    [toEditCycleItem setValue:purchaseCount forKey:@"purchaseCount"];
                    [self totalPriceMultipliedBy:purchaseCount withSeed:[self totalPurchasePrice]];
                    break;
                }
                default:
                    break;
            }
        }
    
        //NSArray* rowsToReload = [NSArray arrayWithObjects:selectedRow, nil];
        //[self.itemTableView reloadRowsAtIndexPaths:rowsToReload withRowAnimation:UITableViewRowAnimationNone];
        //[self.itemTableView reloadData];
        DDLogVerbose(@"%s : Updated cycle item : %@ in row : %i", __PRETTY_FUNCTION__, toEditCycleItem, [selectedRow row]);
}

@end
