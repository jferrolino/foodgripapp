//
//  PVCycleCreationTVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/9/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IZValueSelectorView.h"
#import "Cycle.h"


@protocol CyclePage2DataSource <NSObject>

- (NSManagedObjectContext *) useThisManagedObjectContextForPage2;
- (void) persistedCyclePerItemFrom:(id)sender withIDs:(NSSet *)objectIDs;

@end

@protocol CyclePage2Delegate <NSObject>

- (Cycle *) useThisCycleInstance;

- (NSSet *) useTheseCyclePerItemIDs;

@end

@interface PVCyclePage2TVC : UITableViewController <UITableViewDataSource,UITableViewDelegate, IZValueSelectorViewDataSource, IZValueSelectorViewDelegate>


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

// This is instantiated from PVCyclePage which is referred by this instance variable.
@property (nonatomic, strong) Cycle  *referredCycle;

@property (nonatomic, strong) IBOutlet IZValueSelectorView *horizontalSelector;

@property (nonatomic, weak) id<CyclePage2DataSource> dataSource;
@property (nonatomic, weak) id<CyclePage2Delegate> delegate;

@property (nonatomic, getter = isOkToSave) BOOL okToSave;

- (void) loadCycleItemsFromCoreDataWithThisCategory:(NSString *)categoryName;
@end
