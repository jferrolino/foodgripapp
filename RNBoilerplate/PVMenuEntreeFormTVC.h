//
//  PVMenuEntreeFormTVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/5/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FKFormModel.h"
#import "MenuEntree.h"

@protocol MenuEntreeFormDelegate <NSObject>

-(void) saveButtonIsClicked:(id)sender withObject:(id)object;

@end

@protocol MenuEntreeFormDataSource <NSObject>

@optional

-(UINavigationController *) myNavigationController;

-(NSManagedObjectContext *) useManagedObjectContext;

@end

@interface PVMenuEntreeFormTVC : UITableViewController

@property (nonatomic,weak) id<MenuEntreeFormDelegate> delegate;
@property (nonatomic,weak) id<MenuEntreeFormDataSource> datasource;

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) FKFormModel *formModel;

@property (nonatomic,strong) MenuEntree *entree;
@property (nonatomic,strong) UINavigationController *navigationController;

- (id) initWithFrame:(CGRect) frame underNavigationController:(UINavigationController *)navController;
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil underNavigationController:(UINavigationController *) navController;

@end
