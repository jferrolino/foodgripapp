//
//  ReferencePhotoData.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, Location, MenuEntree;

@interface ReferencePhotoData : NSManagedObject

@property (nonatomic, retain) NSNumber * latMax;
@property (nonatomic, retain) NSNumber * latMin;
@property (nonatomic, retain) NSNumber * lonMax;
@property (nonatomic, retain) NSNumber * lonMin;
@property (nonatomic, retain) NSData * photoData;
@property (nonatomic, retain) NSString * photoDigest;
@property (nonatomic, retain) NSData * bannerPhotoData;
@property (nonatomic, retain) Item *forItem;
@property (nonatomic, retain) Location *forLocation;
@property (nonatomic, retain) MenuEntree *forMenuEntree;

@end
