//
//  PVCycleCreationTVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/9/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCyclePage2TVC.h"
#import "SDSyncEngine.h"
#import "Item.h"
#import "Cycle.h"
#import "CyclePerInventoryItem.h"
#import "PVEditableTableDataRow.h"
#import "PVEditableTableDataRowItem.h"
#import "PVGlobals.h"

#import "SDCoreDataController.h"

#define DEFAULT_ROW_ITEM_PADDING            50
#define ITEM_NAME_HEADER_LABEL          @"Description"
#define INVENTORY_COUNT_HEADER_LABEL    @"Inventory Count"
#define PURCHASE_COUNT_HEADER_LABEL     @"Purchase Count"
#define ITEM_NAME_HEADER_LABEL_WIDTH        120
#define INVENTORY_COUNT_HEADER_LABEL_WIDTH  130
#define PURCHASE_COUNT_HEADER_LABEL_WIDTH   130

#define DEFAULT_TEXTFIELD_HEIGHT        25
#define DEFAULT_COMBO_HEIGHT            25

enum {
    CYCLE_COLUMN_ITEM_NAME = 0,
    CYCLE_COLUMN_QUANTITY_COUNT = 1,
    CYCLE_COLUMN_PURCHASE_COUNT = 2,
} CYCLE_PAGE_2;

@interface PVCyclePage2TVC () <UITableViewDataSource, UITableViewDelegate, EditableTableDataRowDelegate>

@property (nonatomic, strong) NSMutableArray *inventoryItems;
@property (nonatomic, strong) NSMutableArray *cycleItems;

// I will be saving created cycleItem here while not yet persistent in Core Data
@property (nonatomic, strong) NSMutableArray *transientCycleItems;

// same pattern used in InventoryItemsTVC.h
@property (nonatomic, strong) CyclePerInventoryItem *targetCycleItem; 
@property (nonatomic, strong) NSArray *categories;

// This will only be populated once that is just about the cycle items instances are persisted.
//@property (nonatomic, strong) NSMutableSet *cycleItemIDs;

@end

@implementation PVCyclePage2TVC

@synthesize inventoryItems;
@synthesize cycleItems = _cycleItems;
@synthesize managedObjectContext;

@synthesize categories = _categories;
@synthesize referredCycle = _referredCycle;

@synthesize dataSource;
@synthesize horizontalSelector = _horizontalSelector;
@synthesize targetCycleItem;


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSArray *) cycleItems
{
    if(_cycleItems == nil){
        _cycleItems = [NSMutableArray array];
    }
    return _cycleItems;
}

- (void)viewDidLoad
{
    
    if([self dataSource]){
        [self setManagedObjectContext:[self.dataSource useThisManagedObjectContextForPage2]];        
    }
    
    self.horizontalSelector = [[IZValueSelectorView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 94.0)];
    [self.horizontalSelector setBackgroundColor:[UIColor scrollViewTexturedBackgroundColor]];
    [self.horizontalSelector setDataSource:self];
    [self.horizontalSelector setDelegate:self];
    [self.horizontalSelector setShouldBeTransparent:YES];
    [self.horizontalSelector setHorizontalScrolling:YES];
    [self.horizontalSelector setBackgroundColor:[PVGlobals commonViewBackgroundColor] ];
    [self.tableView setTableHeaderView:[self horizontalSelector]];
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    // John: Must default to "All Items" category for the horizontal select.
    //[self loadItemsFromCoreDataWithCategory:nil];
    [self loadCycleItemsFromCoreDataWithThisCategory:nil];

    // Populate the categories lists
    //[self setCategories:[[NSUserDefaults standardUserDefaults] objectForKey:ITEM_CATEGORIES ]];
    [PVGlobals reloadToGetRecentCategories];
    [self setCategories:[PVGlobals retrieveCategories]];

    //[self setEditing:YES];
    //self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadCycleItemsFromCoreDataWithThisCategory:(NSString *)categoryName
{    
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];

            if(self.delegate){
                if([self.delegate.useTheseCyclePerItemIDs count] > 0){
                    NSSet *cycleIDs = [self.delegate useTheseCyclePerItemIDs];
                    NSError *error = nil;
                    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CyclePerInventoryItem"];
                    
                    
                    NSPredicate *fromThisList = [NSPredicate predicateWithFormat:@"self IN %@", cycleIDs ];
                    if( categoryName != nil && ![[categoryName lowercaseString] isEqualToString:@"all items"]){
                        NSPredicate *withThisCategory = [NSPredicate predicateWithFormat:@"(forItem.category like %@)", categoryName];
                       [request setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[withThisCategory,fromThisList]]];
                    } else {
                        [request setPredicate:fromThisList];
                    }
                                        
                    self.cycleItems = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
                    if(error){
                        DDLogError(@"%s : Error message -> %@",__PRETTY_FUNCTION__,error);
                    }
                }
            }        
    }];
}


// =========================================
#pragma mark - Table view data source
// =========================================
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.cycleItems count];
}


- (PVEditableTableDataRow *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CycleItemCell";
    
    PVEditableTableDataRow *cell = (PVEditableTableDataRow *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier itemPadding:DEFAULT_ROW_ITEM_PADDING scaleToFill:YES forTable:self.tableView];
    }
    cell.delegate = self;
    cell.tag = [indexPath row];

    cell.backgroundView = [PVGlobals commonCellBackgroundView:cell];
    
    // Configure the cell...
    CyclePerInventoryItem *cycleItem = [self.cycleItems objectAtIndex:[indexPath row]];
    
    cell.rowItems = [self defineTableRowItems:(CyclePerInventoryItem *)cycleItem];
    [cell applyNonEditingModeToItems];
    
    return cell;
}

// =======================================
#pragma mark - Table view delegate
// =======================================
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DDLogWarn(@"%s : Editing Style is UITableViewCellEditingStyleDelete",__PRETTY_FUNCTION__);
    } else if (editingStyle == UITableViewCellEditingStyleInsert){
        // is called when + sign is clicked for row selected.
        //[self updateItemAtIndexPath:indexPath withItem:self.targetItem forAdd:YES];
        
        // John's Note: We'll only reload data to the table; The persistence will come when DONE button
        // is clicked.
        [self.tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleNone){
        // for inline editing of items' properties??
        DDLogWarn(@"Editing style None is invoked!!!");
    }
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width, tableView.frame.size.height / 12)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    static NSString *CellIdentifier = @"ReviewCell";
    PVEditableTableDataRow *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell){
        cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil itemPadding:50 scaleToFill:NO forTable:tableView];
    }
    //[cell setSelectionStyle:UITableViewCellEditingStyleDelete];
    
    //cell.rowItems = [self defineTableHeaderViewColumnLabelsWithHeight:headerView.frame.size.height];
    
    CGSize size = CGSizeMake(cell.frame.size.width, cell.frame.size.height);
    
    NSArray *labels = @[ITEM_NAME_HEADER_LABEL,
                        INVENTORY_COUNT_HEADER_LABEL,
                        PURCHASE_COUNT_HEADER_LABEL];
    
    NSDictionary *labelsWithTargetedWidth = @{ITEM_NAME_HEADER_LABEL : @ITEM_NAME_HEADER_LABEL_WIDTH,
                                              INVENTORY_COUNT_HEADER_LABEL : @(INVENTORY_COUNT_HEADER_LABEL_WIDTH),
                                              PURCHASE_COUNT_HEADER_LABEL  : @(PURCHASE_COUNT_HEADER_LABEL_WIDTH)};
    
    cell.rowItems = [PVGlobals customTableColumnHeader:cell withSize:size andLabels:labels withCorrespondingWidth:labelsWithTargetedWidth];
    
    [cell applyEditingModeAs:NO];
    [cell setFrame:headerView.frame];
    [headerView addSubview:cell];
    
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section
{
    return tableView.frame.size.height / 12;
}



//
#pragma mark - Private method
//
- (NSArray *)defineTableRowItems:(CyclePerInventoryItem *) cycleItem
{
    DDLogWarn(@"%s: THe cycle item : %@", __PRETTY_FUNCTION__, cycleItem);
    
    //NSNumber *itemNumber = cycleItem.forItem.itemNumber;
    NSString *itemName = (cycleItem.forItem.name == nil)? @"" : cycleItem.forItem.name;
    NSNumber *quantityCount = cycleItem.quantityCount;
    NSNumber *purchaseCount = cycleItem.purchaseCount;
    
    

    
    /*PVEditableTableDataRowItem *numberItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField selections:nil selectionListKey:nil baseSize:CGSizeMake(CATEGORY_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT) canResize:NO normalImage:nil selectedImage:nil controlLabel:[NSString stringWithFormat:@"%@",itemNumber] buttonTarget:nil buttonAction:nil];
    [numberItem toggleEditingMode:NO];
    */
    
    PVEditableTableDataRowItem *nameItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField selections:nil selectionListKey:nil baseSize:CGSizeMake(ITEM_NAME_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT) canResize:NO normalImage:nil selectedImage:nil controlLabel:itemName buttonTarget:nil buttonAction:nil];
    [nameItem toggleEditingMode:NO];
    
    PVEditableTableDataRowItem *quantity = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField selections:nil selectionListKey:nil baseSize:CGSizeMake(INVENTORY_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT) canResize:NO normalImage:nil selectedImage:nil controlLabel:[NSString stringWithFormat:@"%@",quantityCount] buttonTarget:nil buttonAction:nil usingNumberedKeyboard:YES];
    [quantity toggleEditingMode:YES];
    
    PVEditableTableDataRowItem *purchase = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField selections:nil selectionListKey:nil baseSize:CGSizeMake(PURCHASE_COUNT_HEADER_LABEL_WIDTH,DEFAULT_TEXTFIELD_HEIGHT) canResize:NO normalImage:nil selectedImage:nil controlLabel:[NSString stringWithFormat:@"%@",purchaseCount] buttonTarget:nil buttonAction:nil usingNumberedKeyboard:YES];
    [purchase toggleEditingMode:YES];
    
    // This should be aligned with the switch control in method: dataRow delegate method of EditableDataRow Class
    return [NSArray arrayWithObjects:nameItem,quantity, purchase, nil];
}

//
#pragma mark - Value Selector delegates
//
- (NSInteger)numberOfRowsInSelector:(IZValueSelectorView *)valueSelector {
    return [self.categories count];
}

//ONLY ONE OF THESE WILL GET CALLED (DEPENDING ON the horizontalScrolling property Value)
- (CGFloat)rowHeightInSelector:(IZValueSelectorView *)valueSelector {
    return 75.0;
}

- (CGFloat)rowWidthInSelector:(IZValueSelectorView *)valueSelector {
    return (self.horizontalSelector.frame.size.height * 1.25);
}

- (void) categoryViewInitialization
{
    
}

//
#pragma mark - Value Selector delegate
//
- (UIView *)selector:(IZValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index {
    
    CGFloat targetHeight = self.horizontalSelector.frame.size.height;
    CGFloat targetWidth = targetHeight * 1.25;
    
    UIView *catView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, targetWidth,  targetHeight )];
    
    NSString *category = [self.categories objectAtIndex:index];
    NSString *imgCatName = [category lowercaseString];
    
    UIGraphicsBeginImageContext(catView.frame.size);
    
    [[UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg",imgCatName]] drawInRect:catView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    catView.backgroundColor = [UIColor colorWithPatternImage:image];
    

    UILabel * label = nil;
    label = [[UILabel alloc] initWithFrame:CGRectMake(0, (catView.height/3) * 2, targetWidth, targetHeight /3)];
        
    label.text = [NSString stringWithFormat:@"%@", [self.categories objectAtIndex:index]];
    label.textAlignment =  NSTextAlignmentCenter;
    
    [label setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0, 1)];
    [label setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.75f]];
    
    [catView addSubview:label];
    
    return catView;
}

- (CGRect)rectForSelectionInSelector:(IZValueSelectorView *)valueSelector {
    //Just return a rect in which you want the selector image to appear
    //Use the IZValueSelector coordinates
    //Basically the x will be 0
    //y will be the origin of your image
    //width and height will be the same as in your selector image
    if (valueSelector == self.horizontalSelector) {
        return CGRectMake(self.horizontalSelector.frame.size.width/2 - 35.0, 0.0, 100.0, 94.0);
    }
    return CGRectMake(0,0,0,0);
}

- (void)selector:(IZValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index {
    
    NSString *selectedCategory = [self.categories objectAtIndex:index];
    NSSet *updatedObjects  = [self.managedObjectContext updatedObjects];
    
    DDLogWarn(@"%s : The updated objects are %@", __PRETTY_FUNCTION__,updatedObjects);
    
    DDLogWarn(@"%s : Cycle items have size of %i", __PRETTY_FUNCTION__, [self.cycleItems count]);

    [[SDCoreDataController sharedInstance] saveMasterContext];
    [self loadCycleItemsFromCoreDataWithThisCategory:selectedCategory];
    [self.tableView reloadData];
}

//
#pragma mark - EditableTableDataRowDelegate methods


- (void)didSelectDataRow:(EditableTableDataRow *)dataRow inTable:(UITableView *)table
{
    DDLogWarn(@"%s : Selecting a data row. ",__PRETTY_FUNCTION__);
    NSIndexPath *selectedRow = [table indexPathForCell:dataRow];
   // self.tableView setSel
	[self.tableView selectRowAtIndexPath:selectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];
    
}
//
- (void)dataRow:(PVEditableTableDataRow *)dataRow didSetValue:(id)newValue forColumn:(int)column inTable:(UITableView *)table
{
    DDLogWarn(@"%s : Invoking column %i", __PRETTY_FUNCTION__, column );
    
    //column:
    // 2 is for quantity count
    // 3 is for purchase count
    //
    NSIndexPath *selectedRow = [self.tableView indexPathForCell:dataRow];
    
    CyclePerInventoryItem *toEditCycleItem = [self.cycleItems objectAtIndex:[selectedRow row]];
    [toEditCycleItem setValue:[NSNumber numberWithInt:SDObjectCreated] forKey:@"syncStatus"];

    if([[dataRow.rowItems objectAtIndex:column] isKindOfClass:[PVEditableTableDataRowItem class]]){
       
        PVEditableTableDataRowItem *updateCycleItem = [dataRow.rowItems objectAtIndex:column];
        
        [updateCycleItem changeControlLabelTo:newValue];
        
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
            switch (column){
                case CYCLE_COLUMN_QUANTITY_COUNT:
                    [toEditCycleItem setValue:[f numberFromString:newValue] forKey:@"quantityCount"];
                    break;
                case CYCLE_COLUMN_PURCHASE_COUNT:
                    [toEditCycleItem setValue:[f numberFromString:newValue] forKey:@"purchaseCount"];
                    break;
                default:
                    break;
        }
    }
        
    NSError *error = nil;
    [[toEditCycleItem managedObjectContext]save:&error];
    
    if(error)  {
        DDLogError(@"%s : Error: %@", __PRETTY_FUNCTION__, error);
    }
    
    DDLogWarn(@"%s : Updated cycle item : %@", __PRETTY_FUNCTION__, toEditCycleItem);
}
@end