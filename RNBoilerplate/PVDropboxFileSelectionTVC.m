//
//  PVDropboxFileSelectionTVC.m
//  FoodGrip
//
//  Created by ENG002 on 12/17/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVDropboxFileSelectionTVC.h"
#import <DropboxSDK/DropboxSDK.h>
#import "MBProgressHud.h"

@interface PVDropboxFileSelectionTVC ()
@property (strong, nonatomic) NSMutableArray *items;

@end

@implementation PVDropboxFileSelectionTVC

@synthesize items = _items;

- (NSMutableArray *) items
{
    if(!_items){
        _items = [NSMutableArray array];
    }
    return _items;
}

- (id) initWithItems:(NSMutableArray *)items
{
    self = [super init];
    if(self){
        self.items = items;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return self.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    DBMetadata *item = [self.items objectAtIndex:indexPath.row];
    cell.textLabel.text = item.filename;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //[self doSomethingWithRowAtIndexPath:indexPath];
    /*MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = @"Processing";
    */
    /*
    [self doSomethingInBackgroundWithProgressCallback:^(float progress) {
        hud.progress = progress;
    } completionCallback:^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
     */
    MBProgressHUD *hud =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Processing";
    hud.mode = MBProgressHUDModeAnnularDeterminate;
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // Do something...
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

@end
