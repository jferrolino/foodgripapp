//
//  PVMenuTVCViewController.m
//  NewAssetsMan
//
//  Created by jon.ferrolino on 10/20/12.
//  Copyright (c) 2012 Ryan Nystrom. All rights reserved.
//

#import "PVMenuTVC.h"

@interface PVMenuTVC ()

@property (nonatomic,strong) NSArray *menuItems;

@end

@implementation PVMenuTVC

- (void)awakeFromNib
{
    self.menuItems = [NSArray arrayWithObjects: @"Assets", @"Employee", nil];
}

@end
