//
//  Cycle.h
//  FoodGrip
//
//  Created by ENG002-JRF on 2/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    CYCLE_STATUS_CLOSED = 1,
    CYCLE_STATUS_ACTIVE,
    CYCLE_STATUS_ARCHIVED,
} PV_CYCLE_STATUS;

@class CyclePerInventoryItem, Location;

@interface Cycle : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * cycleStatus;
@property (nonatomic, retain) NSDate * endedAt;
@property (nonatomic, retain) NSString * locationName;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSDate * startedAt;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) Location *forLocation;
@property (nonatomic, retain) NSSet *hasItems;

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context;


@end

@interface Cycle (CoreDataGeneratedAccessors)

- (void)addHasItemsObject:(CyclePerInventoryItem *)value;
- (void)removeHasItemsObject:(CyclePerInventoryItem *)value;
- (void)addHasItems:(NSSet *)values;
- (void)removeHasItems:(NSSet *)values;

@end
