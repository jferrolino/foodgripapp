//
//  PVDropboxAPIClient.m
//  FoodGrip
//
//  Created by ENG002 on 12/13/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVDropboxAPIClient.h"
@interface PVDropboxAPIClient()


@end

@implementation PVDropboxAPIClient

@synthesize restClient = _restClient;

- (void)didPressLink:(id)sender
{
    if (![[DBSession sharedSession] isLinked]) {
        [self link:sender];
    } else {
        [self unlink:sender];
        [self.delegate updateControls:self];
    }
}

- (void) link:(id)sender
{
    [[DBSession sharedSession] linkFromController:sender];
}

- (void) unlink:(id)sender
{
    [[DBSession sharedSession] unlinkAll];
    [[[UIAlertView alloc]
      initWithTitle:@"Account Unlinked!" message:@"Your dropbox account has been unlinked"
      delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]
     show];
}

- (DBRestClient*)restClient {
    if (_restClient == nil) {
        _restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        _restClient.delegate = self;
    }
    return _restClient;
}

- (void) startLoadingFilesToMemory:(id)sender
{
    //NSLog(@"INVOKING method >>>>>> startLoadingFilesToMemory!!!");
    NSLog(@" Start loading files to memory..");
    [self.restClient loadMetadata:@"/"];
    //NSLog(@"INVOKING method >>>>>> doneLoadingFilesToMemory!!!");
}

#pragma -
#pragma delegate methods from Dropbox API

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    if (metadata.isDirectory) {
        NSArray *validExt = [NSArray arrayWithObjects:@"csv", nil];
        NSMutableArray *files = nil;
        NSLog(@"Folder '%@' contains:", metadata.path);
        for (DBMetadata *file in metadata.contents) {
            NSString *ext = [[file.path pathExtension] lowercaseString];
            if(!file.isDirectory && [validExt indexOfObject:ext] != NSNotFound){
                if(!files) files = [NSMutableArray array];
                
                [files addObject:file];
                NSLog(@"\t%@", file.filename);
            }
        }
        
        if(files){
            [self.delegate filesRetrieved:files];
        }
        else {
            // alert the user that no file is found
            [[[UIAlertView alloc]initWithTitle:@"No files found" message:@"Foodgrip didn't find a .csv file. Please upload .csv file to your Foodgrip in the dropbox Apps directory." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
        }
    }
}

- (void)restClient:(DBRestClient *)client
loadMetadataFailedWithError:(NSError *)error
{
    [self.delegate reportFailedProcessWithError:error];
    NSLog(@"Error loading metadata: %@", error);
}




- (void)restClient:(DBRestClient*)client loadedFile:(NSString*)localPath
{
    [self.delegate loadedFile:localPath by:client];
    NSLog(@"File loaded into path: %@", localPath);
    
}

- (void)restClient:(DBRestClient*)client loadFileFailedWithError:(NSError*)error {
    NSLog(@"There was an error loading the file - %@", error);
}

#pragma archive  path directory for all files downloaded from dropbox
+ (NSString *) dbArchivePath
{
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask , YES);
    
    // Get one and only document directory from that list
    NSString *documentDirectory = [documentDirectories lastObject];
    
    return documentDirectory;
}

@end

