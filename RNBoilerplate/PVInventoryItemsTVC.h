//
//  PVInventoryItemsTVC.h
//  FoodGrip
//
//  Created by ENG002 on 12/13/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

@interface PVInventoryItemsTVC : UITableViewController 

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) NSMutableArray *invItems;
@property (nonatomic,strong) NSString *entityName;

@property (nonatomic, strong) NSMutableArray *predicates;

@property (nonatomic, strong) IBOutlet UILabel *tableHeader;

- (IBAction)revealMenu:(id)sender;


- (void)saveBulk:(NSArray *)invItems;


@end
