//
//  PVDropboxFileSelectionTVC.h
//  FoodGrip
//
//  Created by ENG002 on 12/17/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVDropboxFileSelectionTVC : UITableViewController

- (id) initWithItems:(NSMutableArray *)array;

@end
