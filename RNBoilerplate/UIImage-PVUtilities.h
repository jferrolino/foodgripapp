//
//  UIImage+PVUtilities.h
//  FoodGrip
//
//  Created by ENG002-JRF on 2/25/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

// There's a bug when creating CI Images from PNG vs JPEG
// This is a workaround
CIImage *ciImageFromPNG(NSString *pngFileName);

@interface UIImage (PVUtilities)

// Extract a subimage
- (UIImage *) subImageWithBounds:(CGRect) rect;

// This is a bug workaround for creating a UIImage from a CIImage
+ (UIImage *) imageWithCIImage: (CIImage *) aCIImage orientation: (UIImageOrientation) anOrientation;

@end
