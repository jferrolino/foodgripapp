//
//  PVMenuVC.h
//  NewAssetsMan
//
//  Created by jon.ferrolino on 10/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

@interface PVMenuVC : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@end
