//
//  PVFoodGripCSV.m
//  PVTestUploadCSV
//
//  Created by ENG002 on 11/16/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVFoodGripCSV.h"
@interface PVFoodGripCSV(){
    NSString *_fileString;
}

@property (strong,nonatomic) NSMutableArray *bufferColumns;
@property (strong) NSString *bufferCategories;

@property (strong,nonatomic) NSMutableArray *parsedArray;
@property (strong,nonatomic) NSMutableArray *parsedData;

@property (strong) NSMutableDictionary *columns;
@property (strong) NSArray *categories;
@property (strong) NSMutableArray *data;

@end

@implementation PVFoodGripCSV

@synthesize bufferColumns = _bufferColumns;
@synthesize bufferCategories = _bufferCategories;
@synthesize parsedArray = _parsedArray;
@synthesize parsedData = _parsedData;
@synthesize columns = _columns;
@synthesize categories = _categories;
@synthesize data = _data;
@synthesize delegate = _delegate;


- (PVFoodGripCSV *)initWithPathForResource:(NSString *)name delegate:(id<FoodGripDelegate>)yourDelegate
{
    self = [super init];
    if(self){
        
        NSError *error = nil;
        NSString *filePath = [[NSBundle mainBundle] pathForResource:name ofType:@"csv"];
        _fileString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        
        self.delegate = yourDelegate;
        
        self.parsedArray = [NSMutableArray array];
        self.categories = [NSArray array];
        self.data = [NSMutableArray array];
    }
    return self;
}

- (PVFoodGripCSV *)initWithPath:(NSString *)filePath delegate:(id<FoodGripDelegate>)yourDelegate
{
    self = [super init];
    if(self){
        NSError *error = nil;
        _fileString = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&error];
        
        if(error){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"File uploaded is not value"
                                                            message:[NSString stringWithFormat:@"The error message : %@",error ]
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            NSLog(@"There is an error with this message %@", error);
        }
        
        [self setDelegate:yourDelegate];
        [self setParsedArray:[NSMutableArray array]];
        [self setCategories:[NSArray array]];
        [self setData:[NSMutableArray array]];
    }
    return self;
}

- (void) extract
{
    NSError *error = nil;
    if (_fileString) {
        CHCSVParser * p = [[CHCSVParser alloc] initWithCSVString:_fileString encoding:NSUTF8StringEncoding error:&error];
        [p setParserDelegate:self];
        [p parse];
    }
}

- (void) parser:(CHCSVParser *)parser didStartDocument:(NSString *)csvFile {
    if([self delegate]){
        [self.delegate extractStarting:self];
    }
}

- (void) parser:(CHCSVParser *)parser didStartLine:(NSUInteger)lineNumber {
}

- (void) parser:(CHCSVParser *)parser didReadField:(NSString *)field {
    
    [self.bufferColumns addObject:field];
}


#define COLUMN_LINE_NUMBER 10
#define RANK_INDEX 0
#define CATEGORIES_INDEX 1
#define CATEGORIES_KEY @"Categories"

- (void) parser:(CHCSVParser *)parser didEndLine:(NSUInteger)lineNumber {
    
    if(lineNumber == COLUMN_LINE_NUMBER){
        NSUInteger index = 0;
        
        for(NSString * column in[NSMutableArray arrayWithArray:self.bufferColumns]){
            if(!self.columns){
                self.columns = [NSMutableDictionary dictionary];
            }
            
            [self.columns setValue:column forKey:[NSString stringWithFormat:@"%i",index++]];
        }
        
    } else if(lineNumber > COLUMN_LINE_NUMBER){
        
        if([self.bufferColumns objectAtIndex:RANK_INDEX] && ![(NSString *)[self.bufferColumns objectAtIndex:RANK_INDEX] isEqualToString:@""]){
            
            if([self.bufferColumns objectAtIndex:CATEGORIES_INDEX] && ![(NSString *)[self.bufferColumns objectAtIndex:CATEGORIES_INDEX] isEqualToString:@""])
            {
                self.bufferCategories = [self.bufferColumns objectAtIndex:CATEGORIES_INDEX];
                self.categories = [self.categories arrayByAddingObject:self.bufferCategories];
            } else {
                [self.bufferColumns replaceObjectAtIndex:CATEGORIES_INDEX withObject:self.bufferCategories];
            }
            
            if(!self.parsedData) {
                self.parsedData = [NSMutableArray arrayWithArray:self.bufferColumns];
            }else {
                [self.parsedData addObjectsFromArray:self.bufferColumns];
            }
        }
    }
    
    self.bufferColumns = [NSMutableArray array];
}

- (void) parser:(CHCSVParser *)parser didEndDocument:(NSString *)csvFile {
    
    NSMutableDictionary *dictData = [NSMutableDictionary dictionary];
    
    int dataIndex = 0;
    do{
        for(int i = 0 ; i < [self.columns count];i++){
            NSString *key = [NSString stringWithFormat:@"%@",[self.columns objectForKey:[NSString stringWithFormat:@"%i",i]]];
            
            if(dataIndex >= [self.parsedData count])
                break;
            
            [dictData setObject:[self.parsedData objectAtIndex:dataIndex++] forKey:key];
        }
        
        [self.data addObject:dictData];
        dictData = [NSMutableDictionary dictionary];
        
    }while (dataIndex < [self.parsedData count]);
    
    if([self delegate]){
        if( [self.delegate respondsToSelector:@selector(extractEnded:withColumns:)]){
            [[self delegate] extractEnded:self withColumns:[[self columns]copy]];
        }
        
        if( [self.delegate respondsToSelector:@selector(extractEnded:withCategories:)]){
            [[self delegate] extractEnded:self withCategories:[[self categories]copy]];
        }
        
        if([self.delegate respondsToSelector:@selector(extractEnded:withData:)]){
            [[self delegate] extractEnded:self withData:[[self data]copy]];
        }
    }
   
}

- (void) parser:(CHCSVParser *)parser didFailWithError:(NSError *)error {
}

@end
