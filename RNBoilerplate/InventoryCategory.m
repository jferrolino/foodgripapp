//
//  InventoryCategory.m
//  FoodGrip
//
//  Created by ENG002-JRF on 2/20/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "InventoryCategory.h"


@implementation InventoryCategory

@dynamic name;
@dynamic image;

@end
