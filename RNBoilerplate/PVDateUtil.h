//
//  PVDateUtil.h
//  FoodGrip
//
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PVDateUtil : NSObject

+ (NSString *) strInStandardDateFormat:(NSDate *) date;
+ (NSString *) strFromDateForJSONEncoding:(NSDate *) date;
@end
