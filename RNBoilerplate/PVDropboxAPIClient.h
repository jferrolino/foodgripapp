//
//  PVDropboxAPIClient.h
//  FoodGrip
//
//  Created by ENG002 on 12/13/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <DropboxSDK/DropboxSDK.h>

@class PVDropboxAPIClient;

@protocol DropboxAPIClientDelegate

- (void)updateControls:(PVDropboxAPIClient *)sender;

- (void) filesRetrieved:(NSMutableArray *)files;

- (void) reportFailedProcessWithError: (NSError *) error;

- (void) loadedFile:(NSString *)localPath by:(DBRestClient *)client;

@end

@interface PVDropboxAPIClient : NSObject <DBRestClientDelegate>

@property (strong, nonatomic) DBRestClient* restClient;

@property (strong,nonatomic) id<DropboxAPIClientDelegate> delegate;

+ (NSString *) dbArchivePath;

- (void)didPressLink:(id)sender;

- (void)startLoadingFilesToMemory:(id)sender;

@end
