//
//  PVCycleDetailsVC.h
//  FoodGrip
//
/*
    This is for view only.
    
    The CRUD will be implemented in wizard like flow
*/
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Cycle.h"
#import "PVInventoryItemsTVC.h"
#import "IZValueSelectorView.h"
#import "CyclePerInventoryItem.h"

@protocol CycleDetailsDataSource <NSObject>

- (Cycle *) useThisInstanceOfCycle;

@end

@interface PVCycleDetailsVC : UIViewController <UITableViewDelegate, UITableViewDataSource,IZValueSelectorViewDelegate, IZValueSelectorViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (nonatomic, strong) Cycle *cycle;
@property (nonatomic, strong ) NSMutableArray *cyclePerItems;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;


@property (strong, nonatomic) IBOutlet IZValueSelectorView *horizontalSelector;
@property (weak, nonatomic) IBOutlet UITableView *itemTableView;
@property (weak, nonatomic) id<CycleDetailsDataSource> cycleDetailsDataSource;

- (void) loadCycleItemsFromCoreDataWithCategory:(NSString *)categoryName;
- (IBAction) saveChanges;

@end
