//
//  PVLocationsGVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GKLParallaxPicturesViewController.h"

@interface PVLocationsGVC : GKLParallaxPicturesViewController

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableArray *currentData;
@property (nonatomic,strong) NSString *entityName;


- (IBAction)revealMenu:(id)sender;

@end
