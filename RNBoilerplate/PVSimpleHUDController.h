//
//  PVSimpleHUDController.h
//  FoodGrip
//
//  Created by ENG002 on 1/16/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"

@interface PVSimpleHUDController : NSObject <MBProgressHUDDelegate>

- (void) showCustomView:(id)sender clipTo:(UIViewController *)base withLabel:(NSString *) label delayFor:(NSInteger) delay;

- (void)showWithLabelMixed:(id)sender clipTo:(UIViewController *)base withLabel:(NSString *)label;

@end
