//
//  PVCyclesTVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCyclesTVC.h"
#import "TDBadgedCell.h"
#import "SDCoreDataController.h"
#import "PVDateUtil.h"
#import "SDSyncEngine.h"
#import "PVCycleDetailsVC.h"
#import "JFDepthView.h"
#import "PVCyclePage2TVC.h"
#import "GKLParallaxPicturesViewController.h"
#import "PVCyclePage1TVC.h"
#import "PVCyclePageContainerVC.h"

#import "PVEditableTableDataRow.h"
#import "PVEditableTableDataRowItem.h"

#define DATERANGE_COLUMN_RELATIVE_WIDTH     250
#define NAME_COLUMN_RELATIVE_WIDTH          100
#define INFO_COLUMN_RELATIVE_WIDTH          100

#define DEFAULT_ROW_ITEM_PADDING            70
#define DEFAULT_TEXTFIELD_HEIGHT            40

@interface PVCyclesTVC () <UITableViewDelegate, UITableViewDataSource, JFDepthViewDelegate, parallaxDelegate, CycleDetailsDataSource, CyclePageContainerDelegate>{
    BOOL isDoneClicked;
}

@property (nonatomic, strong) JFDepthView *depthView;
@property (nonatomic, strong) GKLParallaxPicturesViewController *parallaxController;
@property (nonatomic, strong) Cycle *selectedCycle;

@property (nonatomic, strong) PVCyclePageContainerVC *cyclePagesContainer;
@property (nonatomic, strong) NSManagedObjectID *cycleInventoryObjectID;
@property (nonatomic, strong) NSSet *toDeleteCyclePerItemObjectIDs;
@property (nonatomic, strong) Cycle *cycleObjectForUpdate;
@property (nonatomic) NSInteger cycleObjectNewStatus;

@property (nonatomic, strong) UIBarButtonItem *formDoneButton;

@end

@implementation PVCyclesTVC

@synthesize managedObjectContext = _managedObjectContext;
@synthesize cycles = _cycles;
@synthesize entityName = _entityName;

@synthesize parallaxController = _parallaxController;
@synthesize depthView = _depthView;
@synthesize selectedCycle = _selectedCycle;
@synthesize addCycleButton;
@synthesize formDoneButton;

@synthesize toDeleteCyclePerItemObjectIDs;
@synthesize cyclePagesContainer;
@synthesize cycleObjectForUpdate;
@synthesize cycleObjectNewStatus;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSString *)entityName
{
    if(_entityName == nil){
        _entityName = @"Cycle";
    }
    return _entityName;
}

- (void)loadRecordsFromCoreData {
    
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:self.entityName];
        [request setSortDescriptors:[NSArray arrayWithObjects:
                                     [NSSortDescriptor sortDescriptorWithKey:@"updatedAt" ascending:NO],
                                     [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:NO],nil]];
        [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        self.cycles = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        DDLogInfo(@"%s : The cycles retrieved has a size of %i", __PRETTY_FUNCTION__, [self.cycles count]);
    }];
}

- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:MENU_ITEM_INVENTORY_CYCLES];
    
    self.cycles = [NSMutableArray array];
    isDoneClicked = NO;
    
    [self.navigationItem.leftBarButtonItem  setCustomView:[PVGlobals menuBarButtonThemeFromTarget:self withAction:@selector(revealMenu:)]];

    if(self.managedObjectContext == nil){
        self.managedObjectContext = [[SDCoreDataController sharedInstance]newManagedObjectContext];
    }
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.view setBackgroundColor: [PVGlobals commonViewBackgroundColor]];
    
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    self.depthView = [[JFDepthView alloc] initWithGestureRecognizer:tapRec];
    
    self.cyclePagesContainer = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad"
                                                          bundle:nil] instantiateViewControllerWithIdentifier:@"CycleCreationContainer"];
    [self.cyclePagesContainer.view setFrame:CGRectMake(0, 0, 600, 680)];
    
    self.parallaxController = [[GKLParallaxPicturesViewController alloc] initWithImages:[NSArray arrayWithObjects:@"http://media-cdn.tripadvisor.com/media/photo-s/00/14/0f/98/pension-bacolod-restaurant.jpg",nil] andContentView:self.cyclePagesContainer.view];
    
    self.parallaxController.parallaxDelegate = self;

    //#warning Active this later when CRUD is already functioning..
    [self loadRecordsFromCoreData];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self loadRecordsFromCoreData];
    [[self tableView] reloadData];
}

- (void) reloadWithNewCycleContainer:(PVCyclePageContainerVC *)container
{
    self.parallaxController =  self.parallaxController = [[GKLParallaxPicturesViewController alloc] initWithImages:[NSArray arrayWithObjects:@"http://media-cdn.tripadvisor.com/media/photo-s/00/14/0f/98/pension-bacolod-restaurant.jpg",nil] andContentView:container.view];
}

// ======================================
#pragma mark - Table view methods
// ======================================

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.cycles count];
}

#define DR_VALUE_COLUMN_PADDING     40
#define LN_VALUE_COLUMN_PADDING     50
#define TIC_VALUE_COLUMN_PADDING    40
#define TPC_VALUE_COLUMN_PADDING    60

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"SOMECELL";
        
    TDBadgedCell *cell = (TDBadgedCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell == nil){
        cell = [[TDBadgedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16.0];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    Cycle *cycle = [self.cycles objectAtIndex:indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    NSString *dateRange = [NSString stringWithFormat:@"%@    -    %@",[PVGlobals formatDateToString:[cycle startedAt]],[PVGlobals formatDateToString:[cycle endedAt]]];
    dateRange = [dateRange stringByPaddingToLength:DR_VALUE_COLUMN_PADDING withString:@"\t" startingAtIndex:0];
    
    NSString *locationName = [cycle locationName];
    locationName = [locationName stringByPaddingToLength:LN_VALUE_COLUMN_PADDING withString:@"\t" startingAtIndex:0];
    
    int totalInventoryCount = 0;
    int totalPurchaseCount = 0;
    
    if(cycle.hasItems) {
        for(CyclePerInventoryItem *cyclePerItem in cycle.hasItems){
            
            totalPurchaseCount += [cyclePerItem.purchaseCount intValue];
            totalInventoryCount += [cyclePerItem.quantityCount intValue];
        }
    }
 
    NSString *totalIC = [NSString stringWithFormat:@"%i", totalInventoryCount];
    totalIC = [totalIC stringByPaddingToLength:TIC_VALUE_COLUMN_PADDING withString:@"\t" startingAtIndex:0];
    
    NSString *totalPC = [NSString stringWithFormat:@"%i", totalPurchaseCount];
    totalPC = [totalPC stringByPaddingToLength:TPC_VALUE_COLUMN_PADDING withString:@"\t" startingAtIndex:0];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@%@%@%@",dateRange,locationName,totalIC,totalPC];
    if( cycle.cycleStatus == [NSNumber numberWithInt:1]){
        cell.badgeColor = [UIColor redColor];
        cell.badgeString = @"CLOSED";
    } else if(cycle.cycleStatus == [NSNumber numberWithInt:2]) {
        cell.badgeColor = [UIColor greenColor];
        cell.badgeString = @"OPEN";
    } else if(cycle.cycleStatus == [NSNumber numberWithInt:3]) {
        cell.badgeColor = [UIColor grayColor];
        cell.badgeString = @"ARCHIVED";
    }
        
    cell.backgroundView = [PVGlobals commonCellBackgroundView:cell];
    
    [cell.textLabel setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [cell.textLabel setShadowColor:[UIColor whiteColor]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    
    [cell setTag:[indexPath row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Add logic here..
    PVCycleDetailsVC *cycleDetails = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"CYCLE_DETAILS"];
    
    [self setSelectedCycle:[self.cycles objectAtIndex:[indexPath row]]];
    
    [cycleDetails setCycleDetailsDataSource:self];
    [cycleDetails loadCycleItemsFromCoreDataWithCategory:@"All Items"];    
    [self.navigationController pushViewController:cycleDetails animated:YES];
}

#define DATE_RANGE_HEADER_LABEL             @"Date Range"
#define LOCATION_NAME_HEADER_LABEL          @"Location"
#define TOTAL_PURCHASE_COUNT_HEADER_LABEL   @"Total Purchase Count"
#define TOTAL_INVENTORY_COUNT_HEADER_LABEL  @"Total Inventory Count"
#define TOTAL_HEADER_LABEL                  @"Total"
#define CYCLE_STATUS_HEADER_LABEL           @"Status"

#define TIC_HEADER_LABEL_WIDTH      100
#define TPC_HEADER_LABEL_WIDTH      110
#define DR_HEADER_LABEL_WIDTH       110
#define LN_HEADER_LABEL_WIDTH       200
#define CS_HEADER_LABEL_WIDTH       100

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,tableView.frame.size.width, tableView.frame.size.height / 12)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    PVEditableTableDataRow *cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil itemPadding:DEFAULT_ROW_ITEM_PADDING scaleToFill:NO forTable:self.tableView];

    CGSize size = CGSizeMake(cell.frame.size.width, cell.frame.size.height);
    
    NSArray *labels = @[DATE_RANGE_HEADER_LABEL,
                        LOCATION_NAME_HEADER_LABEL,
                        TOTAL_INVENTORY_COUNT_HEADER_LABEL,
                        TOTAL_PURCHASE_COUNT_HEADER_LABEL,
                        CYCLE_STATUS_HEADER_LABEL];
    
    NSDictionary *labelsWithTargetedWidth = @{
                                              DATE_RANGE_HEADER_LABEL           : @DR_HEADER_LABEL_WIDTH,
                                              LOCATION_NAME_HEADER_LABEL        : @LN_HEADER_LABEL_WIDTH,
                                              TOTAL_INVENTORY_COUNT_HEADER_LABEL: @TIC_HEADER_LABEL_WIDTH,
                                              TOTAL_PURCHASE_COUNT_HEADER_LABEL : @TPC_HEADER_LABEL_WIDTH,
                                              CYCLE_STATUS_HEADER_LABEL         : @CS_HEADER_LABEL_WIDTH};
    
    cell.rowItems = [PVGlobals customTableColumnHeader:cell withSize:size andLabels:labels withCorrespondingWidth:labelsWithTargetedWidth];
    
    [cell applyEditingModeAs:NO];
    [cell setFrame:headerView.frame];
    [headerView addSubview:cell];
    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView
heightForHeaderInSection:(NSInteger)section
{
    return tableView.frame.size.height / 12;
}


#pragma mark - private methods

- (IBAction) addCycle:(id)sender
{
    [self.depthView setDelegate:self];
    [self.depthView presentViewController:self.parallaxController inView:self.view animated:YES];
}

///////////////////////////////////////////
#pragma mark - Depth View methods
///////////////////////////////////////////
// Here is the simple dismissal method called from the tap recognizer passed into init method of JFDepthView
- (void)dismiss {
    [self.depthView dismissPresentedViewInView:self.view animated:YES];
}

#pragma mark - Parallax delegate
- (void )imageTapped:(UIImage *)image
{
    DDLogWarn(@" The image is tapped");
}

#pragma mark - PVCycle details datasource method
-(Cycle *) useThisInstanceOfCycle
{
    return [self selectedCycle];
}

#pragma mark - JF Depth View delegate methods

- (void)willPresentDepthView:(JFDepthView*)depthView
{
    if(formDoneButton == nil){
        self.formDoneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(clickDone:)];
        [self.formDoneButton setTitle:@"Done"];
    }
    
    NSMutableArray *rightButtons =[self.navigationItem.rightBarButtonItems mutableCopy];
    
    [rightButtons addObject:self.formDoneButton];
    
    [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];
    
    [self.addCycleButton setEnabled:NO];

    [self.cyclePagesContainer setCycleContainerDelegate:self];
    
}

- (void)didDismissDepthView:(JFDepthView*)depthView
{
    NSMutableArray *rightButtons =[self.navigationItem.rightBarButtonItems mutableCopy];
    
    [rightButtons removeObject:self.formDoneButton];
    
    [self.navigationItem setRightBarButtonItems:rightButtons animated:YES];

    [self.addCycleButton setEnabled:YES];
    
    // JOHN: We need to skip this when invoked from clickDone method.
    if(!isDoneClicked) [self.cyclePagesContainer manuallyRequestToGetTheseObjects];
    
    self.cyclePagesContainer = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad"
                                                          bundle:nil] instantiateViewControllerWithIdentifier:@"CycleCreationContainer"];
    [self.cyclePagesContainer.view setFrame:CGRectMake(0, 0, 600, 680)];
    
    //JOHN: must remove the cycle and its corresponding items (ie cyclePerItems)
    // when this is invoked. This implies that the user didn't click DONE.

    [self reloadWithNewCycleContainer:self.cyclePagesContainer];

    // JOHN: Let's proceed in the deletion
    [self deleteTheseItemsFromDatabaseWithIDs];

    // JOHN: Just resetting this.
    isDoneClicked = NO;
    
    [self loadRecordsFromCoreData];
    [[self tableView] reloadData];
}

#define CYCLE_ENDED_AT_PROPERTY @"endedAt"
#define CYCLE_STATUS_PROPERTY @"cycleStatus"

// -=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=--=-=-=-=-=---=-=
#pragma mark - This is the Save button you click after creating a cycle (with its corresponding items).
// -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
- (void) clickDone:(id)sender
{
    // Add logic here..

    // JOHN : just do nothing here..
    
    // JOHN: We can show some HUD in action here.. That's basically a trick-show only since we already saved
    // the objects since PVCyclePage2 appeared.
    
    // Then reload new..
    
    
    // JOHN: We should update the object that gets
    // obsolete here (ie FROM ACTIVE to ARCHIVED)
    
    if( [self cycleObjectForUpdate] && [self cycleObjectNewStatus]){
        if(![self managedObjectContext]){
            [self setManagedObjectContext:[[SDCoreDataController sharedInstance] newManagedObjectContext]];
        }
        
        [self.managedObjectContext performBlockAndWait:^{
            
            DDLogWarn(@"%s : Updating object: %@ with cycle status :%d",__PRETTY_FUNCTION__, [self cycleObjectForUpdate], [self cycleObjectNewStatus]);
            
           Cycle *managedObject = (Cycle *)[self.managedObjectContext objectWithID:[self.cycleObjectForUpdate objectID]];
            
            //NSSet *items =  managedObject.hasItems;
            [managedObject setValue:[NSNumber numberWithInteger:[self cycleObjectNewStatus]] forKey:CYCLE_STATUS_PROPERTY];
            [managedObject setValue:[NSDate date] forKey:CYCLE_ENDED_AT_PROPERTY];
            
            NSError *updateError;
            [[managedObject managedObjectContext] save:&updateError];
            
            if(updateError){
                DDLogError(@"Error : %@",updateError);
            }
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
        }];
    }
    
    isDoneClicked = YES;
    [self dismiss];
}


- (void) deleteTheseItemsFromDatabaseWithIDs
{
    if(![self managedObjectContext]){
        [self setManagedObjectContext:[[SDCoreDataController sharedInstance] newManagedObjectContext]];
    }
    
    [self.managedObjectContext performBlockAndWait:^{
        
        // Do database deletion here..
        if([self cycleInventoryObjectID]){
            DDLogInfo(@"The cycleInventoryObjectId %@" , [self cycleInventoryObjectID]);
            
            [self.managedObjectContext deleteObject:[self.managedObjectContext
                                         objectWithID:[self cycleInventoryObjectID]]];
            
            NSManagedObject *cycle = [self.managedObjectContext objectWithID:[self cycleInventoryObjectID]];
            
            DDLogWarn(@"%s : Deleting object %@", __PRETTY_FUNCTION__, cycle);
            
            if(cycle){
                // This will also cascade deletion to its corresponding
                // cycler per inventory item instances.
                [[self managedObjectContext] deleteObject:cycle];
            }
            
            NSError *deletionError = nil;
            [[self managedObjectContext] save:&deletionError];
        }
        
        [[SDCoreDataController sharedInstance] saveMasterContext];
        [self.tableView reloadData];
    }];
}

#pragma mark -  Cycle Page Container delegate
- (void) objectIDsToDelete:(NSSet *)cyclePerItemObjectIDs
{
    [self setToDeleteCyclePerItemObjectIDs:cyclePerItemObjectIDs];
}

- (void) objectIDForCreatedInventoryCycle:(NSManagedObjectID *)cycleInventoryID
{
    [self setCycleInventoryObjectID:cycleInventoryID];
}

- (void) objectToUpdate:(NSManagedObject *)cycleObject withCycleStatus:(NSInteger)cycleStatus
{
    [self setCycleObjectForUpdate:(Cycle *)cycleObject];
    [self setCycleObjectNewStatus:cycleStatus];
}

@end
