//
//  PVSettingsTVC.m
//  FoodGrip
//
//  Created by ENG002 on 12/28/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVSettingsTVC.h"
#import "PVSettings.h"

#import "FormKit.h"
#import "PVSimpleHUDController.h"

@interface PVSettingsTVC () <StringInputTableViewCellDelegate> {
    PVSimpleHUDController *HUDController;
}

@property (nonatomic, strong)  PVSettings *settings;

@end

@implementation PVSettingsTVC

@synthesize settings;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:MENU_ITEM_SETTINGS];
    
    [self.navigationItem.leftBarButtonItem  setCustomView:[PVGlobals menuBarButtonThemeFromTarget:self withAction:@selector(revealMenu:)]];
    
    [self.restaurantName setDelegate:self];
    
    HUDController = [[PVSimpleHUDController alloc]init];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    PVSettings *pvSettings = [[PVSettings alloc]init];
    
    pvSettings.name = [[NSUserDefaults standardUserDefaults] objectForKey:RESTAURANT_NAME];
    pvSettings.contactEmail = [[NSUserDefaults standardUserDefaults] objectForKey:CONTACT_EMAIL];
    pvSettings.inventoryCycleType = [[NSUserDefaults standardUserDefaults] objectForKey:INVENTORY_CYCLE];
    pvSettings.syncDropbox = [[NSUserDefaults standardUserDefaults] boolForKey:SYNC_DROPBOX__];
    pvSettings.language = [[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGE];
    
    DDLogWarn(@"Inventory cycle type: %@ and Language : %@", [pvSettings inventoryCycleType], [pvSettings language] );
    
    self.formModel = [FKFormModel formTableModelForTableView:self.tableView
                                        navigationController:self.navigationController];
    
    [self setSettings:pvSettings];
    
    [FKFormMapping mappingForClass:[PVSettings class] block:^(FKFormMapping *formMapping) {
        
        [formMapping sectionWithTitle:@"General Info" footer:@"" identifier:@"genInfo"];
        [formMapping mapAttribute:@"name" title:@"Restaurant Name" type:FKFormAttributeMappingTypeText];
        
        [formMapping sectionWithTitle:@"Inventory Overview" footer:@"" identifier:@"info"];
        [formMapping mapAttribute:@"inventoryCycleType" title:@"Inventory Cycle"
                     showInPicker:NO
                selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex) {
                    *selectedValueIndex = (NSInteger) value;
                    return [NSArray arrayWithObjects:@"weekly",@"daily",@"monthly",nil];
                } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex){
                    return value;
                }labelValueBlock:^id(id value, id object){
                    return value;
                }];
        [formMapping mapAttribute:@"contactEmail" title:@"Contact Email" type:FKFormAttributeMappingTypeText];

        [formMapping sectionWithTitle:@"Locale" identifier:@"locale"];
        [formMapping mapAttribute:@"language" title:@"Language"
                     showInPicker:NO
         selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex) {
             *selectedValueIndex = 0;
             NSArray *languages = [NSLocale preferredLanguages];
             
             return languages;
         } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex){
             return value;
         }labelValueBlock:^id(id value, id object){
             return value;
         }];
        
        [formMapping sectionWithTitle:@"Data Service Plan" identifier:@"data_service"];
        [formMapping mapAttribute:@"syncDropbox" title:@"Sync to Dropbox" type:FKFormAttributeMappingTypeBoolean];

        [formMapping sectionWithTitle:@"" identifier:@"saveButton"];
        
        [formMapping buttonSave:@"Save" handler:^{
            DDLogWarn(@"%@", self.settings);    
            [self.formModel save];
            
            [HUDController showCustomView:self clipTo:self.navigationController withLabel:@"Settings saved.." delayFor:1];
            
            PVSettings *output = [self.formModel object];
            [[NSUserDefaults standardUserDefaults] setObject:output.name forKey:RESTAURANT_NAME];
            [[NSUserDefaults standardUserDefaults] setObject:output.contactEmail forKey:CONTACT_EMAIL];
            [[NSUserDefaults standardUserDefaults] setBool:output.syncDropbox forKey:SYNC_DROPBOX__];
            [[NSUserDefaults  standardUserDefaults] setObject:output.language forKey:LANGUAGE];
            [[NSUserDefaults standardUserDefaults] setObject:output.inventoryCycleType forKey:INVENTORY_CYCLE];
            
        }];
        
        [self.formModel registerMapping:formMapping];
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        DDLogWarn(@"The object %@ changed model value to \"%@\" with key path: %@",object, value, keyPath);
    }];
    
    [self.formModel loadFieldsWithObject:pvSettings];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)tableViewCell:(StringInputTableViewCell *)cell didEndEditingWithString:(NSString *)value {
	DDLogWarn(@"%@ string changed to: '%@'", cell.textLabel.text, value);
}

@end
