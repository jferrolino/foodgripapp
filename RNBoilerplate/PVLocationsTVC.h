//
//  PVLocationsTVC.h
//  FoodGrip
//
//  Created by ENG002 on 12/12/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"

@interface PVLocationsTVC : UITableViewController

- (IBAction)revealMenu:(id)sender;

@end
