//
//  PVGlobals.m
//  FoodGrip
//
//  Created by ENG002 on 1/23/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVGlobals.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "PVEditableTableDataRowItem.h"
#import <CommonCrypto/CommonDigest.h>
@interface PVGlobals ()


@property (nonatomic, strong) NSMutableArray *allLocations;
@property (nonatomic, strong) NSMutableArray *categories;

@property (nonatomic, strong) NSMutableArray *orderUnits;
@property (nonatomic, strong) NSMutableArray *inventoryUnits;

@end

@implementation PVGlobals


static PVGlobals *sharedSingleton;

@synthesize allLocations = _allLocations;
@synthesize categories = _categories;

@synthesize orderUnits = _orderUnits;
@synthesize inventoryUnits = _inventoryUnits;

+ (id)initialize
{
    static BOOL initialized = NO;
    if(!initialized)
    {
        initialized = YES;
        sharedSingleton = [[PVGlobals alloc] init];
    }
    return sharedSingleton;
}

+ (NSArray *) retrieveAllLocations
{
    /*if( [[sharedSingleton allLocations] count] > 0){
        return [sharedSingleton allLocations];
    }*/
    
    [self reloadToGetAllLocations];
    return [sharedSingleton allLocations];
}

+ (void) reloadToGetAllLocations
{
    [sharedSingleton setAllLocations:nil];
    
    NSManagedObjectContext *context = [[SDCoreDataController sharedInstance] newManagedObjectContext] ;
    [context performBlockAndWait:^{
        
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
        [request setSortDescriptors:[NSArray arrayWithObject:
                                     [NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES]]];
        [request setResultType:NSDictionaryResultType];
        [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        
        NSArray *locs = [context executeFetchRequest:request error:&error];
        
        [[PVGlobals initialize] setAllLocations:[locs mutableCopy]];
        
        if(error){
            DDLogError(@"%s => error:%@",__func__,error);
        }
    }];
}

#pragma mark - Categories

+ (NSArray * ) retrieveCategories
{
    /*if ( [[sharedSingleton categories] count] > 0){
        return [sharedSingleton categories];
    }*/
    [self reloadToGetRecentCategories];
    return [sharedSingleton categories];
}

#define ALL_ITEMS @"All Items"
#define CATEGORY_COLUMN @"category"
#define ITEM_TABLE @"Item"

+ (void) reloadToGetRecentCategories
{
    NSManagedObjectContext *moc = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    [moc performBlockAndWait:^{
        [moc reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:ITEM_TABLE];
        
        [request setResultType:NSDictionaryResultType];
        [request setReturnsDistinctResults:YES];
        [request setPropertiesToFetch:@[CATEGORY_COLUMN]];
        
        NSArray *categories = [moc executeFetchRequest:request error:&error];
        
        BOOL allItemsIsFound = NO;
        NSMutableArray *holder = [NSMutableArray array];
        for (int i =0 ; i < [categories count]; i++){
            NSString *category = [(NSDictionary *)[categories objectAtIndex:i] objectForKey:CATEGORY_COLUMN];
            if(category){
                if([category isEqualToString:ALL_ITEMS]){
                    [holder insertObject:category atIndex:0];
                    allItemsIsFound = YES;
                } else {
                    [holder addObject:category];
                }
            }
        }
        
        // JOHN : Just to make sure that All Items is listed.
        if(!allItemsIsFound){
            NSString *allItemCategory = ALL_ITEMS;
            if(holder){
                [holder insertObject:allItemCategory atIndex:0];
            }
        }
        
        // Set all categories to User Defaults.
        if([holder count] > 0){
            // JOHN: I've provided two options to retrieve categories for compatibility purposes only.
            [[NSUserDefaults standardUserDefaults] setObject:holder forKey:ITEM_CATEGORIES];
            [[PVGlobals initialize] setCategories:holder];
        }
    }];
}

#define ENDED_AT_IS_STILL_ONGOING @"On going.."

+ (NSString *) formatDateToString:(NSDate *)dateValue
{
    
    if(!dateValue){
        return ENDED_AT_IS_STILL_ONGOING;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:dateValue];
    return formattedDateString;
}


+ (UIView *) commonCellBackgroundView:(UITableViewCell *)cell
{
    UIImageView *bgImageView = [[UIImageView alloc]initWithFrame:cell.frame];
    [bgImageView setImage:[UIImage tallImageNamed:@"ipad-list-element.png"]];
    
    return bgImageView;
}

+ (UIColor *) commonViewBackgroundColor
{
    return [UIColor colorWithPatternImage:[UIImage tallImageNamed:@"ipad-BG-pattern.png"]];
}

+ (UIButton *) menuBarButtonThemeFromTarget:(id)target withAction:(SEL)actionMethod
{
    UIImage *myImage = [UIImage imageNamed:@"ButtonMenu"];
    UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton setImage:myImage forState:UIControlStateNormal];
    myButton.showsTouchWhenHighlighted = YES;
    myButton.frame = CGRectMake(0.0, 0.0, myImage.size.width, myImage.size.height);
    
    [myButton addTarget:target action:actionMethod forControlEvents:UIControlEventTouchUpInside];
    
    return myButton;
}


#pragma mark - From Splitwise blog : http://blog.splitwise.com/2012/02/08/developing-multi-currency-support-for-ios/

+ (NSDictionary *)divideEvenlyWithRemainder:(NSDecimalNumber *)numerator
                                         by:(NSDecimalNumber *)denominator
                                forCurrency:(NSString *)currencyCode {
    int32_t precision;
    double rounding;
    CFNumberFormatterGetDecimalInfoForCurrencyCode((__bridge CFStringRef)currencyCode, &precision, &rounding);
    NSDecimalNumberHandler *roundDown = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                                               scale:precision
                                                                                    raiseOnExactness:NO
                                                                                     raiseOnOverflow:NO
                                                                                    raiseOnUnderflow:NO
                                                                                 raiseOnDivideByZero:NO];
    
    NSDecimalNumber *roundDecimal = nil;
    NSDecimalNumber *result = nil;
    //If we have rounding, we need to convert to a count of the smallest physical currency
    if (rounding > 0) {
        //As far as I am aware, the smallest rounding is 0.005, added one more sig fig for safety
        roundDecimal = [NSDecimalNumber decimalNumberWithMantissa:rounding * 10000
                                                         exponent:-4
                                                       isNegative:NO];
        //Because we are now working with whole units, we set scale to 0
        roundDown = [NSDecimalNumberHandler decimalNumberHandlerWithRoundingMode:NSRoundDown
                                                                           scale:0
                                                                raiseOnExactness:NO
                                                                 raiseOnOverflow:NO
                                                                raiseOnUnderflow:NO
                                                             raiseOnDivideByZero:NO];
        
        //this will give us a count of the smallest available physical currency for countries with
        //coins that are larger or smaller than a "penny"
        result = [numerator decimalNumberByDividingBy:roundDecimal withBehavior:roundDown];
        result = [result decimalNumberByDividingBy:denominator
                                      withBehavior:roundDown];
        //Convert back to currency value instead of count
        result = [result decimalNumberByMultiplyingBy:roundDecimal];
    } else {
        result = [numerator decimalNumberByDividingBy:denominator
                                         withBehavior:roundDown];
    }
    
    //Have to use original here in case the user decides to put in units smaller than all4owed for some reason
    NSDecimalNumber *remainder = [numerator decimalNumberBySubtracting:[result decimalNumberByMultiplyingBy:denominator]];
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            result, @"amount",
            remainder, @"remainder",
            nil];
    
}

+ (NSString *) convertDecimalDictionaryToString:(NSDictionary *)decimalDictionary
{
    return [NSString stringWithFormat:@"%@.%@",[decimalDictionary objectForKey:@"amount"],[decimalDictionary objectForKey:@"remainder"]];
}

+ (NSString *) formatCurrencyToString:(NSDecimalNumber *)value
                     /*withCurrencyCode:(NSString *)code*/
{
    NSString *currencyCode =  [self retrieveCurrencyCodeBasedOnCurrentLocale];//(code) ? code : @"USD";
    
    NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                currencyCode, NSLocaleCurrencyCode,
                                [[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGE] , NSLocaleLanguageCode,
                                nil];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:
                        [NSLocale localeIdentifierFromComponents:localeInfo]];
    
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setNumberStyle:NSNumberFormatterCurrencyStyle];
    [fmt setLocale:locale];
    
    return [fmt stringFromNumber:value/*[NSNumber numberWithFloat:<#(float)#>:value]*/];
}

+ (NSString *) retrieveCurrencyCodeBasedOnCurrentLocale
{
    return [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
}

+ (NSDictionary *) retrieveAppCurrentLocaleInfo
{
    NSString *currencyCode = [[NSLocale currentLocale] objectForKey:NSLocaleCurrencyCode];
    
    NSDictionary *localeInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                currencyCode, NSLocaleCurrencyCode,
                                [[NSUserDefaults standardUserDefaults] objectForKey:LANGUAGE] , NSLocaleLanguageCode,
                                nil];

    return localeInfo;
}

# pragma mark  - Inventory ORDER UNIT

+ (NSArray * ) retrieveOrderUnits
{
    /*if ( [[sharedSingleton orderUnits] count] > 0){
        return [sharedSingleton orderUnits];
    }*/
    [self reloadToGetRecentOrderUnits];
    return [sharedSingleton orderUnits];
}

#define ALL_ITEMS @"All Items"
#define ORDER_UNIT_COLUMN @"orderUnit"
#define ITEM_TABLE @"Item"

+ (void) reloadToGetRecentOrderUnits
{
    NSManagedObjectContext *moc = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    [moc performBlockAndWait:^{
        [moc reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:ITEM_TABLE];
        
        [request setResultType:NSDictionaryResultType];
        [request setReturnsDistinctResults:YES];
        [request setPropertiesToFetch:@[ORDER_UNIT_COLUMN]];
        
        NSArray *orderUnits = [moc executeFetchRequest:request error:&error];
        NSMutableArray *holder = [NSMutableArray array];
        
        for (int i =0 ; i < [orderUnits count]; i++){
            NSString *orderUnit = [(NSDictionary *)[orderUnits objectAtIndex:i] objectForKey:ORDER_UNIT_COLUMN];
            if(orderUnit){
                [holder addObject:orderUnit];
            }
        }
        
        // Set all categories to User Defaults.
        if([holder count] > 0){
            // JOHN: I've provided two options to retrieve categories for compatibility purposes only.
            [[NSUserDefaults standardUserDefaults] setObject:holder forKey:ITEM_ORDER_UNITS];
            [[PVGlobals initialize] setOrderUnits:holder];
        }
    }];
}

# pragma mark - Inventory UNIT
+ (NSArray * ) retrieveItemUnits
{
    /*if ( [[sharedSingleton orderUnits] count] > 0){
        return [sharedSingleton orderUnits];
    }*/
    [self reloadToGetRecentOrderUnits];
    return [sharedSingleton orderUnits];
}

#define ALL_ITEMS @"All Items"
#define ITEM_UNIT_COLUMN @"inventoryUnit"
#define ITEM_TABLE @"Item"

+ (void) reloadToGetRecentItemUnits
{
    NSManagedObjectContext *moc = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    [moc performBlockAndWait:^{
        [moc reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:ITEM_TABLE];
        
        [request setResultType:NSDictionaryResultType];
        [request setReturnsDistinctResults:YES];
        [request setPropertiesToFetch:@[ITEM_UNIT_COLUMN]];
        
        NSArray *itemUnits = [moc executeFetchRequest:request error:&error];
        NSMutableArray *holder = [NSMutableArray array];
        
        for (int i =0 ; i < [itemUnits count]; i++){
            NSString *itemUnit = [(NSDictionary *)[itemUnits objectAtIndex:i] objectForKey:ITEM_UNIT_COLUMN];
            if(itemUnit){
                [holder addObject:itemUnit];
            }
        }
        
        // Set all categories to User Defaults.
        if([holder count] > 0){
            // JOHN: I've provided two options to retrieve categories for compatibility purposes only.
            [[NSUserDefaults standardUserDefaults] setObject:holder forKey:ITEM_UNITS];
            [[PVGlobals initialize] setInventoryUnits:holder];
        }
    }];
}

+ (NSArray *)defineTableColumnHeaderLabels:(NSArray *)labels withCorrespondingTargetedWidths:(NSDictionary *)labelsWithWidth withSize:(CGSize)size
{
    NSMutableArray *labelComponents = [NSMutableArray array];
    CGFloat height = size.height;
    //CGFloat width = size.width;
    
    if(labels && labelsWithWidth){
        
        for(NSString *label in labels){
            
            CGFloat targetWidth = [[labelsWithWidth valueForKey:label] floatValue];
            
            PVEditableTableDataRowItem *columnLabelControl = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                                 selections:nil
                                                                                                           selectionListKey:nil
                                                                                                                   baseSize:CGSizeMake(targetWidth,height)
                                                                                                                  canResize:NO
                                                                                                                normalImage:nil
                                                                                                              selectedImage:nil
                                                                                                               controlLabel:label
                                                                                                               buttonTarget:nil
                                                                                                               buttonAction:nil];
            [columnLabelControl toggleEditingMode:NO];
            [labelComponents addObject:columnLabelControl];
        }
    }
    return labelComponents;
}

+ (NSArray *) customTableColumnHeader:(PVEditableTableDataRow *)container withSize:(CGSize)size andLabels:(NSArray *)labels withCorrespondingWidth:(NSDictionary *)labelkeyWithWidths
{    
    return [self defineTableColumnHeaderLabels:labels withCorrespondingTargetedWidths:labelkeyWithWidths withSize:size];
}

+ (NSDecimalNumber *) numberToDecimalNumber:(NSNumber *) number
{
    NSDictionary *localeInfo = [self retrieveAppCurrentLocaleInfo];
    
    return [NSDecimalNumber decimalNumberWithString:[number descriptionWithLocale:localeInfo] locale:localeInfo];
}



#pragma mark  -  from http://stackoverflow.com/questions/2633801/generate-a-random-alphanumeric-string-in-cocoa
NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
NSString *noLowerCase = @"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

+(NSString *) genRandStringLength: (int)len
{
    NSMutableString *randomString = [NSMutableString stringWithCapacity:len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [noLowerCase characterAtIndex: arc4random() % [noLowerCase length]]];
    }
    
    return randomString;
}

#define INVENTORY_ITEM_NUMBER_COLUMN @"itemNumber"

+ (NSString *) randStringUniqueInDBWithLength:(int)len forEntityName:(NSString *)entityName
{
    NSManagedObjectContext *context = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    
    NSString *generated = [self genRandStringLength:len];
    
    if(entityName){
        NSFetchRequest *checkRequest = [[NSFetchRequest alloc] initWithEntityName:entityName];
        NSPredicate *itemNumberCheck = [NSPredicate predicateWithFormat:@"%@ == %@",INVENTORY_ITEM_NUMBER_COLUMN , generated];
        [checkRequest setPredicate:itemNumberCheck];
        
        NSError *checkError = nil;        
        BOOL isFound = [[context executeFetchRequest:checkRequest error:&checkError] count] > 0;
        
        if(isFound){
            return [self randStringUniqueInDBWithLength:len forEntityName:entityName];
        }
    }
    
    return generated;
}

+ (NSString *)removeNonNumericalValuesFromString:(NSString*)toEvaluate
{
    NSString *cleaned = toEvaluate;
    
    return cleaned;
}

#pragma mark - From

+ (NSString *) generateHashForImageData:(NSData *) imageData { 

    unsigned char result[16];
    CC_MD5([imageData bytes], [imageData length], result);
    NSString *imageHash = [NSString stringWithFormat:
                           @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                           result[0], result[1], result[2], result[3], 
                           result[4], result[5], result[6], result[7],
                           result[8], result[9], result[10], result[11],
                           result[12], result[13], result[14], result[15]
                           ];
    return imageHash;
}

#pragma mark - 
+ (UIView *) generateGridContentView:(CGRect)bounds withLabels:(NSArray *)labels
{
    CGFloat targetWidth = bounds.size.width;
    CGFloat targetHeight = bounds.size.height / 3.0f;
    //CGFloat targetPtY = targetHeight * 0.25;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, targetWidth, targetHeight)];
    
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;

    UIView *gridLabelView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, targetWidth, targetHeight)];
    gridLabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;


    if(labels && [labels count] > 0)
        label.text = labels[0];
    
    [label setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont boldSystemFontOfSize:20]];
    [label setShadowColor:[UIColor whiteColor]];
    [label setShadowOffset:CGSizeMake(0, 1)];
    [label setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0.35f]];
    [label.layer setCornerRadius:8];
    
    [gridLabelView addSubview:label];

    return gridLabelView;
}

+ (NSArray *) deduplicateImages:(NSArray *)allImages
{
    NSMutableDictionary *imageWithHash = [NSMutableDictionary dictionary];
    
    if(allImages){
        for(UIImage *image in allImages){
            
            NSData *imageData = UIImagePNGRepresentation(image);
            NSString *hashKey = [PVGlobals generateHashForImageData:imageData];
            
            //DDLogVerbose(@"The hashKey generated:%@", hashKey);
            
            
            if([imageWithHash objectForKey:hashKey] == nil){
                [imageWithHash setValue:image forKey:hashKey];
            }
        }
    }
    
    DDLogVerbose(@"All the keys in the dictionary %@ with count of %i", [imageWithHash allKeys], [imageWithHash count]);
    return [imageWithHash allValues];
}

#warning - John's Notice: You might want to put this to a class where it make sense..

+ (NSArray *) prepareBannerWith:(NSArray *)currentData forView:(UIView *)thisView
{
    
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:[currentData count]];
    
    for( NSManagedObject *obj in currentData){
        
        UIView *view = [[UIView alloc] initWithFrame:thisView.bounds];
        UIGraphicsBeginImageContext(view.frame.size);
        
        if( obj && [obj isKindOfClass:[Location class]]){
            Location *location = (Location *)obj;
            ReferencePhotoData *photoData = nil;
            
            for(ReferencePhotoData *photo in [location containPhotos]){
                if(photo){
                    photoData = photo;
                }
            }
            
            if(photoData){
                UIImage *locationPhoto = [UIImage imageWithData:[photoData bannerPhotoData]];
                
                if(locationPhoto){
                    [locationPhoto drawInRect:view.bounds];
                }
            }
        }

        if( obj && [obj isKindOfClass:[MenuEntree class]]){
            MenuEntree *entree = (MenuEntree *)obj;
            ReferencePhotoData *photoData = nil;
            
            for(ReferencePhotoData *photo in [entree containPhotos]){
                if(photo){
                    photoData = photo;
                }
            }
            
            if(photoData){
                UIImage *ingredientPhoto = [UIImage imageWithData:[photoData bannerPhotoData]];
                
                if(ingredientPhoto){
                    [ingredientPhoto drawInRect:view.bounds];
                }
            }
        }

        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        [images addObject:image];
    }
    return (images != nil && [images count] > 0) ? [images copy] : nil;
}

@end
