//
//  PVMultiSelectionTVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/13/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MultiSelectionDelegate <NSObject>

- (void)caller:(id)sender withSelectedItems:(NSArray *)selected;

@end

@interface PVMultiSelectionTVC : UITableViewController {
    NSMutableArray *_selectedMarks;
}

@property (nonatomic) NSArray *dataSource;
@property (nonatomic, strong) NSMutableArray *selectedMarks;
@property (nonatomic, strong) NSString *entityName;

@property (nonatomic, weak ) id<MultiSelectionDelegate> delegate;

- (void) loadRecordsFromCoreData;

- (void) alreadyInSelectedMarks:(NSManagedObject *)managedObject;

- (BOOL) isInSelectedMarks:(NSManagedObject *)managedObject;

@end
