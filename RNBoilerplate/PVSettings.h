//
//  PVSettings.h
//  FoodGrip
//
//  Created by ENG002 on 1/15/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

enum CONTROL_NUMBER_GENERATION_TYPE
{
    AUTO,
    USER_SUPPLIED
};

@interface PVSettings : NSObject

@property (nonatomic, strong ) NSString * name;
@property (nonatomic, strong) NSString * inventoryCycleType;
@property (nonatomic) BOOL toShowTotal;
@property (nonatomic, strong ) NSString *contactEmail;
@property (nonatomic, strong ) NSString *language;
@property (nonatomic, strong ) NSLocale *currency;
@property (nonatomic) BOOL syncDropbox;
@property (nonatomic, strong) NSString *controlNumberGeneration;

@end
