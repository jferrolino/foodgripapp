//
//  PVCameraViewController.m
//  FoodGrip
//
//  Created by ENG002 on 1/10/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//
/*#define COOKBOOK_PURPLE_COLOR	[UIColor colorWithRed:0.20392f green:0.19607f blue:0.61176f alpha:1.0f]
#define BARBUTTON(TITLE, SELECTOR) 	[[UIBarButtonItem alloc] initWithTitle:TITLE style:UIBarButtonItemStylePlain target:self action:SELECTOR]
#define RESIZABLE(_VIEW_)   [_VIEW_ setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth]
*/
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#import "PVCameraViewController.h"

@interface PVCameraImageHelper () 

@end

@implementation PVCameraViewController 

@synthesize imageCollectionController = _imageCollectionController;
@synthesize photos = _photos;

@synthesize cameraDelegate;

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(!(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])){
        
        self.view.frame = self.parentViewController.view.bounds;
    }
    return self;
}

- (id) initWithFrame:(CGRect) frame
{
    self = [self init];
    if(self){
        self.view.frame = frame;
    }
    return self;
}

- (NSMutableArray *) photos
{
    if(!_photos){
        _photos = [NSMutableArray array];
    }
    return _photos;
}

- (void) switch: (id) sender
{
    [helper switchCameras];
}

// Start or Pause
- (void) toggle:(id)sender
{
   if(helper.session.isRunning)
    {
        self.navigationItem.rightBarButtonItem = BARBUTTON(@"Resume", @selector(toggle:));
        [helper stopRunningSession];
    } else {
        self.navigationItem.rightBarButtonItem = BARBUTTON(@"Pause", @selector(toggle:));
        [helper startRunningSession];
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    imageView.frame = self.view.bounds;
    imageView.center = CGRectGetCenter(self.view.bounds);
}

- (void) viewDidLayoutSubviews
{
    [helper layoutPreviewInView:imageView];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}


#pragma mark - Image Picker

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    if(!image){
        image = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    // This is for single photo upload.
    imageView.image = image;
    
    // This is for supporting multi photo upload.
    [self.photos addObject:image];
    
    if(IS_IPHONE)
    {
        [self dismissViewControllerAnimated:YES completion:NULL];
    } else {
        [popoverController dismissPopoverAnimated:YES];
    }
    imagePickerController = nil;
    
    [self.collectionView reloadData];
}

- (void) viewWillDisappear:(BOOL)animated
{
    if(self.cameraDelegate){
                
        if([self.cameraDelegate respondsToSelector:@selector(uploadedImages:)]){
            //[self.cameraDelegate populatedImageView:imageView];
            [self.cameraDelegate uploadedImages:[self.photos copy]];  
        }
        
        if([self.cameraDelegate respondsToSelector:@selector(uploadedImage:)]){
            [self.cameraDelegate uploadedImage:[imageView.image copy]]; // for single
        }
    }

    self.photos = nil;
    self->imageView = nil;

}

- (void)viewDidDisappear:(BOOL)animated
{
    self.photos = nil;
    
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
    imagePickerController = nil;
}


- (void) popoverControllerDidDismissPopover:(UIPopoverController *)aPopoverController
{
    imagePickerController = nil;
    popoverController = nil;
}

- (void) pickImage:(id) sender
{
    // Create an initialize the picker
    imagePickerController = [[UIImagePickerController alloc]init];
    imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePickerController.allowsEditing = editSwitch.isOn;
    
    imagePickerController.delegate = self;
    
    if(IS_IPHONE)
    {
        [self presentViewController:imagePickerController animated:YES completion:NULL];
    } else {
        if(popoverController) [popoverController dismissPopoverAnimated:NO];
        popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        popoverController.delegate = self;
        [popoverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

- (void) snapImage:(id) sender
{
    // Create and initialize the picker
    imagePickerController = [[UIImagePickerController alloc] init];

    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePickerController.delegate = self;

    if(IS_IPHONE){
        //[[self presentModalViewController:imagePickerController animated:YES];
        [self presentViewController:imagePickerController animated:YES completion:NULL];
        

    } else {
        if(popoverController) [popoverController dismissPopoverAnimated:NO];
        popoverController = [[UIPopoverController alloc] initWithContentViewController:imagePickerController];
        popoverController.delegate = self;
        [popoverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem
            permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    }
}


- (void) loadView
{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    RESIZABLE(self.view);
    
    
    editSwitch = [[UISwitch alloc] init];
    self.navigationItem.titleView = editSwitch;
    
    self.navigationController.navigationBar.tintColor = COOKBOOK_PURPLE_COLOR;
    //self.navigationItem.rightBarButtonItem  = BARBUTTON(@"Pick", @selector(pickImage:));
    
	//self.navigationController.navigationBar.tintColor = COOKBOOK_PURPLE_COLOR;
    
    // Switch between cameras
    if ([PVCameraImageHelper numberOfCameras] > 1)
        self.navigationItem.leftBarButtonItem = BARBUTTON(@"Switch", @selector(switch:));
    
    // Start or Pause
    //self.navigationItem.rightBarButtonItem = BARBUTTON(@"Pause", @selector(toggle:));
    
    NSMutableArray *barButtons = [NSMutableArray array];

    UIBarButtonItem *pick = BARBUTTON(@"Pick", @selector(pickImage:));
    [barButtons addObject:pick];

    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
     UIBarButtonItem *snap = BARBUTTON(@"Camera", @selector(snapImage:)); 
     [barButtons addObject:snap];  
    }
    
    //UIBarButtonItem *pause = BARBUTTON(@"Pause", @selector(toggle:));
    
    self.navigationItem.rightBarButtonItems = barButtons;
    
    // The image view holds the live preview
    imageView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    RESIZABLE(imageView);
    
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CollectionCell"];
    //self.collectionView
    
    helper = [PVCameraImageHelper helperWithCamera:kCameraFront];
    [helper startRunningSession];
    [helper embedPreviewInView:imageView];
}

- (void) viewDidLoad
{
    [editSwitch setOn:YES];
}

//
#pragma mark - UICollectionViewDelegate
//
- (void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void) collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

//
#pragma mark - UICollectionViewFlowLayoutDelegate
//
- (CGSize) collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
   sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGSize retval = CGSizeMake(100,100);
    retval.height += 35;
    retval.width += 35;
    
    return retval;
}

- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout *)collectionViewLayout
         insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(50, 20, 50, 20);
}

#pragma mark - UICollectionViewDatasource

- (UICollectionViewCell *) collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //static CellIdentifier *
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    
    UIImage *image = ([self.photos count] > 0) ? [self.photos objectAtIndex:[indexPath row]] : nil;
    cell.backgroundView = [[UIImageView alloc] initWithImage:image];
    return cell;
}

- (int) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.photos count];
}

@end

/*
#import "PVCameraViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PVCameraViewController ()

@property (nonatomic,strong) UISwitch *editSwitch;

@end

@implementation PVCameraViewController

@synthesize imageView;
@synthesize toolbar;
@synthesize popoverController;

@synthesize editSwitch = _editSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) loadView
{
    [super loadView];

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *cameraButton = [[UIBarButtonItem alloc] initWithTitle:@"Camera" style:UIBarButtonItemStyleBordered target:self action:@selector(useCamera:)];
    
    UIBarButtonItem *cameraRollButton = [[UIBarButtonItem alloc] initWithTitle:@"Camera Roll" style:UIBarButtonItemStyleBordered target:self action:@selector(userCameraRoll:)];
    
    NSArray *items = [NSArray arrayWithObjects:cameraButton, cameraRollButton, nil];
    [toolbar setItems:items animated:NO];
}

- (void) useCamera:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        imagePicker.allowsEditing = NO;
        
        //[self presentModalViewController:imagePicker animated:YES];
        
        [self presentViewController:imagePicker animated:YES completion:NULL];
        newMedia = YES;
    }
}



- (void) userCameraRoll: (id)sender
{
    if([self.popoverController isPopoverVisible]){
        [self.popoverController dismissPopoverAnimated:YES];
    } else {
        if( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]){
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
            imagePicker.delegate = self;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagePicker.mediaTypes = [NSArray arrayWithObjects:(NSString *) kUTTypeImage, nil];
            
            imagePicker.allowsEditing = NO;
            
            self.popoverController = [[UIPopoverController alloc]initWithContentViewController:imagePicker];
            
            popoverController.delegate = self;
            
            [self.popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
            
            newMedia = NO;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.  
}

///////////////////////////////////////////////////////
#pragma mark - Delegate Methods
//////////////////////////////////////////////////////
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.popoverController dismissPopoverAnimated:YES];
    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    
    //[self dismissModalViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    if ([mediaType isEqualToString:(NSString *) kUTTypeImage ]){
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        imageView.image = image;
        
        if(newMedia)
            UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:finishedSavingWithError:contextInfo:), nil);
    } else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]){
        // FOr future support, maybe.
    }
}

- (void) image:(UIImage *)image finishedSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if(error){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Save failed" message:@"Failed to save Image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [alert show];
    }
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //[self dismissModalViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
*/
