//
//  PVMenuEntreeFormWithParallaxVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/11/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMenuEntreeFormTVC.h"
#import "PVTouchAwareParallaxPicturesVC.h"

@interface PVMenuEntreeFormWithParallaxVC : PVTouchAwareParallaxPicturesVC <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) UITableView *entreeTableForm;

@property (nonatomic,weak) id<MenuEntreeFormDelegate> delegate;
@property (nonatomic,weak) id<MenuEntreeFormDataSource> datasource;

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic,strong) FKFormModel *formModel;
@property (nonatomic,strong) MenuEntree *entree;

- (id) initWithFrame:(CGRect) frame underNavigationController:(UINavigationController *)navController;
- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil underNavigationController:(UINavigationController *) navController;

@end
