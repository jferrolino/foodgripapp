//
//  MenuEntree.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Item, Location, ReferencePhotoData;

@interface MenuEntree : NSManagedObject

@property (nonatomic, retain) NSString * controlNumber;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSDecimalNumber * sellingPrice;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * preparationProcedure;
@property (nonatomic, retain) NSString * cookingProcedure;
@property (nonatomic, retain) NSString * orderingProcedure;
@property (nonatomic, retain) NSString * presentation;
@property (nonatomic, retain) NSString * comments;
@property (nonatomic, retain) NSOrderedSet *containIngredients;
@property (nonatomic, retain) NSOrderedSet *containPhotos;
@property (nonatomic, retain) NSOrderedSet *usedInLocations;

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context;

@end

@interface MenuEntree (CoreDataGeneratedAccessors)

- (void)insertObject:(Item *)value inContainIngredientsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainIngredientsAtIndex:(NSUInteger)idx;
- (void)insertContainIngredients:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainIngredientsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainIngredientsAtIndex:(NSUInteger)idx withObject:(Item *)value;
- (void)replaceContainIngredientsAtIndexes:(NSIndexSet *)indexes withContainIngredients:(NSArray *)values;
- (void)addContainIngredientsObject:(Item *)value;
- (void)removeContainIngredientsObject:(Item *)value;
- (void)addContainIngredients:(NSOrderedSet *)values;
- (void)removeContainIngredients:(NSOrderedSet *)values;
- (void)insertObject:(ReferencePhotoData *)value inContainPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainPhotosAtIndex:(NSUInteger)idx;
- (void)insertContainPhotos:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainPhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainPhotosAtIndex:(NSUInteger)idx withObject:(ReferencePhotoData *)value;
- (void)replaceContainPhotosAtIndexes:(NSIndexSet *)indexes withContainPhotos:(NSArray *)values;
- (void)addContainPhotosObject:(ReferencePhotoData *)value;
- (void)removeContainPhotosObject:(ReferencePhotoData *)value;
- (void)addContainPhotos:(NSOrderedSet *)values;
- (void)removeContainPhotos:(NSOrderedSet *)values;
- (void)insertObject:(Location *)value inUsedInLocationsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromUsedInLocationsAtIndex:(NSUInteger)idx;
- (void)insertUsedInLocations:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeUsedInLocationsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInUsedInLocationsAtIndex:(NSUInteger)idx withObject:(Location *)value;
- (void)replaceUsedInLocationsAtIndexes:(NSIndexSet *)indexes withUsedInLocations:(NSArray *)values;
- (void)addUsedInLocationsObject:(Location *)value;
- (void)removeUsedInLocationsObject:(Location *)value;
- (void)addUsedInLocations:(NSOrderedSet *)values;
- (void)removeUsedInLocations:(NSOrderedSet *)values;
@end
