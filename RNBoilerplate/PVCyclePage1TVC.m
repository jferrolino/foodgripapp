//
//  PVCyclePage1TVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/22/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCyclePage1TVC.h"
#import "SDCoreDataController.h"
#import "FormKit.h"
#import "SDSyncEngine.h"
#import "Location.h"
#import "PVGlobals.h"

@interface PVCyclePage1TVC ()
@property (nonatomic, strong) NSArray *locations;
@end

@implementation PVCyclePage1TVC

@synthesize managedObjectContext = _managedObjectContext;
@synthesize locations;
@synthesize cycle;
@synthesize formModel;
@synthesize delegate;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void) initialize
{
    if(self.dataSource){
        self.managedObjectContext = [self.dataSource useThisManagedObjectContextForPage1];
        self.cycle = [self.dataSource useThisCycleInstanceForPage1];
    }
}

- (void)viewDidLoad
{
    if([self managedObjectContext] == nil)
        self.managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    
    
    self.formModel = [FKFormModel formTableModelForTableView:self.tableView
                                        navigationController:self.navigationController];
    
    //  The cycle will be instantiated initially by the PageContainer
    if([self cycle] == nil ){
         Cycle *l_cycle = [[Cycle alloc]initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"Cycle" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
        self.cycle = l_cycle;
    }

    [FKFormMapping mappingForClass:[Cycle class] block:^(FKFormMapping *formMapping) {
        [formMapping sectionWithTitle:@"Inventory Cycle Info" footer:@"Thank you!" identifier:@"Foodgrip inventory cycle info"];
        
        [formMapping mapAttribute:@"locationName"
                            title:@"Location"
                     showInPicker:YES
                selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
            
                    NSArray *arrLocations = [PVGlobals retrieveAllLocations];
                    NSMutableArray *names = [NSMutableArray array];
                    
                    for(int index = 0; index < [arrLocations count]; index ++){
                        [names addObject:[[arrLocations objectAtIndex:index] valueForKey:@"name"]];
                    }
                    return (names == nil ||  ([names count] == 0))? [NSArray arrayWithObjects:@"location 1", @"location 2" , nil]: names;
                } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
                    return value;
                } labelValueBlock:^id(id value, id object) {
                    return value;
        }];
        
        [formMapping mappingForAttribute:@"startedAt"
                                   title:@"Start date"
                                    type:FKFormAttributeMappingTypeDate
                        attributeMapping:^(FKFormAttributeMapping *mapping) {
                                     
                                     mapping.dateFormat = @"MMM dd, yyyy";
                                 }];
        
        /*
        [formMapping mappingForAttribute:@"endedAt"
                                   title:@"Ended date"
                                    type:FKFormAttributeMappingTypeDate
                        attributeMapping:^(FKFormAttributeMapping *mapping) {
                            
                            mapping.dateFormat = @"MMM dd, yyyy";
                        }];

        */
        
        [formMapping buttonSave:@"Save" handler:^{
            
            DDLogWarn(@"%s : Savin..", __PRETTY_FUNCTION__);
            
            [[self delegate] onFormSubmission:self withObject:self.formModel.object];

        }];
        
        [self.formModel registerMapping:formMapping];
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        DDLogWarn(@"Did change model value");
    }];
    
    [self.formModel loadFieldsWithObject:self.cycle];
    
    
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
