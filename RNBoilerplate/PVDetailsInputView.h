//
//  PVDetailsInputView.h
//  FoodGrip
//
//  Created by ENG002 on 1/3/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Location.h"

@interface PVDetailsInputView : UIView

@property (nonatomic, strong) Location *location;

@end
