//
//  PVCyclePageVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/22/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCyclePageContainerVC.h"
#import "PVCyclePage1TVC.h"
#import "PVCyclePage2TVC.h"
#import "Cycle.h"
#import "CyclePerInventoryItem.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "UIAlertView+BlockExtensions.h"

@interface PVCyclePageContainerVC ()  <CyclePage1Delegate,CyclePage1DataSource,CyclePage2DataSource,CyclePage2Delegate,UIAlertViewDelegate>

@property (nonatomic, strong) Cycle *cycle;

@property (assign) BOOL pageControlUsed;
@property (assign) NSUInteger page;
@property (assign) BOOL rotating;

// My page view controllers
@property (nonatomic, strong) PVCyclePage1TVC *cyclePage1;
@property (nonatomic, strong) PVCyclePage2TVC *cyclePage2;

// These are the list of cycle peritems persisted in page 2. If
// DONE is not invoked, this should be removed from database.
@property (nonatomic, strong) Cycle *cycleInventory;
@property (nonatomic, strong) NSSet *cyclePerItemsObjectIDs;

- (void)loadScrollViewWithPage:(int)page;
@end

@implementation PVCyclePageContainerVC

@synthesize pageControlUsed = _pageControlUsed;
@synthesize page = _page;
@synthesize pageControl = _pageControl;
@synthesize scrollView = _scrollView;

@synthesize cycle = _cycle;
@synthesize managedObjectContext;

@synthesize cyclePage1;
@synthesize cyclePage2;

@synthesize cycleInventory;
@synthesize cyclePerItemsObjectIDs;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setManagedObjectContext:[[SDCoreDataController sharedInstance] newManagedObjectContext]];
    
    [self.scrollView setPagingEnabled:YES];
	[self.scrollView setScrollEnabled:YES];
	[self.scrollView setShowsHorizontalScrollIndicator:NO];
	[self.scrollView setShowsVerticalScrollIndicator:NO];
	[self.scrollView setDelegate:self];
    
    self.scrollView.bounds = CGRectMake(0.0, 0.0, 600, 320);

    [self setCycle:[NSEntityDescription insertNewObjectForEntityForName:@"Cycle" inManagedObjectContext:self.managedObjectContext]];
    
    // Add the controllers for the its children here..
    self.cyclePage1 = [self.storyboard  instantiateViewControllerWithIdentifier:@"CycleCreationPage1"];
    [cyclePage1 setDataSource:self];
    [cyclePage1 setDelegate:self];
    

	self.cyclePage2 = [self.storyboard instantiateViewControllerWithIdentifier:@"CycleCreationPage2"];
    [cyclePage2 setDataSource:self];
    
    [self addChildViewController:cyclePage1];
    [self addChildViewController:cyclePage2];
    
    [self.pageControl setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    [self.view setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
}

// SO that PVCyclesTVC can get a hold of cycleInventory and cyclerPerItems objects ids
- (void) manuallyRequestToGetTheseObjects
{
    // JOHN: Let's pass this to PVCycleTVC
    if(self.cycleContainerDelegate){
        [self.cycleContainerDelegate objectIDForCreatedInventoryCycle:[self.cycleInventory objectID]];
        [self.cycleContainerDelegate objectIDsToDelete:[self cyclePerItemsObjectIDs]];
    }
}

#pragma mark - public method

- (void) reloadPages
{
    if(self.cyclePage1){
        self.cyclePage1 = nil;
        // Add the controllers for the its children here..
        self.cyclePage1 = [self.storyboard  instantiateViewControllerWithIdentifier:@"CycleCreationPage1"];
        [self.cyclePage1 setDataSource:self];
        [self.cyclePage1 setDelegate:self];
    }
    
    if(self.cyclePage2){
        self.cyclePage2 = nil;
        self.cyclePage2 = [self.storyboard instantiateViewControllerWithIdentifier:@"CycleCreationPage2"];
        [self.cyclePage2 setDataSource:self];

    }
}

- (BOOL)automaticallyForwardAppearanceAndRotationMethodsToChildViewControllers
{
	return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
	_rotating = YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
	
	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [self.childViewControllers count], self.scrollView.frame.size.height);
	NSUInteger page = 0;
	for (viewController in self.childViewControllers) {
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * page;
		frame.origin.y = 0;
		viewController.view.frame = frame;
		page++;
	}
	
	CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * _page;
    frame.origin.y = 0;
	[self.scrollView scrollRectToVisible:frame animated:NO];
	
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	_rotating = NO;
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	[viewController didRotateFromInterfaceOrientation:fromInterfaceOrientation];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	
	for (NSUInteger i =0; i < [self.childViewControllers count]; i++) {
		[self loadScrollViewWithPage:i];
	}
	
	self.pageControl.currentPage = 0;
	_page = 0;
	[self.pageControl setNumberOfPages:[self.childViewControllers count]];
	
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewWillAppear:animated];
	}
	
	self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [self.childViewControllers count], self.scrollView.frame.size.height);
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	
	if ([self.childViewControllers count]) {
		UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
		if (viewController.view.superview != nil) {
			[viewController viewDidAppear:animated];
		}
	}
}

- (void)viewWillDisappear:(BOOL)animated {
	if ([self.childViewControllers count]) {
		UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
		if (viewController.view.superview != nil) {
			[viewController viewWillDisappear:animated];
		}
	}
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
	UIViewController *viewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	if (viewController.view.superview != nil) {
		[viewController viewDidDisappear:animated];
	}
    
    // Clears the pages contents. After closing the depthview.
    [self setCyclePage1:nil];
    [self setCyclePage2:nil];

	[super viewDidDisappear:animated];
}

- (void)loadScrollViewWithPage:(int)page {
    if (page < 0)
        return;
    if (page >= [self.childViewControllers count])
        return;
    
	// replace the placeholder if necessary
    UIViewController *controller = [self.childViewControllers objectAtIndex:page];
    if (controller == nil) {
		return;
    }
	
	// add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        controller.view.frame = frame;            
        [self.scrollView addSubview:controller.view];
    }
}

- (void)previousPage {
	if (_page - 1 > 0) {
        
		// update the scroll view to the appropriate page
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * (_page - 1);
		frame.origin.y = 0;
		
		UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
		UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page - 1];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		
		[self.scrollView scrollRectToVisible:frame animated:YES];
		
		self.pageControl.currentPage = _page - 1;
		// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
		_pageControlUsed = YES;
	}
}

- (void)nextPage {
	if (_page + 1 > self.pageControl.numberOfPages) {
		
		// update the scroll view to the appropriate page
		CGRect frame = self.scrollView.frame;
		frame.origin.x = frame.size.width * (_page + 1);
		frame.origin.y = 0;
		
		UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
		UIViewController *newViewController = [self.childViewControllers objectAtIndex:_page + 1];
		[oldViewController viewWillDisappear:YES];
        		[newViewController viewWillAppear:YES];
		
		[self.scrollView scrollRectToVisible:frame animated:YES];
		
		self.pageControl.currentPage = _page + 1;
		// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
		_pageControlUsed = YES;
	}
}

- (IBAction)changePage:(id)sender {
    int page = ((UIPageControl *)sender).currentPage;
	
	// update the scroll view to the appropriate page
    CGRect frame = self.scrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    
	UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
	UIViewController *newViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewWillDisappear:YES];
	[newViewController viewWillAppear:YES];
	
	[self.scrollView scrollRectToVisible:frame animated:YES];
	
	// Set the boolean used when scrolls originate from the UIPageControl. See scrollViewDidScroll: above.
    _pageControlUsed = YES;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
	UIViewController *oldViewController = [self.childViewControllers objectAtIndex:_page];
	UIViewController *newViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
	[oldViewController viewDidDisappear:YES];
	[newViewController viewDidAppear:YES];
	
	_page = self.pageControl.currentPage;
}

#pragma mark UIScrollViewDelegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // We don't want a "feedback loop" between the UIPageControl and the scroll delegate in
    // which a scroll event generated from the user hitting the page control triggers updates from
    // the delegate method. We use a boolean to disable the delegate logic when the page control is used.
    if (_pageControlUsed || _rotating) {
        // do nothing - the scroll was initiated from the page control, not the user dragging
        return;
    }
	
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.scrollView.frame.size.width;
    int page = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
	if (self.pageControl.currentPage != page) {
		UIViewController *oldViewController = [self.childViewControllers objectAtIndex:self.pageControl.currentPage];
		UIViewController *newViewController = [self.childViewControllers objectAtIndex:page];
		[oldViewController viewWillDisappear:YES];
		[newViewController viewWillAppear:YES];
		self.pageControl.currentPage = page;
		[oldViewController viewDidDisappear:YES];
		[newViewController viewDidAppear:YES];
		_page = page;
	}
}

// At the begin of scroll dragging, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _pageControlUsed = NO;
}

// At the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControlUsed = NO;
}

#define CYCLE_STARTED_AT_PROPERTY @"startedAt"
#define CYCLE_LOCATION_NAME_PROPERTY @"locationName"
#define CYCLE_STATUS_PROPERTY @"cycleStatus"
#define CYCLE_SYNC_STATUS_PROPERTY @"syncStatus"
#define INVENTORY_CYCLE_FOR_LOCATION_FOUND_TITLE @"Inventory for this location found."
#define INVENTORY_CYCLE_FOR_LOCATION_FOUND_MESSAGE @"This will close currently open inventory for this location. \n Do you want to:"
#define BUTTON_TITLE_PROCEED @"Proceed"
#define BUTTON_TITLE_CANCEL @"Cancel"

#pragma mark - Cycla Page 1 Delegate methods
- (void) onFormSubmission:(id)sender withObject:(id)object
{
	// JOHN: proceed to the next page after user clicks the save button
	//[self changePage:self];
    [[self pageControl] setCurrentPage:1];
    
    // update the cycle in this class
    if([object isKindOfClass:[Cycle class]]){
                
        Cycle *objCycle = (Cycle *) object;
        
        [[self managedObjectContext]reset];
        
		Cycle *newCycle = [NSEntityDescription insertNewObjectForEntityForName:@"Cycle" inManagedObjectContext:self.managedObjectContext];
        [newCycle  setValue:[objCycle valueForKey:CYCLE_STARTED_AT_PROPERTY]
                     forKey:CYCLE_STARTED_AT_PROPERTY];
        
        /*[newCycle  setValue:[objCycle valueForKey:@"endedAt"]
                     forKey:@"endedAt"];*/
        
        [newCycle  setValue:[objCycle valueForKey:CYCLE_LOCATION_NAME_PROPERTY]
                     forKey:CYCLE_LOCATION_NAME_PROPERTY];
        
        [newCycle setValue:[NSNumber numberWithInt:CYCLE_STATUS_ACTIVE]  forKey:CYCLE_STATUS_PROPERTY];
        [newCycle setValue:[NSNumber numberWithInt:SDObjectCreated] forKey:CYCLE_SYNC_STATUS_PROPERTY];
        
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"name like %@", [newCycle valueForKey:@"locationName"]]];
    
        NSError *error = nil;
        NSArray *fromFetch = [self.managedObjectContext executeFetchRequest:request error:&error];        
        
        if(error){
            DDLogError(@"%s : Error :%@", __PRETTY_FUNCTION__,error);
        }
        
        DDLogVerbose(@"The fetch result for location: %@", fromFetch);
        Location *loadedLocation = ([fromFetch count] <= 0 )? nil : [fromFetch objectAtIndex:0];

        if(loadedLocation){
            [newCycle  setValue:loadedLocation
                         forKey:@"forLocation"];
        }
        
        // JOHN: check first the database if the same cycle
        // is already in the database with the same / overlapping start and
        // end date for the same location.
        // (1) If this happened, ask user if he/she wants to proceed.
        // (2) If decided to proceed, notify the user that the previous cycle will be CLOSED

        NSFetchRequest *checkRequest = [[NSFetchRequest alloc] initWithEntityName:@"Cycle"];
        NSPredicate *dateRange = [NSPredicate predicateWithFormat:@"(startedAt <= %@)",
                                  [newCycle startedAt]];
        NSPredicate *sameLocation = [NSPredicate predicateWithFormat:@"forLocation == %@",[newCycle forLocation]];
        NSPredicate *stillActive = [NSPredicate predicateWithFormat:@"cycleStatus == %d", CYCLE_STATUS_ACTIVE];
        [checkRequest setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[dateRange,sameLocation,stillActive]]];
        
        NSError *checkError = nil;
        
        NSManagedObjectContext *temporary = [[SDCoreDataController sharedInstance] newManagedObjectContext];
        // So that results here with not share context with self.managedObjectContext.
        NSArray * results = [temporary executeFetchRequest:checkRequest error:&checkError];
        
        DDLogWarn(@"%s : Results size is %d", __PRETTY_FUNCTION__, [results count]);
        Cycle *cycleFound = nil;
        
        BOOL isFound = NO;
        for(NSManagedObject * result in results){
            
            if(![[result objectID] isTemporaryID]){
                cycleFound = (Cycle *)result;
                isFound = YES;
                [temporary reset];
                temporary = nil;
                    break;
            }
        }
        
        //DDLogWarn(@"%@",checkRequest );
        //DDLogWarn(@"The check request %@ against the new one %@", cycleFound, newCycle);
        
        
        if(checkError){
            DDLogError(@"%s : Checking error => %@", __PRETTY_FUNCTION__, checkError);
        } else {
            if(isFound){
                [[[UIAlertView alloc] initWithTitle:INVENTORY_CYCLE_FOR_LOCATION_FOUND_TITLE
                                            message:INVENTORY_CYCLE_FOR_LOCATION_FOUND_MESSAGE
                                    completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                        
                                        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
                                        if([title isEqualToString:BUTTON_TITLE_PROCEED])
                                        {

                                            
                                            if([self cycleContainerDelegate]){
                                                [self.cycleContainerDelegate objectToUpdate:cycleFound
                                                                            withCycleStatus:CYCLE_STATUS_CLOSED];
                                            }
                                            
                                            
                                            
                                            //cycleFound = nil;
                                            [self dataPersistence:alertView withThisCycle:newCycle];
                                            
                                        }
                                        else if([title isEqualToString:BUTTON_TITLE_CANCEL])
                                        {
                                            return;
                                            // NSLog(@"Button 2 was selected.");
                                        }
                                    }
                                  cancelButtonTitle:BUTTON_TITLE_CANCEL otherButtonTitles:BUTTON_TITLE_PROCEED, nil] show];
                
            }
            
            else {
                
                // JOHN: We don't need this now because we don't have endedAt supplied by user but automatically supplied.
                // JOHN: if no overrlapping instances found.
                // We need to check if an entry for this location has an active inventory.
                // We need to set it to CLOSED when setting the new one as ACTIVE
                
                /*NSFetchRequest *checkRequest = [[NSFetchRequest alloc] initWithEntityName:@"Cycle"];
                NSPredicate *dateRange = [NSPredicate predicateWithFormat:@"(%@ >= endedAt)", [newCycle startedAt]];
                
                NSPredicate *sameLocation = [NSPredicate predicateWithFormat:@"forLocation == %@",[newCycle forLocation]];
                NSPredicate *stillActive = [NSPredicate predicateWithFormat:@"cycleStatus == %d", CYCLE_STATUS_ACTIVE];
                [checkRequest setPredicate:[NSCompoundPredicate andPredicateWithSubpredicates:@[dateRange,sameLocation,stillActive]]];
                
                NSError *error;
                NSArray *listedCycles = [self.managedObjectContext executeFetchRequest:checkRequest error:&error];
                
                for(Cycle *cycle in listedCycles){
                    if(![[cycle objectID]isTemporaryID] && [[cycle cycleStatus] isEqualToNumber:[NSNumber numberWithInt:CYCLE_STATUS_ACTIVE]]){
                        // JOHN: Pass this to a delegate. The delegate will be responsible on committing
                        // this on the database.
                        
                        if([self cycleContainerDelegate ]){
                            [self.cycleContainerDelegate objectToUpdate:cycle
                                                        withCycleStatus:CYCLE_STATUS_CLOSED];
                        }
                    }
                }*/
                
                [self dataPersistence:nil withThisCycle:newCycle];
            }
        }
    }
}

#define INVENTORY_CYCLE_NO_ITEMS_FOUND_TITLE @"No items available"
#define INVENTORY_CYCLE_NO_ITEMS_FOUND_MESSAGE @"Sorry.. We can't proceed."
#define BUTTON_TITLE_OK @"Ok"

- (void) dataPersistence:(id)sender withThisCycle:(Cycle *)newCycle
{
	[self.managedObjectContext performBlockAndWait:^{
                    
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
        
        NSArray *inventoryItems = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        
        if(!inventoryItems || [inventoryItems count] <= 0){
            
            [[[UIAlertView alloc]initWithTitle:INVENTORY_CYCLE_NO_ITEMS_FOUND_TITLE
                                      message:INVENTORY_CYCLE_NO_ITEMS_FOUND_MESSAGE
                              completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                                
                              }
                            cancelButtonTitle:BUTTON_TITLE_OK otherButtonTitles:nil] show];
            return;
        }
        
        if(error){
            DDLogWarn(@"%s :  Error => %@", __PRETTY_FUNCTION__, error);
        }
        
        NSMutableArray *arrCycleItems = [NSMutableArray array];
        
        // JOHN : Only create the intances when referred cycle is available.
        for(Item *item in inventoryItems){
            
            CyclePerInventoryItem *cyclePerItem = [NSEntityDescription insertNewObjectForEntityForName:@"CyclePerInventoryItem"
                                                                                inManagedObjectContext:self.managedObjectContext];
            [cyclePerItem setBy:newCycle];
            [cyclePerItem setForItem:item];
            [arrCycleItems addObject:cyclePerItem];
        }
        
        NSError *savingError = nil;
        [[self managedObjectContext] save:&savingError];
        
        if(savingError){
            DDLogWarn(@"%s : Error: %@", __PRETTY_FUNCTION__, savingError);
        }
        
        [[SDCoreDataController sharedInstance]saveMasterContext];
        
        // End data persistence
        NSMutableSet *mutableSet = [NSMutableSet set];
        for( CyclePerInventoryItem *ci in arrCycleItems)
        {
            [mutableSet addObject:[ci objectID]];
        }
        
        self.cycleInventory = newCycle;
        self.cyclePerItemsObjectIDs = [mutableSet copy];
        
        [self segueToNextPage];
    }];
}

- (void) segueToNextPage
{
    if([self cyclePage2]){
        
        //[self setCycle:newCycle];
        //NSLog(@"%s : The cycle are %@", __PRETTY_FUNCTION__, [self cycle]);
        
        [self.cyclePage2 setDelegate:self];
        [self.cyclePage2 loadCycleItemsFromCoreDataWithThisCategory:@"All Items"];
        [[self.cyclePage2 tableView] reloadData];
    }
    
    [self changePage:[self pageControl]];
}

#pragma mark - Cycle Page 1 Data Source
- (NSManagedObjectContext *) useThisManagedObjectContextForPage1
{
    return [self managedObjectContext];
}

- (Cycle *) useThisCycleInstanceForPage1
{
    return [self cycle];
}

#pragma mark - Cycle Page 2 Delegate method

- (Cycle *) useThisCycleInstance
{
    return [self cycle];
}

- (NSSet *) useTheseCyclePerItemIDs
{
    return [self cyclePerItemsObjectIDs];
}

/////////////////////////////////////////////
#pragma mark - Cycle Page 2 Data source
////////////////////////////////////////////
- (NSManagedObjectContext *) useThisManagedObjectContextForPage2
{
    return [self managedObjectContext];
}

- (void) persistedCyclePerItemFrom:(id)sender withIDs:(NSSet *)objectIDs
{
	DDLogWarn(@"%s : Persisted Cycle Per Item Data source method is called.",__PRETTY_FUNCTION__);
    DDLogWarn(@"The object IDs are: %@", objectIDs  );
    
     
    [self setCyclePerItemsObjectIDs:objectIDs];
    
}
@end
