
//
//  PVInventoryItemsTVC.m
//  FoodGrip
//
//  Created by ENG002 on 12/13/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVInventoryItemsTVC.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "Item.h"
#import "EditableTableDataRow.h"
#import "PVEditableTableDataRowItem.h"
#import "PVEditableTableDataRow.h"

#import "PVFoodGripUtil.h"
#import "PVSimpleHUDController.h"
#import "PVGlobals.h"

#define DEFAULT_ROW_ITEM_PADDING                        30

#define NAME_COLUMN_RELATIVE_WIDTH						130
#define CATEGORY_COLUMN_RELATIVE_WIDTH					80
#define ITEM_NAME_COLUMN_RELATIVE_WIDTH                 160
#define UNITS_COLUMN_RELATIVE_WIDTH                      80
#define PRICE_COLUMN_RELATIVE_WIDTH                      80

#define NUMBER_OF_DATA_FIELDS							8

#define DEFAULT_POPOVER_WIDTH							320
#define DEFAULT_POPUP_HEIGHT							21
#define DEFAULT_COMBO_HEIGHT							24
#define DEFAULT_TEXTFIELD_HEIGHT						24

enum{
    // JOHN: updated Feb 21, 2013
    //    return [NSArray arrayWithObjects: categoryItem, itemNumber, nameItem, orderUnitItem, orderPriceItem, orderToInventoryItem,inventoryUnitItem, inventoryPriceItem,/*extensionItem,*/ nil];
    
    INVENTORY_ITEM_COLUMN_CATEGORY = 0,
    INVENTORY_ITEM_COLUMN_NUMBER = 1,
    INVENTORY_ITEM_COLUMN_NAME = 2,
    INVENTORY_ITEM_COLUMN_UNIT = 6,
    INVENTORY_ITEM_COLUMN_PRICE = 7,
    INVENTORY_ITEM_COLUMN_ORDER_UNIT = 3,
    INVENTORY_ITEM_COLUMN_ORDER_PRICE = 4,
    INVENTORY_ITEM_COLUMN_COUNT = 8,
    INVENTORY_ITEM_COLUMN_ORDER_TO_INVENTORY= 5
} INVENTORY_ITEM;

@interface PVInventoryItemsTVC () <EditableTableDataRowDelegate, UIActionSheetDelegate,EditableTableDataRowItemDelegate>{
    
    NSMutableArray *_categories;
    NSMutableArray *_orderUnits;
    NSMutableArray *_itemUnits;
    MBProgressHUD *HUD;
    PVSimpleHUDController *HUDController;
}

@property (nonatomic,strong) Item *targetItem;
@property (nonatomic,strong) NSMutableArray *deletionArray;

@end

@implementation PVInventoryItemsTVC

@synthesize invItems = _invItems;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize entityName = _entityName;

@synthesize tableHeader = _tableHeader;
@synthesize predicates = _predicates;


@synthesize deletionArray;

- (NSString *)entityName
{
    if(_entityName == nil){
        _entityName = @"Item";
    }
    return _entityName;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSMutableArray *)invItems
{
    if(_invItems == nil){
        _invItems = [[NSMutableArray alloc]init];
    }
    return _invItems;
}

- (NSMutableArray *) predicates
{
    if(_predicates == nil){
        _predicates = [[NSMutableArray alloc] init];
        [_predicates addObject:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
    }
    return _predicates;
}

- (void)loadRecordsFromCoreData {
    
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:self.entityName];
        [request setSortDescriptors:[NSArray arrayWithObjects:
                                     [NSSortDescriptor sortDescriptorWithKey:@"category" ascending:YES],nil]];
        
        [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        //[request setPredicate:nil];  // we've want to see home many items are therein
        
        for( NSPredicate *predicate in self.predicates){
            [request setPredicate:predicate];
        }
        
        self.invItems = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setTitle:MENU_ITEM_INVENTORY_ITEMS];
    
    self.managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    
    [self.navigationItem.leftBarButtonItem  setCustomView:[PVGlobals menuBarButtonThemeFromTarget:self withAction:@selector(revealMenu:)]];
    
    [self.tableView setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithTitle:kEditLabel style:UIBarButtonItemStylePlain target:self action:@selector(editTableItems:)];
    UIBarButtonItem *bulkItem = [self.navigationItem rightBarButtonItem];
    
    // JOHN: Will not include bulk delete for now..
    /*UIBarButtonItem *bulkDelete = [[UIBarButtonItem alloc] initWithTitle:kBulkDeleteLabel style:UIBarButtonItemStylePlain target:self action:@selector(bulkDeleteItems:)];*/
    
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:bulkItem, /*bulkDelete,*/ editItem, nil]];
    
    [self loadRecordsFromCoreData];
    
    _categories = [[PVGlobals retrieveCategories] mutableCopy]; //[[NSUserDefaults standardUserDefaults] arrayForKey:ITEM_CATEGORIES];
    _itemUnits = [[PVGlobals retrieveItemUnits] mutableCopy];
    _orderUnits = [[PVGlobals retrieveOrderUnits] mutableCopy];
    
    [self.tableHeader setFont:[UIFont boldSystemFontOfSize:DEFAULT_TEXTFIELD_FONT_SIZE]];
    //[self.tableHeader sizeToFit];

}

- (void)bulkDeleteItems:(id)sender
{
    self.tableView.allowsMultipleSelectionDuringEditing = YES;
    
    [self editTableItems:sender];
}

- (void)editTableItems:(id)sender
{
    if(self.editing){
        [super setEditing:NO animated:YES];
		[self.tableView setEditing:NO animated:YES];
		[self.tableView reloadData];
        
        //NSInteger doneButtonIndex = ([self.tableView allowsMultipleSelectionDuringEditing])? 1 : 2;
        
        UIBarButtonItem *editButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
        //UIBarButtonItem *bulkDeleteButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
        
        if([self.tableView allowsMultipleSelectionDuringEditing]){
            [self.tableView setAllowsMultipleSelectionDuringEditing:NO];
        }
        
        [editButton setTitle:kEditLabel];
        [editButton setStyle:UIBarButtonItemStylePlain];
        
        /*
        switch(doneButtonIndex)
        {
            case 2:
                [editButton setTitle:kEditLabel];
                [editButton setStyle:UIBarButtonItemStylePlain];
                break;
            case 1:
                [bulkDeleteButton setTitle:kBulkDeleteLabel];
                [bulkDeleteButton setStyle:UIBarButtonItemStylePlain];
                break;
            default:
                break;
        }*/
        
        UIBarButtonItem *menuItem = [[[self navigationItem] leftBarButtonItems]objectAtIndex:0];
        [[self navigationItem] setLeftBarButtonItems:[NSArray arrayWithObjects:menuItem, nil] animated:NO];
        
        // John's note: must add a branch right here to check if item are needed to be persisted or not.
        // Persist changes made to our mode
        [self.managedObjectContext performBlockAndWait:^{
            NSError *error = nil;
            
            BOOL saved = [self.managedObjectContext save:&error];
            if (!saved) {
                // do some real error handling
                DDLogError(@"Could not save Date due to %@", error);
            }
         
            [[SDCoreDataController sharedInstance] saveMasterContext];
            [self.tableView reloadData];
            //[[SDSyncEngine sharedEngine]startSync];
        }];
    } else {
        [super setEditing:YES animated:YES];
		[self.tableView setEditing:YES animated:YES];
        [self.tableView reloadData];
        
        //#warning remove this integer literal and change it with some const variable or the like
        //NSInteger doneButtonIndex = ([self.tableView allowsMultipleSelectionDuringEditing])? 1 : 2;
        
        UIBarButtonItem *editButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1 /*2*/];
        //UIBarButtonItem *bulkDeleteButton = [self.navigationItem.rightBarButtonItems objectAtIndex:1];
        
        [editButton setTitle:kDoneLabel];
        [editButton setStyle:UIBarButtonItemStyleDone];
        
        /*
        switch(doneButtonIndex)
        {
            case 2:
                [editButton setTitle:kDoneLabel];
                [editButton setStyle:UIBarButtonItemStyleDone];
                break;
            case 1:
                [bulkDeleteButton setTitle:kDoneLabel];
                [bulkDeleteButton setStyle:UIBarButtonItemStyleDone];
                break;
            default:
                break;
        }
         */

        UIBarButtonItem *menuItem = [[[self navigationItem] leftBarButtonItems]objectAtIndex:0];
        UIBarButtonItem *deleteItem = nil;
                
        NSMutableArray *leftButtonSet = [NSMutableArray array];
        
        if([self.tableView allowsMultipleSelectionDuringEditing]){
            deleteItem = [[UIBarButtonItem alloc] initWithTitle:kDeleteLabel style:UIBarButtonItemStylePlain target:self action:@selector(deleteAction:)];
            deleteItem.tintColor = [UIColor redColor];
            leftButtonSet = [NSArray arrayWithObjects:menuItem,deleteItem, nil];
        } else {
            leftButtonSet = [NSArray arrayWithObjects:menuItem, nil];
        }
        
        [[self navigationItem]setLeftBarButtonItems:leftButtonSet animated:NO];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject *itemToRemove = [self.invItems objectAtIndex:indexPath.row];
        [self.managedObjectContext performBlockAndWait:^{
            if ([[itemToRemove valueForKey:@"objectId"] isEqualToString:@""] || [itemToRemove valueForKey:@"objectId"] == nil) {
                [self.managedObjectContext deleteObject:itemToRemove];
            } else {
                [itemToRemove setValue:[NSNumber numberWithInt:SDObjectDeleted] forKey:@"syncStatus"];
            }
            NSError *error = nil;
            BOOL saved = [self.managedObjectContext save:&error];
            if (!saved) {
                NSLog(@"Error saving main context: %@", error);
            }
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
            [self loadRecordsFromCoreData];
            [self.tableView reloadData];
        }];
    } else if (editingStyle == UITableViewCellEditingStyleInsert){
        // is called when + sign is clicked for row selected.
        [self updateItemAtIndexPath:indexPath withItem:self.targetItem forAdd:YES];
        
        // John's Note: We'll only reload data to the table; The persistence will come when DONE button
        // is clicked.
        [self.tableView reloadData];
        
    } else if (editingStyle == UITableViewCellEditingStyleNone){
        // for inline editing of items' properties??
        NSLog(@"Editing style None is invoked!!!");
    }
}

///////////////////////////////////////////////////////////////////
#pragma mark - Table view data source
//////////////////////////////////////////////////////////////////
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //int count = [self.invItems count];
    int count = [self.invItems count];//[[[_fetchedResultsController sections] objectAtIndex:section] numberOfObjects];
	if(self.editing) count +=1;
    
	return count;
}

#define ITEM_NUMBER_LENGTH 8
#define TARGET_ENTITY_NAME @"Item"

- (NSArray *)defineTableRowItems:(Item *) inventoryItem forEditing:(BOOL)isEditing
{
    NSString *itemName = inventoryItem.name;
    
    NSString *inventoryUnit = inventoryItem.inventoryUnit;
    NSDecimalNumber *inventoryPrice = (inventoryItem.inventoryPrice) ? inventoryItem.inventoryPrice : [NSDecimalNumber zero];
    
    NSString *orderUnit = inventoryItem.orderUnit;
    NSDecimalNumber *orderPrice = (inventoryItem.orderPrice)?inventoryItem.orderPrice : [NSDecimalNumber zero];;
    NSNumber *inventoryCount = inventoryItem.inventoryCount;

    NSNumber *orderToInventory = (inventoryItem.orderToInventory) ? inventoryItem.orderToInventory : [NSNumber numberWithInt:0];
    NSString *category = inventoryItem.category;
    NSString *itemNumber = inventoryItem.itemNumber;
    
    // Create the components..
    PVEditableTableDataRowItem *nameItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                               selections:nil
                                                                                         selectionListKey:nil
                                                                                                 baseSize:CGSizeMake(NAME_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                canResize:NO
                                                                                              normalImage:nil
                                                                                            selectedImage:nil
                                                                                             controlLabel:itemName
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil];
    
    
    [nameItem toggleEditingMode:YES];

    
    
    PVEditableTableDataRowItem *categoryItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeCombo selections:_categories selectionListKey:nil baseSize:CGSizeMake(CATEGORY_COLUMN_RELATIVE_WIDTH, DEFAULT_COMBO_HEIGHT) canResize:NO normalImage:[UIImage imageNamed:@"popupButton.png"] selectedImage:[UIImage imageNamed:@"popupButtonSelected.png"] controlLabel:category buttonTarget:nil buttonAction:nil];
    
    [categoryItem toggleEditingMode:YES];

    // JOHN : Only for category because we want to do something with the user's input
    // for this control
    categoryItem.delegate = self;
    
    PVEditableTableDataRowItem *inventoryUnitItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeCombo selections:_itemUnits selectionListKey:nil baseSize:CGSizeMake(UNITS_COLUMN_RELATIVE_WIDTH, DEFAULT_COMBO_HEIGHT) canResize:NO normalImage:[UIImage imageNamed:@"popupButton.png"] selectedImage:[UIImage imageNamed:@"popupButtonSelected.png"] controlLabel:inventoryUnit buttonTarget:nil buttonAction:nil usingNumberedKeyboard:YES];
    
    [inventoryUnitItem toggleEditingMode:YES];
    [inventoryUnitItem setDelegate:self];

    NSDecimalNumber *numerator = orderPrice;
    NSDecimalNumber *denominator = [PVGlobals numberToDecimalNumber:orderToInventory];
    NSDictionary *result = nil;
    if([denominator integerValue] > 0){
        result = [PVGlobals divideEvenlyWithRemainder:numerator
                                                   by:denominator
                                          forCurrency:[PVGlobals retrieveCurrencyCodeBasedOnCurrentLocale]];
    }
    inventoryPrice = (result) ? [NSDecimalNumber decimalNumberWithString:[PVGlobals convertDecimalDictionaryToString:result]] : [NSDecimalNumber zero];
    
    PVEditableTableDataRowItem *inventoryPriceItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                     selections:nil
                                                                                               selectionListKey:nil
                                                                                                       baseSize:CGSizeMake(PRICE_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                      canResize:NO
                                                                                                    normalImage:nil
                                                                                                  selectedImage:nil
                                                                                                   controlLabel:[NSString stringWithFormat:@"%@",inventoryPrice]
                                                                                                   buttonTarget:nil
                                                                                                   buttonAction:nil
                                                                                                     asCurrency:YES
                                                      usingNumberedKeyboard:YES];
    [inventoryPriceItem toggleEditingMode:NO];



    
    PVEditableTableDataRowItem *orderUnitItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeCombo selections:_orderUnits selectionListKey:nil baseSize:CGSizeMake(UNITS_COLUMN_RELATIVE_WIDTH, DEFAULT_COMBO_HEIGHT) canResize:NO normalImage:[UIImage imageNamed:@"popupButton.png"] selectedImage:[UIImage imageNamed:@"popupButtonSelected.png"] controlLabel:orderUnit buttonTarget:nil buttonAction:nil usingNumberedKeyboard:YES];
    
    [orderUnitItem toggleEditingMode:YES];
    [orderUnitItem setDelegate:self];

    
    PVEditableTableDataRowItem *orderPriceItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                     selections:nil
                                                                                               selectionListKey:nil
                                                                                                       baseSize:CGSizeMake(PRICE_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                      canResize:NO
                                                                                                    normalImage:nil
                                                                                                  selectedImage:nil
                                                                                             controlLabel:[NSString stringWithFormat:@"%@",orderPrice]
                                                                                             buttonTarget:nil
                                                                                             buttonAction:nil
                                                                                               asCurrency:YES usingNumberedKeyboard:YES];
    [orderPriceItem toggleEditingMode:YES];
    

    
    PVEditableTableDataRowItem *inventoryCountItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                    selections:nil
                                                                                              selectionListKey:nil
                                                                                                      baseSize:CGSizeMake(PRICE_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                     canResize:NO
                                                                                                   normalImage:nil
                                                                                                 selectedImage:nil
                                                                                                  controlLabel:[NSString stringWithFormat:@"%@",inventoryCount]
                                                                                                  buttonTarget:nil
                                                                                                  buttonAction:nil];
    [inventoryCountItem toggleEditingMode:YES];
 
    /*
    NSDecimalNumber *countAsDecimalNumberObj = [NSDecimalNumber decimalNumberWithString:[inventoryCount descriptionWithLocale:[PVGlobals retrieveAppCurrentLocaleInfo]] locale:[PVGlobals retrieveAppCurrentLocaleInfo]];
     */
    
    if(!itemNumber) itemNumber = [PVGlobals randStringUniqueInDBWithLength:ITEM_NUMBER_LENGTH forEntityName:TARGET_ENTITY_NAME];
        
    PVEditableTableDataRowItem *itemNumberItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                     selections:nil
                                                                                               selectionListKey:nil
                                                                                                       baseSize:CGSizeMake(NAME_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                      canResize:NO
                                                                                                    normalImage:nil
                                                                                                  selectedImage:nil
                                                                                                   controlLabel:itemNumber
                                                                                                   buttonTarget:nil
                                                                                                   buttonAction:nil];
    
    
    PVEditableTableDataRowItem *orderToInventoryItem = [[PVEditableTableDataRowItem alloc] initWithRowItemControlType:ControlTypeTextField
                                                                                                    selections:nil
                                                                                              selectionListKey:nil
                                                                                                      baseSize:CGSizeMake(PRICE_COLUMN_RELATIVE_WIDTH,DEFAULT_TEXTFIELD_HEIGHT)
                                                                                                     canResize:NO
                                                                                                   normalImage:nil
                                                                                                 selectedImage:nil
                                                                                                  controlLabel:[NSString stringWithFormat:@"%@",orderToInventory]
                                                                                                  buttonTarget:nil
                                                                                                  buttonAction:nil usingNumberedKeyboard:YES];
    
    [orderToInventoryItem toggleEditingMode:YES];

    // This should be aligned with the switch control in method: dataRow delegate method of EditableDataRow Class
    return [NSArray arrayWithObjects: categoryItem, itemNumberItem, nameItem, orderUnitItem, orderPriceItem, orderToInventoryItem,inventoryUnitItem, inventoryPriceItem,/*extensionItem,*/ nil];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CELL";
    PVEditableTableDataRow *cell = (PVEditableTableDataRow *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSLog(@"method tableView: + cell is nil, Instantiate a new cell.");
        
         cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier itemPadding:DEFAULT_ROW_ITEM_PADDING scaleToFill:YES forTable:self.tableView];
    }
    cell.delegate = self;
    
    // Set up the cell...
	if( ([self.invItems count] == 0 || indexPath.row == ([self.invItems count])) && self.editing)
	{
        // Instantiate a non-managed item object.
        Item *objItem = [[Item alloc] initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
        
        // John's note: objItem is only used here a container of variables but not really used as a model reference for the controls
        // after code below is executed, objItem is destroyed.
        cell.rowItems = [self defineTableRowItems:(Item *)objItem forEditing:self.editing];
        //[cell applyEditingModeAs:self.editing];
        [cell applyEditingModeAsWithOptions:self.editing];
        
        return cell;
	}

    Item *item =  [self.invItems objectAtIndex:indexPath.row];
    
    cell.backgroundView = [PVGlobals commonCellBackgroundView:cell];
    
    cell.tag = [indexPath row];
    cell.rowItems =  [self defineTableRowItems:(Item *)item forEditing:self.editing];
    [cell applyEditingModeAsWithOptions:self.editing];
     
    return cell;
}

// The editing style for a row is the kind of button displayed to the left of the cell when in editing mode.
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // No editing style if not editing or the index path is nil.
    if (self.editing == NO || !indexPath) return UITableViewCellEditingStyleNone;
    // Determine the editing style based on whether the cell is a placeholder for adding content or already
    // existing content. Existing content can be deleted.
    if (self.editing && indexPath.row == ([self.invItems count]))
	{
		return UITableViewCellEditingStyleInsert;
	} else
	{
		return UITableViewCellEditingStyleDelete;
	}
    return UITableViewCellEditingStyleNone;
}

#define ITEM_NUMBER_KEY @"itemNumber"
#define CATEGORY_KEY @"category"
#define EXTENSION_KEY @"extension"
#define ORDER_TO_INVENTORY_KEY @"orderToInventory"
#define ORDER_PRICE_KEY @"orderPrice"
#define ORDER_UNIT_KEY @"orderUnit"
#define INVENTORY_PRICE_KEY @"inventoryPrice"
#define INVENTORY_UNIT_KEY @"inventoryUnit"
#define ITEM_NAME_KEY @"name"
#define SYNC_STATUS_KEY @"syncStatus"

- (void) updateItemAtIndexPath:(NSIndexPath *)indexPath withItem:(Item *)inventoryItem forAdd:(BOOL)toAdd
{
        if(self.editing){
            if([self.invItems count] == 0 || [self.invItems count] == [indexPath row]){
                
                DDLogVerbose(@"Begin adding new item at index path %i", [indexPath row]);
                
                // Instantiate new managed object
                Item *newItem = [self targetItem];
                
                if(newItem == nil){
                    newItem = [NSEntityDescription insertNewObjectForEntityForName:TARGET_ENTITY_NAME inManagedObjectContext:self.managedObjectContext];
                }
                
                NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
                [f setNumberStyle:NSNumberFormatterDecimalStyle];
                
                [newItem setValue:inventoryItem.syncStatus forKey:SYNC_STATUS_KEY];
                
                [newItem setValue:inventoryItem.name forKey:ITEM_NAME_KEY];
                [newItem setValue:inventoryItem.inventoryUnit forKey:INVENTORY_UNIT_KEY];
                [newItem setValue:inventoryItem.inventoryPrice forKey:INVENTORY_PRICE_KEY];
                
                [newItem setValue:inventoryItem.orderUnit forKey:ORDER_UNIT_KEY];
                [newItem setValue:inventoryItem.orderPrice forKey:ORDER_PRICE_KEY];
                
                [newItem setValue:inventoryItem.orderToInventory forKey:ORDER_TO_INVENTORY_KEY];
                [newItem setValue:inventoryItem.extension forKey:EXTENSION_KEY];
                [newItem setValue:inventoryItem.category forKey:CATEGORY_KEY];
                
                
                DDLogVerbose(@"Is the item here to be Added.. >%@",(toAdd)?@"YES":@"NO");
                if(toAdd){
                    
                    // JOHN: If newly create item, auto generate if itemNumber is not supplied.
                    //if(!([inventoryItem.itemNumber length] > 0)){
                    [newItem setValue:[PVGlobals randStringUniqueInDBWithLength:ITEM_NUMBER_LENGTH forEntityName:TARGET_ENTITY_NAME] forKey:ITEM_NUMBER_KEY];
                    //}
                    
                    DDLogVerbose(@" method: updateItemAtIndexPath: + adding new item to self.invItems");
                    [self.invItems addObject:newItem];
                    
                    
                    
                    DDLogVerbose(@"Before: The target item in %s is %@",__func__, newItem) ;
                    [self setTargetItem:nil];
                    DDLogVerbose(@"After: The target item in %s is %@",__func__, newItem) ;
                    
                } else {
                    
                    [newItem setValue:inventoryItem.itemNumber forKey:ITEM_NUMBER_KEY];
                    
                    DDLogVerbose(@" method: updateItemAtIndexPath: + setting value to Target Item");
                    [self setTargetItem:newItem];
                }
            }
        }
}

- (void)checkSyncStatus {
 
    if(HUD == nil){
        HUDController = [[PVSimpleHUDController alloc]init];
    }
    
    if ([[SDSyncEngine sharedEngine] syncInProgress]) {
        
        [HUDController showWithLabelMixed:self clipTo:self withLabel:@"Refreshing"];
        
        //[self replaceRefreshButtonWithActivityIndicator];
        
        // Let's disable the DONE/EDIT button when refreshing is on going.
        //[[[[self navigationItem] leftBarButtonItems] objectAtIndex:1] setEnabled:NO];
    } else {
        //[HUDController showWithLabelMixed:self clipTo:self.navigationController withLabel:@""];
        
        //[self removeActivityIndicatorFromRefreshButon];
        
        // Let's enable this again after removing activity indicator
        //[[[[self navigationItem] leftBarButtonItems]objectAtIndex:1] setEnabled:YES];
    }
}

////////////////////////////////////////
#pragma mark - Activity Indicator management
////////////////////////////////////////
- (void)replaceRefreshButtonWithActivityIndicator {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [activityIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [activityIndicator startAnimating];
    
    // Configure for text only and offset down
	HUD.mode = MBProgressHUDModeText;
	HUD.labelText = @"Some message...";
	HUD.margin = 10.f;
	HUD.yOffset = 150.f;
}

- (void)removeActivityIndicatorFromRefreshButon {
    /*
    UIBarButtonItem *menuItem = [[self.navigationItem leftBarButtonItems] objectAtIndex:0];

    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:menuItem, nil] animated:YES];
     */
	HUD.removeFromSuperViewOnHide = YES;
    [HUD hide:YES afterDelay:3];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"syncInProgress"]) {
        [self checkSyncStatus];
    }
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

///////////////////////////////////////
#pragma mark - Row reordering
//////////////////////////////////////
// Determine whether a given row is eligible for reordering or not.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
// Process the row move. This means updating the data model to correct the item indices.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath
	  toIndexPath:(NSIndexPath *)toIndexPath
{
	NSString *item = [self.invItems objectAtIndex:fromIndexPath.row];
	[self.invItems removeObject:item];
	[self.invItems insertObject:item atIndex:toIndexPath.row];
}

#pragma mark - Table view delegate
/*
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!self.tableView.isEditing)
    {
        //self.viewController.title = [tableView cellForRowAtIndexPath:indexPath].textLabel.text;
        //[[self navigationController] pushViewController:self.viewController animated:YES];
        
        //[[self navigationController] pushViewController:[self.navigationController.view] animated:YES];
        //[self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
    else
    {
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        //NSString *deleteButtonTitle = [NSString stringWithFormat:kDeletePartialTitle, selectedRows.count];
        
        if([selectedRows count] == [self.invItems count]){
            //
        }
        
        //if (selectedRows.count == self.dataArray.count)
        //{
        //    deleteButtonTitle = kDeleteAllTitle;
        //}
       // self.deleteButton.title = deleteButtonTitle;
    }
    
    
}*/

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath
{
    if(proposedDestinationIndexPath.row == [self.invItems count]){
        return sourceIndexPath;
    }
    return proposedDestinationIndexPath;
}

#define CATEGORY_HEADER_LABEL               @"Category"
#define ITEM_NUMBER_HEADER_LABEL            @"Item Number"
#define ITEM_NAME_HEADER_LABEL              @"Description"
#define INVENTORY_UNIT_HEADER_LABEL         @"Inventory Unit: Unit"
#define INVENTORY_PRICE_HEADER_LABEL        @"Inventory Unit: Price"
#define ORDER_UNIT_HEADER_LABEL             @"Order Unit: Unit"
#define ORDER_PRICE_HEADER_LABEL            @"Order Unit: Price"
#define INVENTORY_COUNT_HEADER_LABEL        @"Inventory Count"
#define ORDER_TO_INVENTORY_HEADER_LABEL     @"Order To Inventory"
#define EXTENSION_HEADER_LABEL              @"Extension"

#define CATEGORY_COLUMN_HEADER_WIDTH    100
#define DEFAULT_COLUMN_HEADER_WIDTH     120

- (UIView *)tableView:(UITableView *)tableView 
    viewForHeaderInSection:(NSInteger)section
{   
     UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.tableView.frame.size.width, self.tableView.frame.size.height / 12)];
    [headerView setBackgroundColor:[UIColor whiteColor]];
    
    PVEditableTableDataRow *cell = [[PVEditableTableDataRow alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil itemPadding:DEFAULT_ROW_ITEM_PADDING scaleToFill:NO forTable:self.tableView];
    
    //cell.rowItems = [self defineTableHeaderViewColumnLabelsWithHeight:headerView.frame.size.height];
    
    CGSize size = CGSizeMake(PRICE_COLUMN_RELATIVE_WIDTH, cell.frame.size.height);

    NSArray *labels = @[CATEGORY_HEADER_LABEL,
                        ITEM_NUMBER_HEADER_LABEL,
                        ITEM_NAME_HEADER_LABEL,
                        ORDER_UNIT_HEADER_LABEL,
                        ORDER_PRICE_HEADER_LABEL,
                        ORDER_TO_INVENTORY_HEADER_LABEL,
                        INVENTORY_UNIT_HEADER_LABEL,
                        INVENTORY_PRICE_HEADER_LABEL];
    
    NSDictionary *labelsWithTargetedWidth = @{
                                              CATEGORY_HEADER_LABEL             : @CATEGORY_COLUMN_HEADER_WIDTH,
                                              ITEM_NUMBER_HEADER_LABEL          : @(DEFAULT_COLUMN_HEADER_WIDTH - 10),
                                              ITEM_NAME_HEADER_LABEL            : @DEFAULT_COLUMN_HEADER_WIDTH,
                                              ORDER_UNIT_HEADER_LABEL           : @UNITS_COLUMN_RELATIVE_WIDTH,
                                              ORDER_PRICE_HEADER_LABEL          : @UNITS_COLUMN_RELATIVE_WIDTH,
                                              ORDER_TO_INVENTORY_HEADER_LABEL   : @UNITS_COLUMN_RELATIVE_WIDTH,
                                              INVENTORY_UNIT_HEADER_LABEL       : @UNITS_COLUMN_RELATIVE_WIDTH,
                                              INVENTORY_PRICE_HEADER_LABEL      : @UNITS_COLUMN_RELATIVE_WIDTH
                                            };

    cell.rowItems = [PVGlobals customTableColumnHeader:cell withSize:size andLabels:labels withCorrespondingWidth:labelsWithTargetedWidth];
    
    [cell applyEditingModeAs:NO];
    [cell setFrame:headerView.frame];    
    [headerView addSubview:cell];

    return headerView;
}

-(CGFloat) tableView:(UITableView *)tableView 
  heightForHeaderInSection:(NSInteger)section
{
    return tableView.frame.size.height / 12;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *menuIdentifier = [NSString stringWithFormat:@"%@%@", MENU_ITEM_THE_MENU_KEY, SUFFIX_FOR_MENU_ITEMS];
    self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:menuIdentifier];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"SDSyncEngineSyncCompleted" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self loadRecordsFromCoreData];
        [self.tableView reloadData];
    }];
    [[SDSyncEngine sharedEngine] addObserver:self forKeyPath:@"syncInProgress" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewDidUnload {
    //[self setRefreshButton:nil];
    [super viewDidUnload];
    
    deletionArray = nil;
}

- (IBAction)refreshButtonTouched:(id)sender {
    [[SDSyncEngine sharedEngine] startSync];
}

- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

////////////////////////////////////////////////////////////
#pragma mark - EditableTableDataRowItem Delegate methods
///////////////////////////////////////////////////////////

- (void)dataRow:(PVEditableTableDataRow *)dataRow didSetValue:(id)newValue forColumn:(int)column inTable:(UITableView *)table
{
	NSIndexPath     *selectedRow = [self.tableView indexPathForCell:dataRow];
    NSLog(@"The inventory items have a size of %i and the selected row is: %i ", [self.invItems count], [selectedRow row]);
    
    Item *toEditItem = [self targetItem];
    
    if(toEditItem == nil ){
        NSLog(@" Target item is nil");
        if([selectedRow row] < [self.invItems count]){
            NSLog(@" We'll get the item from the table");
            toEditItem = [self.invItems objectAtIndex:[selectedRow row]];
            toEditItem.syncStatus = [NSNumber numberWithInt:SDObjectSynced];
        } else {
            NSLog(@"We'll create a new non-managed instance for this item");
            toEditItem = [[Item alloc] initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
            toEditItem.syncStatus = [NSNumber numberWithInt:SDObjectCreated];
        }
    }
 
    if([[dataRow.rowItems objectAtIndex:column] isKindOfClass:[PVEditableTableDataRowItem class]]){
        PVEditableTableDataRowItem *updateItem = [dataRow.rowItems objectAtIndex:column];
        [updateItem changeControlLabelTo:newValue];
        
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        switch (column) {
            case INVENTORY_ITEM_COLUMN_CATEGORY:
                toEditItem.category = newValue;
                break;
            case INVENTORY_ITEM_COLUMN_NAME:
                toEditItem.name = newValue;
                break;
            case INVENTORY_ITEM_COLUMN_UNIT:
                toEditItem.inventoryUnit = newValue;
                break;
            case INVENTORY_ITEM_COLUMN_PRICE:
                toEditItem.inventoryPrice = [PVGlobals numberToDecimalNumber:[f numberFromString:newValue]];
                break;
            case INVENTORY_ITEM_COLUMN_ORDER_UNIT:
                toEditItem.orderUnit = newValue;
                break;
            case INVENTORY_ITEM_COLUMN_ORDER_PRICE:
                toEditItem.orderPrice = [PVGlobals numberToDecimalNumber:[f numberFromString:newValue]];
                break;
            case INVENTORY_ITEM_COLUMN_COUNT:
                toEditItem.inventoryCount = [PVGlobals numberToDecimalNumber:[f numberFromString:newValue]];
                break;
            case INVENTORY_ITEM_COLUMN_ORDER_TO_INVENTORY:
                toEditItem.orderToInventory = [PVGlobals numberToDecimalNumber:[f numberFromString:newValue]];
                break;
            case INVENTORY_ITEM_COLUMN_NUMBER:
                toEditItem.itemNumber = newValue;
                break;
            /*case 8:
                toEditItem.extension = (NSDecimalNumber *)[f numberFromString:newValue];
                break;*/
            default:
                break;
        }
    }
    
	//[self.tableView reloadData];
	//[self.tableView selectRowAtIndexPath:selectedRow animated:NO scrollPosition:UITableViewScrollPositionNone];
    
    [self updateItemAtIndexPath:selectedRow withItem:toEditItem forAdd:NO];
}

- (void)didSelectDataRow:(EditableTableDataRow *)dataRow inTable:(UITableView *)table
{
	//[self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
	[self.tableView selectRowAtIndexPath:[self.tableView indexPathForCell:dataRow] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    self.tableView.tableHeaderView = self.tableHeader;
	[self.tableView setNeedsLayout];
}

///////////////////////////////////
#pragma mark - public methods
//////////////////////////////////

- (void)saveBulk:(NSArray *)invItems
{
    for(NSDictionary *item in invItems ){
        
        if(self.managedObjectContext == nil) {
            self.managedObjectContext = [[SDCoreDataController sharedInstance]newManagedObjectContext];
        }
        
        Item *newItem = [NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext];
        
        //[newItem setValue:[NSNumber numberWithInt:SDObjectCreated] forKey:@"syncStatus"];
        
        [newItem setValue:[item objectForKey:@"name"] forKey:@"name"];
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        
        //return [NSArray arrayWithObjects: categoryItem, nameItem, inventoryUnitItem, inventoryPriceItem, orderUnitItem, orderPriceItem, inventoryCountItem, orderToInventoryItem, extensionItem, nil];
        
        [newItem setValue:[item objectForKey:@"inventoryUnit"] forKey:@"inventoryUnit"];
        NSDecimalNumber *inventoryPrice = (NSDecimalNumber *)[f numberFromString:[item objectForKey:@"inventoryPrice"]];
        [newItem setValue:inventoryPrice forKey:@"inventoryPrice"];
        
        [newItem setValue:[item objectForKey:@"orderUnit"] forKey:@"orderUnit"];
        NSDecimalNumber *orderPrice = (NSDecimalNumber *)[f numberFromString:[item objectForKey:@"orderPrice"]];
        [newItem setValue:orderPrice forKey:@"orderPrice"];
        
        NSNumber *inventoryCount = [f numberFromString:[item objectForKey:@"count"]];
        [newItem setValue:inventoryCount forKey:@"inventoryCount"];
                                    
        NSDecimalNumber *extension = (NSDecimalNumber *)[f numberFromString:[item objectForKey:@"extension"]];
        [newItem setValue:extension forKey:@"extension"];
                                    
        NSNumber *orderToInventory = [f numberFromString:[item objectForKey:@"orderToInventory"]];
        [newItem setValue:orderToInventory forKey:@"orderToInventory"];
        
        [newItem setValue:[item objectForKey:@"Category"] forKey:@"category"];
    }
    
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        BOOL saved = [self.managedObjectContext save:&error];
        if (!saved) {
            // do some real error handling
            NSLog(@"Could not save Date due to %@", error);
        }
        [[SDCoreDataController sharedInstance] saveMasterContext];
        [[SDSyncEngine sharedEngine]startSync];
        
        //[PVFoodGripUtil initItemCategoriesToDefaults:self.managedObjectContext];
        [PVGlobals reloadToGetRecentCategories];
        [self.tableView reloadData];
    }];
}

//////////////////////////////////////////////////
#pragma mark - 
//////////////////////////////////////////////////
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self checkSyncStatus];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"SDSyncEngineSyncCompleted" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self loadRecordsFromCoreData];
        [self.tableView reloadData];
    }];
    [[SDSyncEngine sharedEngine] addObserver:self forKeyPath:@"syncInProgress" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SDSyncEngineSyncCompleted" object:nil];
    
    //[SDSyncEngine sharedEngine]
    
    [[SDSyncEngine sharedEngine] removeObserver:self forKeyPath:@"syncInProgress"];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		// delete the selected rows
        NSArray *selectedRows = [self.tableView indexPathsForSelectedRows];
        if (selectedRows.count > 0)
        {
            deletionArray = [NSMutableArray array];
            for (NSIndexPath *selectionIndex in selectedRows)
            {
                [deletionArray addObject:[self.invItems objectAtIndex:selectionIndex.row]];
            }
            //[self.invItems removeObjectsInArray:deletionArray];
            
            // then delete the only the rows in our table that were selected
            //[self.tableView deleteRowsAtIndexPaths:selectedRows withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        /*
        else
        {
            [self.invItems removeAllObjects];
            
            // since we are deleting all the rows, just reload the current table section
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        */
        [self.managedObjectContext performBlockAndWait:^{
            
            for(Item *itemToRemove in deletionArray){
                if ([[itemToRemove valueForKey:@"objectId"] isEqualToString:@""] || [itemToRemove valueForKey:@"objectId"] == nil) {
                    [self.managedObjectContext deleteObject:itemToRemove];
                } else {
                    [itemToRemove setValue:[NSNumber numberWithInt:SDObjectDeleted] forKey:@"syncStatus"];
                }
                NSError *error = nil;
                BOOL saved = [self.managedObjectContext save:&error];
                if (!saved) {
                    NSLog(@"Error saving main context: %@", error);
                }
                
                [[self invItems]removeObject:itemToRemove];
            }
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
            [self loadRecordsFromCoreData];
            
            [self editTableItems:self];
            [self.tableView reloadData];
        }];
        
        //[self resetUI]; // reset our UI
        [self.tableView setEditing:NO animated:YES];
        
        // This refer to the DONE button when in edit mode
        [[[[self navigationItem] rightBarButtonItems] objectAtIndex:1] setEnabled: ([self.invItems count] > 0) ? YES : NO];
	}
}


- (IBAction)deleteAction:(id)sender
{
    // open a dialog with just an OK button
	NSString *actionTitle = ([[self.tableView indexPathsForSelectedRows] count] == 1) ? kDeleteItemMessage : kDeleteItemsMessage;
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:actionTitle
                                                             delegate:self
                                                    cancelButtonTitle:kCancelLabel
                                               destructiveButtonTitle:kOKLabel
                                                    otherButtonTitles:nil];
    
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];	// show from our table view (pops up in the middle of the table)
}

//
#pragma mark - Editable Row Item Delegate Methods
//
- (void)rowItem:(EditableTableDataRowItem *)rowItem controlDidSelectItem:(id)selection
{
    DDLogVerbose(@"%s : The value is %@",__PRETTY_FUNCTION__, selection);
}
- (void)rowItem:(EditableTableDataRowItem *)rowItem controlDidSetValue:(NSString *)newValue
{
    DDLogVerbose(@"%s : The value is %@",__PRETTY_FUNCTION__, newValue);
}
- (void)rowItem:(EditableTableDataRowItem *)rowItem controlDidToggleToValue:(BOOL)newToggleValue
{
    
}
- (void)rowItemWasSelected:(EditableTableDataRowItem *)rowItem
{
    
}
@end
