//
//  CyclePerInventoryItem.m
//  FoodGrip
//
//  Created by ENG002-JRF on 2/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "CyclePerInventoryItem.h"
#import "Cycle.h"
#import "Item.h"


@implementation CyclePerInventoryItem

@dynamic createdAt;
@dynamic purchaseCount;
@dynamic quantityCount;
@dynamic syncStatus;
@dynamic updatedAt;
@dynamic objectId;
@dynamic by;
@dynamic forItem;


- (id)initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
    
    self = (CyclePerInventoryItem *)entity;
    if(self){
        if(context){
            [context deleteObject:self];
        }
    }
    return self;
}

- (NSDictionary *)JSONToCreateObjectOnServer
{
    NSString *jsonString = nil;
    
    NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.purchaseCount, @"purchaseCount",
                                    self.quantityCount, @"quantityCount",
                                    self.syncStatus, @"syncStatus",nil];
    
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:jsonDictionary
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    if (!jsonData) {
        NSLog(@"Error creating jsonData: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    return jsonDictionary;
}



@end
