//
//  PVEditableTableDataRowItem.h
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "EditableTableDataRowItem.h"

@interface PVEditableTableDataRowItem : EditableTableDataRowItem{
    
    UILabel *controlAlt; // this will hold the label when control is not activated (ie clicking "EDIT" to show controls")
}


@property (nonatomic, strong) UILabel *controlAlt;
@property (nonatomic) BOOL toEdit;


- (id)initWithRowItemControlType:(int)controlType selections:(NSArray *)selections selectionListKey:(NSString *)selectionListKey baseSize:(CGSize)size canResize:(BOOL)resize normalImage:(UIImage *)normalButtonImage selectedImage:(UIImage *)selectedButtonImage controlLabel:(NSString *)label buttonTarget:(id)target buttonAction:(SEL)action asCurrency:(BOOL)asCurrency usingNumberedKeyboard:(BOOL)useNumberKeyboard;

- (void) toggleEditingMode:(BOOL)toEdit;


- (void)changeControlLabelTo:(NSString *)newValue;

@end
