//
//  PVDropboxSelectionAlertTableView.h
//  FoodGrip
//
//  Created by ENG002 on 12/18/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVAlertTableView.h"
#import <DropboxSDK/DropboxSDK.h>

@protocol DropboxSelectionAlertDelegate

- (void)didSelectRowWithDBMetadata:(DBMetadata *)file withContext:(id)context;
@end

@interface PVDropboxSelectionAlertTableView : PVAlertTableView

@property (nonatomic,strong) id<DropboxSelectionAlertDelegate> myDelegate;

@end
