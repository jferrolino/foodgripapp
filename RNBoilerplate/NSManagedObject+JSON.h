//
//  NSManagedObject+JSON.h
//  FoodGrip
//
//  Created by ENG002 on 12/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (JSON)

- (NSDictionary *)JSONToCreateObjectOnServer;

//- (NSString *)dateStringForAPIUsingDate:(NSDate *)date;

@end
