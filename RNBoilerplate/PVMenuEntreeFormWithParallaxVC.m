//
//  PVMenuEntreeFormWithParallaxVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/11/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMenuEntreeFormWithParallaxVC.h"
#import "FKFormMapping.h"
#import "FKFormModel.h"
#import "SDCoreDataController.h"
#import "PVMultiSelectLocationsTVC.h"
#import "PVMultiSelectItemsTVC.h"

@interface PVMenuEntreeFormWithParallaxVC () <MultiSelectionDelegate> {
    
    NSOrderedSet *_selectedLocations;
    NSOrderedSet *_selectedIngredients;
    NSIndexPath *_pathForRowToUpdate;
}

@end

@implementation PVMenuEntreeFormWithParallaxVC

@synthesize entreeTableForm = _entreeTableForm;
@synthesize formModel = _formModel;

@synthesize managedObjectContext = _managedObjectContext;

- (NSManagedObjectContext *) managedObjectContext
{
  if([self datasource]){
    if([self.datasource useManagedObjectContext]){
      NSManagedObjectContext * moc = [self.datasource useManagedObjectContext];

      _managedObjectContext = moc;
    } else {
      _managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    }
  }
  
  return _managedObjectContext;
}

- (id) initWithFrame:(CGRect) frame underNavigationController:(UINavigationController *)navController
{
    if( (self = [super init]))
    {
        [self.view setFrame:frame];
    }
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil underNavigationController:(UINavigationController *) navController
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]){
        //[self setNavigationController:navController];
    }
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]){
        
    }
    return self;
}

#define HACK_TARGET_WIDTH_FOR_FORM_TABLE 600.0f

- (void) loadView
{
    [super loadView];
    self.view.backgroundColor = [PVGlobals commonViewBackgroundColor];
    
    CGRect entreeRect = CGRectMake(0,0, HACK_TARGET_WIDTH_FOR_FORM_TABLE, self.view.bounds.size.height );
    self.entreeTableForm = [[UITableView alloc] initWithFrame:entreeRect style:UITableViewStyleGrouped];
    self.entreeTableForm.delegate = self;
    
    _imageScroller  = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _imageScroller.backgroundColor                  = [UIColor clearColor];
    _imageScroller.showsHorizontalScrollIndicator   = NO;
    _imageScroller.showsVerticalScrollIndicator     = NO;
    _imageScroller.pagingEnabled                    = YES;
    
    _imageViews = [NSMutableArray arrayWithCapacity:10];
    
    [self addImages:@[[UIImage imageNamed:@"entree-default.jpg"]]];
    
    _transparentScroller = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _transparentScroller.backgroundColor                = [UIColor clearColor];
    _transparentScroller.delegate                       = self;
    _transparentScroller.bounces                        = NO;
    _transparentScroller.pagingEnabled                  = YES;
    _transparentScroller.showsVerticalScrollIndicator   = YES;
    _transparentScroller.showsHorizontalScrollIndicator = NO;
    
    _contentScrollView = [[UIScrollView alloc] init];
    _contentScrollView.backgroundColor              = [UIColor clearColor];
    _contentScrollView.delegate                     = self;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.currentPage = 0;
    [_pageControl setHidesForSinglePage:YES];

    [_contentScrollView addSubview:self.entreeTableForm];
    [_contentScrollView addSubview:_pageControl];
    [_contentScrollView addSubview:_transparentScroller];
    
    _contentView = self.entreeTableForm;
    
    [self.view addSubview:_imageScroller];
    [self.view addSubview:_contentScrollView];
    
    // JOHN : instantiate the table view
    [self createMenuEntreeForm];
}

#define LOCATION_ENTITY_NAME @"Location"
#define INGREDIENTS_ENTITY_NAME @"Item"

#define MENU_ENTREE_FORM_LABEL_NO_LOCATION @"Not available in any location."

- (void) createMenuEntreeForm
{
    //self.managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    DDLogVerbose(@"The navigation Controller from the form %@", self.navigationController);
    

    self.formModel = [FKFormModel formTableModelForTableView:self.entreeTableForm
                                        navigationController:self.navigationController];
        
    MenuEntree *menuEntree = [[MenuEntree alloc]initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"MenuEntree" 
                                                                                                      inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
    
    self.entree = menuEntree;
    
    [FKFormMapping mappingForClass:[MenuEntree class] block:^(FKFormMapping *formMapping) {
        
        
        [formMapping sectionWithTitle:@"Menu Entree Info" identifier:@"Menu Entree info"];
        
        
        [formMapping mapAttribute:@"controlNumber" title:@"Control Number" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"name" title:@"Description" type:FKFormAttributeMappingTypeText];

        [formMapping sectionWithTitle:@"Manual Procedure" identifier:@"manual procedure"];
        
        [formMapping mapAttribute:@"preparationProcedure" title:@"Preparation " type:FKFormAttributeMappingTypeBigText];
        [formMapping mapAttribute:@"orderingProcedure" title:@"Ordering " type:FKFormAttributeMappingTypeBigText];
        [formMapping mapAttribute:@"cookingProcedure" title:@"Cooking" type:FKFormAttributeMappingTypeBigText];

        [formMapping mapAttribute:@"presentation" title:@"Presentation " type:FKFormAttributeMappingTypeBigText];
        [formMapping mapAttribute:@"comments" title:@"Additional Comments" type:FKFormAttributeMappingTypeBigText];
        
        [formMapping sectionWithTitle:@"Ingredients" identifier:@"cellIngredients"];
        
        [formMapping mapCustomCell:[UITableViewCell class]
                        identifier:@"ingredients"
              willDisplayCellBlock:^(UITableViewCell *cell, id object, NSIndexPath *indexPath) {
                  cell.textLabel.textAlignment = NSTextAlignmentCenter;
                  cell.textLabel.text = @"No available ingredients";
                  
                  if(_selectedIngredients){
                      DDLogVerbose(@"_selectedIngredients is now populated!!!");
                      cell.textLabel.text = [NSString stringWithFormat:@"Uses %i ingredient(s)", [_selectedIngredients count]];
                  }
                  
              }
              didSelectBlock:^(UITableViewCell *cell, id object, NSIndexPath *indexPath) {
                  PVMultiSelectItemsTVC *selection = [self.storyboard instantiateViewControllerWithIdentifier:@"MULTI_SELECTION_ITEM_TVC"];
                  [selection setEntityName:INGREDIENTS_ENTITY_NAME];
                  [selection setDelegate:self];
                  
                  if(_selectedIngredients){
                      DDLogVerbose(@"The size of _selectedIngredients %i", [_selectedIngredients count]);
                      
                      NSMutableArray *mutIngredients = [NSMutableArray arrayWithArray:[_selectedIngredients array]];
                      [selection setSelectedMarks:mutIngredients];
                  }
                  
                  _pathForRowToUpdate = indexPath;
                  
                  [self.navigationController pushViewController:selection animated:YES];
              }];
        
        [formMapping sectionWithTitle:@"Locations" identifier:@"cellLocations"];
        
        [formMapping mapCustomCell:[UITableViewCell class]
                        identifier:@"locations"
              willDisplayCellBlock:^(UITableViewCell *cell, id object, NSIndexPath *indexPath) {
                  
                  cell.textLabel.textAlignment = NSTextAlignmentCenter;
                  cell.textLabel.text = MENU_ENTREE_FORM_LABEL_NO_LOCATION;
                  
                  if(_selectedLocations){                    
                      cell.textLabel.text = [NSString stringWithFormat:@"Used by %i location(s)", [_selectedLocations count]];
                  }
                  
              }     didSelectBlock:^(UITableViewCell *cell, id object, NSIndexPath *indexPath) {
                  NSLog(@"You pressed me");
                  
                  PVMultiSelectLocationsTVC *selection = [self.storyboard instantiateViewControllerWithIdentifier:@"MULTI_SELECTION_LOCATION_TVC"];
                  [selection setEntityName:LOCATION_ENTITY_NAME];
                  [selection setDelegate:self];
                  
                  if(_selectedLocations){
                      DDLogVerbose (@"The size of _selectedLocations %i", [_selectedLocations count]);
                      
                      NSMutableArray *mutLocations = [NSMutableArray arrayWithArray:[_selectedLocations array]];
                      [selection setSelectedMarks:mutLocations];
                  }
                  
                  _pathForRowToUpdate = indexPath;
                  
                  [self.navigationController pushViewController:selection animated:YES];
                  
              }];
        
        [formMapping buttonSave:@"Save" handler:^{
            
            MenuEntree *newEntreeWithLocationAndIngredients = [NSEntityDescription insertNewObjectForEntityForName:@"MenuEntree" inManagedObjectContext:self.managedObjectContext];
            
            id obj = [[self formModel] object];
            
            if([obj isKindOfClass:[MenuEntree class]]){

                if(_selectedLocations){
                    NSMutableArray *selectedLocationsObjectIDs = [NSMutableArray arrayWithCapacity:[_selectedLocations count]];
                    
                    for(NSManagedObject *mo in _selectedLocations){
                        if(mo){
                            [selectedLocationsObjectIDs addObject:[mo objectID]];
                        }
                    }
                    
                    NSFetchRequest *reqLocation = [[NSFetchRequest alloc] initWithEntityName:@"Location"];

                    DDLogVerbose(@"selected location IDs %@", selectedLocationsObjectIDs);

                    [reqLocation setPredicate:[NSPredicate predicateWithFormat:@"SELF IN %@", selectedLocationsObjectIDs]];
                    
                    NSError *error = nil;
                    NSArray *fromFetch = [newEntreeWithLocationAndIngredients.managedObjectContext executeFetchRequest:reqLocation error:&error];
                    
                    if(error){
                      DDLogError(@"%s Error : %@", __PRETTY_FUNCTION__, error);
                    }

                    if(fromFetch != nil){

                        DDLogVerbose(@"The fetched results: %@", fromFetch);
                        [newEntreeWithLocationAndIngredients setValue:[NSOrderedSet orderedSetWithArray:fromFetch] forKey:@"usedInLocations"];
                    }
                }
                
                if(_selectedIngredients){
                    
                    NSMutableArray *selectedIngredientsObjectIDs = [NSMutableArray arrayWithCapacity:[_selectedLocations count]];
                    
                    for(NSManagedObject *object in _selectedLocations){
                        if(object){
                            [selectedIngredientsObjectIDs addObject:[object objectID]];
                        }
                    }
                    
                    NSFetchRequest *reqIngredients = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
                    [reqIngredients setPredicate:[NSPredicate predicateWithFormat:@"SELF IN %@", selectedIngredientsObjectIDs]];
                    
                    NSError *error = nil;
                    NSArray *fromFetch = [newEntreeWithLocationAndIngredients.managedObjectContext executeFetchRequest:reqIngredients error:&error];
                    
                    if(error){
                        DDLogError(@"%s Error: %@", __PRETTY_FUNCTION__, error);
                    }

                    if(fromFetch){
                        [newEntreeWithLocationAndIngredients setContainIngredients:[NSOrderedSet orderedSetWithArray:fromFetch]];
                    }
                }
            }
            
            [self.delegate saveButtonIsClicked:self.formModel.object withObject:newEntreeWithLocationAndIngredients];
        }];
        
        [self.formModel registerMapping:formMapping];
        
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        DDLogVerbose(@"The object:%@ ; value:%@ ; keyPath:%@", object, value, keyPath);
    }];
    
    [self.formModel loadFieldsWithObject:self.entree];
}



#define Multi Selection component delegate

- (void)caller:(id)sender withSelectedItems:(NSMutableArray *)selected
{
    if(selected && [selected count] > 0){
        
        if([self isArray:selected ofThisType:[Location class]]){
            
            DDLogVerbose(@"The _seleted array has a size of %i", [selected count]);
            _selectedLocations = [NSOrderedSet orderedSetWithArray:selected];
        }
        
        if([self isArray:selected ofThisType:[Item class]]) {
            
            _selectedIngredients = [NSOrderedSet orderedSetWithArray:selected];
        }
    } else {
        
        if( [sender isKindOfClass:[PVMultiSelectLocationsTVC class]])
        {
            _selectedLocations = nil;
        }
        
        if( [sender isKindOfClass:[PVMultiSelectItemsTVC class]])
        {
            _selectedIngredients = nil;
        }
     }
    
    [self.navigationController popViewControllerAnimated:YES];
    if(_pathForRowToUpdate != nil){
        [self.entreeTableForm reloadRowsAtIndexPaths:@[_pathForRowToUpdate]
                                    withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (BOOL) isArray:(NSMutableArray *)array ofThisType:(Class)class
{
    for( id obj in array){
        if([obj isKindOfClass:class]){
            return YES;
        }
    }
    return NO;
}


#define MENU_ENTREE_FORM @"Create a menu"

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:MENU_ENTREE_FORM];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // [self.formModel.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewRowAnimationTop animated:YES];
}

@end
