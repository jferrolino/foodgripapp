//
//  PVAlertTableView.h
//  FoodGrip
//
//  Created by ENG002 on 12/18/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertTableViewDelegate
- (void)didSelectRowAtIndex:(NSInteger)row withContext:(id)context;
@end

@interface PVAlertTableView : UIAlertView <UITableViewDelegate,UITableViewDataSource>{
    UITableView *inventoryItemsView;
    id<AlertTableViewDelegate> caller;
    id context;
    NSArray *data;
    int tableHeight;
}

-(id)initWithCaller:(id<AlertTableViewDelegate>)_caller data:(NSArray*)_data title:(NSString*)_title andContext:(id)_context;

@property (nonatomic, strong) id<AlertTableViewDelegate> caller;
@property (nonatomic, strong) id context;
@property (nonatomic, strong) NSArray *data;

@end
