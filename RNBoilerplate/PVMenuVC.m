//
//  PVMenuVC.m
//  NewAssetsMan
//
//  Created by jon.ferrolino on 10/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVMenuVC.h"
#import <Foundation/Foundation.h>
#import "UIImage+iPhone5.h"
#import "MasterCell.h"

@interface PVMenuVC()

@property (nonatomic,strong) NSArray *menuItems;
@property (nonatomic,strong) NSArray *menuItemKeys;

@property (nonatomic, strong) NSArray *sectionRestaurantItems;
@property (nonatomic, strong) NSArray *sectionInventoryItems;
@property (nonatomic, strong) NSArray *sectionOtherItems;

@property (nonatomic, strong) NSArray *sectionRestaurantItemKeys;
@property (nonatomic, strong) NSArray *sectionInventoryItemKeys;
@property (nonatomic, strong) NSArray *sectionOtherItemKeys;

@property (nonatomic, strong) NSArray *menuSections;

@end


@implementation PVMenuVC

@synthesize menuSections;

-(void) awakeFromNib
{
    
    self.menuSections = @[MENU_ITEM_SECTION_RESTAURANT, MENU_ITEM_SECTION_INVENTORY, MENU_ITEM_SECTION_OTHERS];
 
    self.sectionRestaurantItems = @[
                                   MENU_ITEM_RESTAURANT_LOCATION,
                                   MENU_ITEM_RESTAURANT_MENUS,
                                   ];
    self.sectionRestaurantItemKeys = @[
                                       MENU_ITEM_RESTAURANT_LOCATION_KEY,
                                       MENU_ITEM_RESTAURANT_MENUS_KEY
                                       
                                       ];
    
    self.sectionInventoryItems = @[
                                   MENU_ITEM_INVENTORY_CYCLES,
                                   MENU_ITEM_INVENTORY_ITEMS,
                                   ];
    
    self.sectionInventoryItemKeys = @[MENU_ITEM_INVENTORY_CYCLES_KEY,
                                      MENU_ITEM_INVENTORY_ITEMS_KEY
                                      ];
    
    self.sectionOtherItems = @[
                               MENU_ITEM_REPORTS,
                               MENU_ITEM_ANALYTICS,
                               MENU_ITEM_SETTINGS,
                               MENU_ITEM_CREDITS
                               ];
    
    self.sectionOtherItemKeys = @[
                                  MENU_ITEM_REPORTS_KEY,
                                  MENU_ITEM_ANALYTICS_KEY,
                                  MENU_ITEM_SETTINGS_KEY,
                                  MENU_ITEM_CREDITS_KEY
                                  ];
     
}

#define HEADER_HEIGHT 40

- (void) viewDidLoad
{

    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = nil;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.view.backgroundColor=[PVGlobals commonViewBackgroundColor];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    if(sectionIndex == 0){
        return 2;
    }
    
    if(sectionIndex == 1){
        return 2;
    }
    
    return 4;
    
    //return self.menuItems.count;
}

//- (NSInteger) t
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.menuSections count];
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = MENU_ITEM_SECTION_RESTAURANT;//NSLocalizedString(@"mySectionName", @"mySectionName");
            break;
        case 1:
            sectionName = MENU_ITEM_SECTION_INVENTORY;//NSLocalizedString(@"myOtherSectionName", @"myOtherSectionName");
            break;
            // ...
        default:
            sectionName = MENU_ITEM_SECTION_OTHERS;//@"";
            break;
    }
    return sectionName;
}


-(CALayer *)createShadowWithFrame:(CGRect)frame
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = frame;
    
    UIColor* lightColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    UIColor* darkColor = [[UIColor blackColor] colorWithAlphaComponent:0.3];
    
    gradient.colors = [NSArray arrayWithObjects:(id)darkColor.CGColor, (id)lightColor.CGColor, nil];
    
    return gradient;
}

//- (UITableViewCell*)tab

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];    
    cell.backgroundView = [PVGlobals commonCellBackgroundView:cell];
    
    [cell.textLabel setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
    [cell.textLabel setShadowColor:[UIColor whiteColor]];
    [cell.textLabel setShadowOffset:CGSizeMake(0, 1)];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    
    if(indexPath.section == 0){
        
        [cell.textLabel setText:[self.sectionRestaurantItems objectAtIndex:[indexPath row]]];
    }
    
    if(indexPath.section == 1){
        [cell.textLabel setText:[self.sectionInventoryItems objectAtIndex:[indexPath row]]];
    }
    
    
    if(indexPath.section == 2){
        [cell.textLabel setText:[self.sectionOtherItems objectAtIndex:[indexPath row]]];
    }
    
    
    if(indexPath.row == 8 ){
        CALayer *shadow = [self createShadowWithFrame:CGRectMake(0,67,320,5)];
        [cell.layer addSublayer:shadow];
    }

    
    return cell;
}

#define NOT_AVAILABLE_TITLE @"Not available"
#define NOT_AVAILABLE_MESSAGE @"This feature is not yet available for testing. Sorry"

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = nil;
    
    if(indexPath.section == 0)
        identifier = [NSString stringWithFormat:@"%@%@", [self.sectionRestaurantItemKeys objectAtIndex:indexPath.row], SUFFIX_FOR_MENU_ITEMS];
    else if(indexPath.section == 1)
        identifier = [NSString stringWithFormat:@"%@%@", [self.sectionInventoryItemKeys objectAtIndex:indexPath.row], SUFFIX_FOR_MENU_ITEMS];
    else
        identifier = [NSString stringWithFormat:@"%@%@", [self.sectionOtherItemKeys objectAtIndex:indexPath.row], SUFFIX_FOR_MENU_ITEMS ];
    
    
    @try{
        UIViewController *newTopViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    
        [self.slidingViewController anchorTopViewOffScreenTo:ECRight animations:nil onComplete:^{
            CGRect frame = self.slidingViewController.topViewController.view.frame;
            self.slidingViewController.topViewController = newTopViewController;
            self.slidingViewController.topViewController.view.frame = frame;
            [self.slidingViewController.topViewController.view.layer setShadowOpacity:0.75f];
            [self.slidingViewController.topViewController.view.layer setShadowRadius:10.0f];
            [self.slidingViewController.topViewController.view.layer setShadowColor:[UIColor blackColor].CGColor];
            
            [self.slidingViewController.topViewController.view setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
            
            [self.slidingViewController resetTopView];
        }];
    } @catch (NSException *ne) {
        [[[UIAlertView alloc] initWithTitle:NOT_AVAILABLE_TITLE
                                    message:NOT_AVAILABLE_MESSAGE
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show ];
    }
}
@end
