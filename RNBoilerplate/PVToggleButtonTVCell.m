//
//  PVToggleButtonTVCell.m
//  FoodGrip
//
//  Created by ENG002 on 12/28/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVToggleButtonTVCell.h"

@implementation PVToggleButtonTVCell

@synthesize switchControl = _switchControl;


- (void) initializeInputView
{
    [self setSwitchControl:[[UISwitch alloc]init]];
    [self.switchControl setOn:NO];
    [self setAccessoryView:[[UIView alloc] initWithFrame:self.switchControl.frame]];
    [self.accessoryView addSubview:self.switchControl];
}

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self initializeInputView];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initializeInputView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
	[super setSelected:selected];
	if (selected) {
		[self.switchControl becomeFirstResponder];
	}
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
	if (selected) {
		[self.switchControl becomeFirstResponder];
	}
}

- (void)setBoolValue:(BOOL)value {
	self.switchControl.on = value;
}

- (BOOL)boolValue {
	return self.switchControl.on;
}

@end
