//
//  PVMultiSelectLocationsTVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/14/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//
#import "PVMultiSelectLocationsTVC.h"
#import "CRTableViewCell.h"


@implementation PVMultiSelectLocationsTVC

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setRightBarButtonItem:nil];
    
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                                style:UIBarButtonItemStyleDone
                                                                               target:self
                                                                               action:@selector(done:)]];
    
    [self.tableView setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    [self.tableView setDataSource:self];
    [self.tableView setDelegate: self];
    
    [super loadRecordsFromCoreData];
}

- (void) done:(id)sender
{
    if([self delegate]){
        
        if(self.selectedMarks && [self.selectedMarks count] > 0)
            [self.delegate caller:self
                withSelectedItems:self.selectedMarks];
        else
            [self.delegate caller:self
                withSelectedItems:[NSMutableArray new]];
    }
}

- (void) viewDidDisappear:(BOOL)animated
{
    DDLogVerbose(@"Alert: Multi selection is disappearing!!!");
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CRTableViewCellIdentifier = @"cellIdentifier";
    
    CRTableViewCell *cell = (CRTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CRTableViewCellIdentifier];
    
    if (cell == nil) {
        cell = [[CRTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CRTableViewCellIdentifier];
    }
    
    Location *location = [self.dataSource objectAtIndex:[indexPath row]];
    
    DDLogVerbose(@"The location %@", location);    
    cell.isSelected = [self isInSelectedMarks:location];//[self isInSelectedMarks:location removeIfFound:NO];
    
    DDLogVerbose(@"The cell is selected : %i", cell.isSelected );
    
    [cell.textLabel setText:[location name]];
    [cell setBackgroundView:[PVGlobals commonCellBackgroundView:cell]];
    
    return cell;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Location *location = [self.dataSource objectAtIndex:[indexPath row]];

    [self alreadyInSelectedMarks:location];
    
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                     withRowAnimation:UITableViewRowAnimationAutomatic];
    
}

@end
