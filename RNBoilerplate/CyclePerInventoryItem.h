//
//  CyclePerInventoryItem.h
//  FoodGrip
//
//  Created by ENG002-JRF on 2/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cycle, Item;

@interface CyclePerInventoryItem : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * purchaseCount;
@property (nonatomic, retain) NSNumber * quantityCount;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) Cycle *by;
@property (nonatomic, retain) Item *forItem;


- (id)initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context;

@end
