//
//  PVInventoryItemVC.h
//  FoodGrip
//
//  Created by ENG002 on 12/19/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface PVInventoryItemVC : UIViewController

@property (nonatomic,strong) NSString* entityName;
@property (nonatomic, strong) NSManagedObject *invItem;

//- (void) saveBulk:(NSArray *)invItems;

@end
