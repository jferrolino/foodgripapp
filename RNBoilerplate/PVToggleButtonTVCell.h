//
//  PVToggleButtonTVCell.h
//  FoodGrip
//
//  Created by ENG002 on 12/28/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVToggleButtonTVCell : UITableViewCell {
    UISwitch *switchControl;
}

@property (nonatomic) BOOL value;
@property (nonatomic, strong) UISwitch *switchControl;

@end
