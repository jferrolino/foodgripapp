//
//  PVEditableTableDataRowItem.m
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVEditableTableDataRowItem.h"
#import "PVGlobals.h"

@interface PVEditableTableDataRowItem()

@property (nonatomic,strong) UIControl *tempHolder;
@end

@implementation PVEditableTableDataRowItem

@synthesize controlAlt = _controlAlt;
@synthesize toEdit = _toEdit;
@synthesize tempHolder;

- (id)initWithRowItemControlType:(int)controlType selections:(NSArray *)selections selectionListKey:(NSString *)selectionListKey baseSize:(CGSize)size canResize:(BOOL)resize normalImage:(UIImage *)normalButtonImage selectedImage:(UIImage *)selectedButtonImage controlLabel:(NSString *)label buttonTarget:(id)target buttonAction:(SEL)action asCurrency:(BOOL)asCurrency usingNumberedKeyboard:(BOOL)useNumberKeyboard
{
    self = [self initWithRowItemControlType:controlType selections:selections selectionListKey:selectionListKey baseSize:size canResize:resize normalImage:normalButtonImage selectedImage:selectedButtonImage controlLabel:label buttonTarget:target buttonAction:action usingNumberedKeyboard:useNumberKeyboard];
    if(self){
        if(asCurrency)
            [_controlAlt setText:[PVGlobals formatCurrencyToString:[NSDecimalNumber decimalNumberWithString:label]]];
    }
    return self;
}

// Full initializer allowing for one-step setup of the control
- (id)initWithRowItemControlType:(int)controlType selections:(NSArray *)selections selectionListKey:(NSString *)selectionListKey baseSize:(CGSize)size canResize:(BOOL)resize normalImage:(UIImage *)normalButtonImage selectedImage:(UIImage *)selectedButtonImage controlLabel:(NSString *)label buttonTarget:(id)target buttonAction:(SEL)action
{
    self = [super initWithRowItemControlType:controlType selections:selections selectionListKey:selectionListKey baseSize:size canResize:resize normalImage:normalButtonImage selectedImage:selectedButtonImage controlLabel:label buttonTarget:target buttonAction:action];
    if(self){
        _controlAlt = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        
        [_controlAlt setFont:[UIFont systemFontOfSize:DEFAULT_TEXTFIELD_FONT_SIZE]];
        [_controlAlt setTextAlignment:NSTextAlignmentCenter];
        
        [_controlAlt setTextColor:[UIColor colorWithRed:0.0 green:68.0/255 blue:118.0/255 alpha:1.0]];
        [_controlAlt setShadowColor:[UIColor whiteColor]];
        [_controlAlt setShadowOffset:CGSizeMake(0, 1)];
        [_controlAlt setBackgroundColor:[UIColor clearColor]];
        
        _controlAlt.numberOfLines = 3;
        _controlAlt.lineBreakMode = NSLineBreakByWordWrapping;
        
        
        
        //[_controlAlt sizeToFit];
        [_controlAlt setText:label];
    }
    return self;
}

- (void)toggleEditingMode:(BOOL)toEdit
{
    // holder tempororarily currently used control
    if(!toEdit){
        [self.control setHidden:YES];
        [self.controlAlt setHidden:NO];
    } else {
        [self.control setHidden:NO];
        [self.controlAlt setHidden:YES];
    }
    self.toEdit = toEdit;
}

- (void)toggleControlAndLabelOnly:(BOOL)toEdit
{
    if(!toEdit){
        [self.control setHidden:YES];
    }
    self.toEdit = toEdit;
}

- (void) changeControlLabelTo:(NSString *)newValue
{
    // Checks what controlType an instance is using.. and set the new value in regard to that.
    if(self.itemControlType == ControlTypeTextField){
        NSLog(@"Attempting to set new value to a control of type: ControlTypeTextField");
        [(UITextField *)self.control setText:newValue];
    }else if(self.itemControlType == ControlTypePopup || self.itemControlType == ControlTypeCombo){
        if(self.normalImage){
            [((UIButton *)self.control) setTitle:newValue forState:UIControlStateNormal];
        }else {
            [[((UIButton *)self.control) titleLabel]setText:newValue];
        }
    }else if(self.itemControlType == ControlTypeButton){
        NSLog(@"Attempting to set new value to a control of type: ControlTypeButton as normalImage? %@",self.normalImage);
        if(self.normalImage){
            [((UIButton *)self.control) setTitle:newValue forState:UIControlStateNormal];
        } else{
            [[((UIButton *)self.control) titleLabel] setText:newValue];
        }
    }else if(self.itemControlType == ControlTypeToggleButton){
        // attempting set new value to a control of type "ControlTypeToggleButton"
        NSLog(@"Attempting to set new value to a control of type: ControlTypeToggleButton");
    }
}

@end
