//
//  PVAssetsManTVC.m
//  NewAssetsMan
//
//  Created by jon.ferrolino on 10/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVAssetsManTVC.h"

@interface PVAssetsManTVC ()

@end

@implementation PVAssetsManTVC

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        // Customize the table
        
        // The className to query on
        self.className = @"Asset";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"text";
        
        // Uncomment the following line to specify the key of a PFFile on the PFObject to display in the imageView of the default cell style
        // self.imageKey = @"image";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 25;
    }
    return self;
}

- (UITableViewCell *) tableView:(UITableView *)tableView
          cellForRowAtIndexPath:(NSIndexPath *)indexPath
                         object:(PFObject *)object
{
    static NSString *cellIdentifier = @"AssetsCell";
    
    PFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell){
        cell = [[PFTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    // Configure the cell to show todo item with a priority at the bottom
    cell.textLabel.text = [object objectForKey:@"name"];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ , %@",[object objectForKey:@"inventoryNumber"],[object objectForKey:@"description"]];
    
    return cell;
}


- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuTop"];
    
    //[self.view addGestureRecognizer:self.slidingViewController.panGesture];
    //[self.tableView addGestureRecognizer:self.slidingViewController.panGesture];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
}

@end
