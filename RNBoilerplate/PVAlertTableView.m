//
//  PVAlertTableView.m
//  FoodGrip
//
//  Created by ENG002 on 12/18/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//
//  This is based from : http://starterstep.wordpress.com/2009/03/24/custom-uialertview-with-uitableview/

#import "PVAlertTableView.h"

@interface PVAlertTableView()
-(void) prepare;
@end

@implementation PVAlertTableView

@synthesize caller, context, data;

- (id)initWithCaller:(id)_caller data:(NSArray *)_data title:(NSString *)_title andContext:(id)_context
{
    NSMutableString *messageString = [NSMutableString stringWithString:@"\n"];
    tableHeight = 0;
    if([_data count] < 6){
        for(int i = 0; i < [_data count]; i++){
            [messageString appendString:@"\n\n"];
            tableHeight += 53;
        }
    } else {
        messageString = [NSMutableString stringWithString:@"\n\n\n\n\n\n\n\n\n\n"];
        tableHeight = 207;
    }
    
    if(self = [super initWithTitle:_title message:messageString delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil]){
        self.caller = _caller;
        self.context = _context;
        self.data = _data;
        [self prepare];
    }
    return self;
}

- (void) alertView:(UIAlertView * )alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.caller didSelectRowAtIndex:-1 withContext:self.context];
}

- (void)show{
    self.hidden = YES;
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(myTimer:) userInfo:nil repeats:NO];
    [super show];
}

- (void) prepare{
    inventoryItemsView = [[UITableView alloc]initWithFrame:CGRectMake(11, 50, 261, tableHeight) style:UITableViewStylePlain];
    if([data count] < 5){
        inventoryItemsView.scrollEnabled = NO;
    }
    inventoryItemsView.delegate = self;
    inventoryItemsView.dataSource = self;
    [self addSubview:inventoryItemsView];
    
    /*
    UIImageView *imgView = [[[UIImageView alloc] initWithFrame:CGRectMake(11, 50, 261, 4)] autorelease];
    56
    imgView.image = [UIImage imageNamed:@"top.png"];
    57
    [self addSubview:imgView];
    58
    
    59
    imgView = [[[UIImageView alloc] initWithFrame:CGRectMake(11, tableHeight+46, 261, 4)] autorelease];
    60
    imgView.image = [UIImage imageNamed:@"bottom.png"];
    61
    [self addSubview:imgView];
    62
    
    63
    
    64
    CGAffineTransform myTransform = CGAffineTransformMakeTranslation(0.0, 10);
    65
    [self setTransform:myTransform];
     */

}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"ABC"];
    
    if(cell == nil) {
        //cell = [[UITableViewCell alloc]initWithFrame:CGRectZero reuseIdentifier:@"ABC"];
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        //cell.font = [UIFont boldSystemFontOfSize:14];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    
    cell.textLabel.text = [[data objectAtIndex:indexPath.row] description];
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissWithClickedButtonIndex:0 animated:YES];
    [self.caller didSelectRowAtIndex:indexPath.row withContext:self.context];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [data count];
}

- (void) dealloc{
    self.data = nil;
    self.caller = nil;
    self.context = nil;
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)myTimer:(NSTimer *)_timer{
    self.hidden = NO;
    [inventoryItemsView flashScrollIndicators];
}

@end
