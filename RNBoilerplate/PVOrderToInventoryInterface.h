//
//  PVOrderUnitRefDataInterface.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OrderUnitRefData.h"
#import "ReferenceData.h"

@protocol PVOrderToInventoryInterface <NSObject>

@property (nonatomic, retain) NSString * equivalenceDesc;
@property (nonatomic, retain) NSNumber * toInventoryNumberValue;
@property (nonatomic, retain) OrderUnitRefData *fromOrder;
@property (nonatomic, retain) ReferenceData *toInventory;

@end
