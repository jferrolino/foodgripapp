//
//  NSManagedObject+JSON.m
//  FoodGrip
//
//  Created by ENG002 on 12/20/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "NSManagedObject+JSON.h"

@implementation NSManagedObject (JSON)

- (NSDictionary *)JSONToCreateObjectOnServer
{
    @throw [NSException exceptionWithName:@"JSONStringToCreateObjectOnServer Not Overridden" reason:@"Must override JSONStringToCreateObjectOnServer on NSManagedObject class" userInfo:nil];
    return nil;
}



@end
