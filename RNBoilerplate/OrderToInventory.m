//
//  OrderToInventory.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "OrderToInventory.h"
#import "OrderUnitRefData.h"
#import "ReferenceData.h"


@implementation OrderToInventory

@dynamic equivalenceDesc;
@dynamic toInventoryNumberValue;
@dynamic fromOrder;
@dynamic toInventory;

@end
