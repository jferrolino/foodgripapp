//
//  Item.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CyclePerInventoryItem, MenuEntree, ReferencePhotoData;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDecimalNumber * extension;
@property (nonatomic, retain) NSNumber * inventoryCount;
@property (nonatomic, retain) NSDecimalNumber * inventoryPrice;
@property (nonatomic, retain) NSString * inventoryUnit;
@property (nonatomic, retain) NSString * itemNumber;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSDecimalNumber * orderPrice;
@property (nonatomic, retain) NSNumber * orderToInventory;
@property (nonatomic, retain) NSString * orderUnit;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSOrderedSet *containPhotos;
@property (nonatomic, retain) NSSet *inCycles;
@property (nonatomic, retain) NSOrderedSet *usedInMenuEntree;

- (id)initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context;
@end

@interface Item (CoreDataGeneratedAccessors)

- (void)insertObject:(ReferencePhotoData *)value inContainPhotosAtIndex:(NSUInteger)idx;
- (void)removeObjectFromContainPhotosAtIndex:(NSUInteger)idx;
- (void)insertContainPhotos:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeContainPhotosAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInContainPhotosAtIndex:(NSUInteger)idx withObject:(ReferencePhotoData *)value;
- (void)replaceContainPhotosAtIndexes:(NSIndexSet *)indexes withContainPhotos:(NSArray *)values;
- (void)addContainPhotosObject:(ReferencePhotoData *)value;
- (void)removeContainPhotosObject:(ReferencePhotoData *)value;
- (void)addContainPhotos:(NSOrderedSet *)values;
- (void)removeContainPhotos:(NSOrderedSet *)values;
- (void)addInCyclesObject:(CyclePerInventoryItem *)value;
- (void)removeInCyclesObject:(CyclePerInventoryItem *)value;
- (void)addInCycles:(NSSet *)values;
- (void)removeInCycles:(NSSet *)values;

- (void)insertObject:(MenuEntree *)value inUsedInMenuEntreeAtIndex:(NSUInteger)idx;
- (void)removeObjectFromUsedInMenuEntreeAtIndex:(NSUInteger)idx;
- (void)insertUsedInMenuEntree:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeUsedInMenuEntreeAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInUsedInMenuEntreeAtIndex:(NSUInteger)idx withObject:(MenuEntree *)value;
- (void)replaceUsedInMenuEntreeAtIndexes:(NSIndexSet *)indexes withUsedInMenuEntree:(NSArray *)values;
- (void)addUsedInMenuEntreeObject:(MenuEntree *)value;
- (void)removeUsedInMenuEntreeObject:(MenuEntree *)value;
- (void)addUsedInMenuEntree:(NSOrderedSet *)values;
- (void)removeUsedInMenuEntree:(NSOrderedSet *)values;
@end
