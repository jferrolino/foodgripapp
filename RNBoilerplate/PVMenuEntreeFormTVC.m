//
//  PVMenuEntreeFormTVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/5/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMenuEntreeFormTVC.h"
#import "FKFormMapping.h"
#import "SDCoreDataController.h"


@interface PVMenuEntreeFormTVC ()

@end

@implementation PVMenuEntreeFormTVC

@synthesize entree;
@synthesize navigationController = _navigationController;

@synthesize managedObjectContext = _managedObjectContext;

- (NSManagedObjectContext *) managedObjectContext 
{
    if([self datasource]){
        if( [self.datasource userManagedObjectContext]){}
            
            NSManagedObjectContext *moc = [self.datasource useManagedObjectContext];
            
            _managedObjectContext = moc;
        } else {
            _managedObjectContext = [[SDCoreDataController sharedInstance] backgroundManagedObjectContext];        
        }
    }
    return _managedObjectContext;
}

- (id) initWithFrame:(CGRect) frame underNavigationController:(UINavigationController *)navController
{
    if( (self = [super init]))
    {
        [self setNavigationController:navController];
        [self.view setFrame:frame];
    }
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil underNavigationController:(UINavigationController *) navController
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]){
        [self setNavigationController:navController];
    }
    return self;
}

- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]){
        
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self setTitle:@"Create a Menu"];
    
    DDLogVerbose(@"The navigation Controller from the form %@", self.navigationController);
    
    self.formModel = [FKFormModel formTableModelForTableView:self.tableView
                                        navigationController:self.navigationController];
    
    MenuEntree *menuEntree = [[MenuEntree alloc]initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"MenuEntree" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
    
    self.entree = menuEntree;
    
    [FKFormMapping mappingForClass:[MenuEntree class] block:^(FKFormMapping *formMapping) {
        
         
        
        [formMapping sectionWithTitle:@"Menu Entree Info" footer:@"Thank you!" identifier:@"Menu Entree info"];
        
        [formMapping mapAttribute:@"name" title:@"Name" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"sellingPrice" title:@"Selling Price" type:FKFormAttributeMappingTypeFloat];
        
        [formMapping mapAttribute:@"forPhotos"
                            title:@"Found in Locations"
                selectValuesBlock:^NSArray *(id value, id object, NSInteger *selectedValueIndex){
                     showInPicker:NO
                    *selectedValueIndex = 1;
                    
                    return [NSOrderedSet orderedSet]; /*[NSOrderedSet orderedSetWithArray:[NSArray arrayWithObjects:@"location1", @"location2", nil]
                                                       range:
                                                   copyItems:YES];*/
                    
                    
        } valueFromSelectBlock:^id(id value, id object, NSInteger selectedValueIndex) {
            return value;
            
        } labelValueBlock:^id(id value, id object) {
            return value;
            
        }];
        
        [formMapping buttonSave:@"Save" handler:^{
            
            // call delegate only when successful
            [self.delegate saveButtonIsClicked:self.formModel withObject:self.formModel.object];
        }];
        [self.formModel registerMapping:formMapping];
        //_formMapping = formMapping;
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        //DDLogWarn(@"Did change model value");
        
        DDLogVerbose(@"The object:%@ ; value:%@ ; keyPath:%@", object, value, keyPath);
    }];
    
    [self.formModel loadFieldsWithObject:self.entree];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidUnload {
    [super viewDidUnload];
    
    self.formModel = nil;
    self.entree = nil;
}

@end
