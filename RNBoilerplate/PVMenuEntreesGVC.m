//
//  PVMenusGVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 2/18/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMenuEntreesGVC.h"

#define DEPTH_VIEW_ADD_MENU @"Add a menu"
#define MENU_ENTREE_ENTITY_NAME @"MenuEntree"

@interface PVMenuEntreesGVC (){
    
    JFDepthView *depthView;
    PVSimpleHUDController *HUDController;
    
    NSInteger _lastDeleteItemIndexAsked;
}

@property (nonatomic, weak) GMGridView *gridView;
@property (nonatomic, strong) JFDepthView *depthView;

@property (nonatomic, strong) PVTouchAwareParallaxPicturesVC *parallaxViewController;
@property (nonatomic, strong) UIPopoverController *popupController;

@property (nonatomic, strong) UINavigationController *formNavController;
@property (nonatomic, strong) NSArray *imagesLoaded;

- (void)addMoreItem;
- (void)removeItem;
- (void)refreshItem;
- (void)presentInfo;
- (void)presentOptions:(UIBarButtonItem *)barButton;
- (void)optionsDoneAction;
- (void)dataSetChange:(UISegmentedControl *)control;

@end

@implementation PVMenuEntreesGVC

@synthesize gridView = _gridView;
@synthesize depthView = _depthView;
@synthesize parallaxViewController;
@synthesize popupController;
@synthesize managedObjectContext = _managedObjectContext;

@synthesize formNavController = _formNavController;

@synthesize imagesLoaded = _imagesLoaded;
@synthesize currentData = _currentData;

- (id)init
{
    self = [super init];
    if(self){
        [self loadRecordsFromCoreData];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self = [self init];
    }
    return self;
}

- (NSManagedObjectContext *) managedObjectContext
{
    if(!_managedObjectContext) {
        _managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    }
    return _managedObjectContext;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    
    [self setTitle:MENU_ITEM_RESTAURANT_MENUS];
    
    self.gridView.mainSuperView = [self.navigationController view];
    HUDController = [[PVSimpleHUDController alloc]init];
    
    [self.navigationItem.leftBarButtonItem setCustomView:[PVGlobals menuBarButtonThemeFromTarget:self withAction:@selector(revealMenu:)]];
        
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    
    self.depthView = [[JFDepthView alloc] initWithGestureRecognizer:tapRec];
    [self.depthView setDelegate:self];
}

- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *menuIdentifier = [NSString stringWithFormat:@"%@%@",MENU_ITEM_THE_MENU_KEY, SUFFIX_FOR_MENU_ITEMS];
    
    self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:menuIdentifier];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"SDSyncEngineSyncCompleted" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self loadRecordsFromCoreData];
        [self.gridView reloadData];
    }];
    [[SDSyncEngine sharedEngine] addObserver:self forKeyPath:@"syncInProgress" options:NSKeyValueObservingOptionNew context:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadRecordsFromCoreData
{
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"MenuEntree"];
        
        /*[request setSortDescriptors:[NSArray arrayWithObject:
                                     [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES]]];
        */
        //[request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        
        self.currentData = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
        DDLogVerbose(@"The currentData count %i", [self.currentData count]);

    }];
}

//////////////////////////////////////////////////////////////
#pragma mark controller events
//////////////////////////////////////////////////////////////

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [PVGlobals commonViewBackgroundColor];
    
    NSInteger spacing = INTERFACE_IS_PHONE ? 10 : 15;
    
    CGRect frame = CGRectMake(0, 0, self->_contentView.bounds.size.width, self->_contentView.bounds.size.height);
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:frame];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    
    _gridView = gmGridView;
    
    _gridView.style = GMGridViewStylePush;
    _gridView.itemSpacing = spacing;
    _gridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gridView.centerGrid = YES;
    _gridView.actionDelegate = self;
    _gridView.sortingDelegate = self;
    _gridView.transformDelegate = self;
    _gridView.dataSource = self;
    _gridView.enableEditOnLongPress = YES;
    _gridView.disableEditOnEmptySpaceTap= YES;
    _gridView.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontalPagedLTR];
    
    _gridView.backgroundColor = [PVGlobals commonViewBackgroundColor];
    
    
    _imageScroller  = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _imageScroller.backgroundColor                  = [UIColor clearColor];
    _imageScroller.showsHorizontalScrollIndicator   = NO;
    _imageScroller.showsVerticalScrollIndicator     = NO;
    _imageScroller.pagingEnabled                    = YES;
    
    NSArray *images = [PVGlobals prepareBannerWith:self.currentData forView:self.view];
    _imageViews = [NSMutableArray arrayWithCapacity:[images count]];
    
    [self addImages:images];
    
    _transparentScroller = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _transparentScroller.backgroundColor                = [UIColor clearColor];
    _transparentScroller.delegate                       = self;
    _transparentScroller.bounces                        = NO;
    _transparentScroller.pagingEnabled                  = YES;
    _transparentScroller.showsVerticalScrollIndicator   = YES;
    _transparentScroller.showsHorizontalScrollIndicator = NO;
    
    _contentScrollView = [[UIScrollView alloc] init];
    _contentScrollView.backgroundColor              = [UIColor clearColor];
    _contentScrollView.delegate                     = self;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    
    
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.currentPage = 0;
    [_pageControl setHidesForSinglePage:YES];
    
    [_contentScrollView addSubview:_gridView];
    [_contentScrollView addSubview:_pageControl];
    [_contentScrollView addSubview:_transparentScroller];
    
    _contentView = _gridView;
    [self.view addSubview:_imageScroller];
    [self.view addSubview:_contentScrollView];
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (void) dataSetChange:(UISegmentedControl *)SegmentedControl
{
    [self.gridView reloadData];
}

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [self.currentData count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(170, 135);
        }
        else
        {
            return CGSizeMake(140, 110);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(285, 205);
        }
        else
        {
            return CGSizeMake(230, 175);
        }
    }
}

#define GRID_CLOSE_BUTTON_ICON_NAMED @"close_x.png"
#define GRID_LAYER_CORNER_RADIUS 8

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:self.gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    GMGridViewCell *cell = [self.gridView dequeueReusableCell];
    
    if(!cell){
        cell = [[GMGridViewCell alloc] init];
    }
    
         cell.deleteButtonIcon = [UIImage imageNamed:GRID_CLOSE_BUTTON_ICON_NAMED];
        cell.deleteButtonOffset = CGPointMake(-15, -15);
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
        UIGraphicsBeginImageContext(view.frame.size);
        
        MenuEntree *entree = [self.currentData objectAtIndex:index];
        ReferencePhotoData *photoData = nil;

        if([entree.containPhotos count ] > 0){        
            for(ReferencePhotoData *first in entree.containPhotos){
                photoData = first;
                break;
            }
        }
                
        NSString * name = [entree name];
        
        if(photoData){
            UIImage *entreeAvatar = [UIImage imageWithData:[photoData photoData]];
            
            if(entreeAvatar){
                [entreeAvatar drawInRect:view.bounds];
            }
        }
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        if(image)
            view.backgroundColor = [UIColor colorWithPatternImage:image];

        view.layer.masksToBounds = NO;
        view.layer.cornerRadius = GRID_LAYER_CORNER_RADIUS;
        
        cell.contentView = view;
     
    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    if(name)
        [cell.contentView addSubview:[PVGlobals generateGridContentView:cell.contentView.bounds withLabels:@[name]]];
    
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return YES;
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    DDLogWarn(@"Did tap at index %d", position);
    
    // Pass in the view controller you want to present (self.topViewController)
    // and the view you want it to be displayed within (self.view)
    //[self.depthView presentViewController:self.locDetailsVC inView:self.view animated:YES];
    
    // Optionally, if you don't care about rotation support, you can just pass in
    // the view you want to present (self.topViewController.view)
    // and the view you want it to be displayed within (self.view)
    //[self.depthView presentView:self.navController.view inView:self.view];
}


- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    DDLogWarn(@"Tap on empty space");
}

#define MESSAGE_TO_DELETE_ITEM @"Are you sure you want to delete this item?"
#define ALERT_VIEW_TITLE_CONFIRM @"Confirm"
#define CANCEL_BUTTON_TITLE @"Cancel"
#define DELETE_BUTTON_TITLE @"Delete"

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm" message:@"Are you sure you want to delete this item?" delegate:self cancelButtonTitle:CANCEL_BUTTON_TITLE otherButtonTitles:DELETE_BUTTON_TITLE, nil];
    
    [alert show];
    
    _lastDeleteItemIndexAsked = index;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // invoked
    if (buttonIndex == 1)
    {
        NSManagedObject *itemToRemove = [self.currentData objectAtIndex:_lastDeleteItemIndexAsked];
        [self.managedObjectContext performBlockAndWait:^{
            if ([[itemToRemove valueForKey:@"objectId"] isEqualToString:@""] || [itemToRemove valueForKey:@"objectId"] == nil) {
                [self.managedObjectContext deleteObject:itemToRemove];
            } else {
                [itemToRemove setValue:[NSNumber numberWithInt:SDObjectDeleted] forKey:@"syncStatus"];
            }
            NSError *error = nil;
            BOOL saved = [self.managedObjectContext save:&error];
            if (!saved) {
                DDLogError(@"Error saving main context: %@", error);
            }
            
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
            //[[SDSyncEngine sharedEngine] startSync];
            [self loadRecordsFromCoreData];
            
            [_gridView removeObjectAtIndex:_lastDeleteItemIndexAsked withAnimation:GMGridViewItemAnimationFade];
        }];
    }
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewSortingDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor orangeColor];
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil
     ];
}

- (void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor redColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil
     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return YES;
}

- (void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    NSObject *object = [self.currentData objectAtIndex:oldIndex];
    [self.currentData removeObject:object];
    [self.currentData insertObject:object atIndex:newIndex];
}

- (void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
    [self.currentData exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
}


//////////////////////////////////////////////////////////////
#pragma mark DraggableGridViewTransformingDelegate
//////////////////////////////////////////////////////////////

- (CGSize)GMGridView:(GMGridView *)gridView sizeInFullSizeForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index inInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(320, 210);
        }
        else
        {
            return CGSizeMake(300, 310);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(700, 530);
        }
        else
        {
            return CGSizeMake(600, 500);
        }
    }
}

- (UIView *)GMGridView:(GMGridView *)gridView fullSizeViewForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    UIView *fullView = [[UIView alloc] init];
    fullView.backgroundColor = [UIColor yellowColor];
    fullView.layer.masksToBounds = NO;
    fullView.layer.cornerRadius = 8;
    
    CGSize size = [self GMGridView:_gridView sizeInFullSizeForCell:cell atIndex:index inInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    fullView.bounds = CGRectMake(0, 0, size.width, size.height);
    
    UILabel *label = [[UILabel alloc] initWithFrame:fullView.bounds];
    label.text = [NSString stringWithFormat:@"Fullscreen View for cell at index %d", index];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (INTERFACE_IS_PHONE)
    {
        label.font = [UIFont boldSystemFontOfSize:15];
    }
    else
    {
        label.font = [UIFont boldSystemFontOfSize:20];
    }
    
    [fullView addSubview:label];
    
    
    return fullView;
}

- (void)GMGridView:(GMGridView *)gridView didStartTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor blueColor];
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEndTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor redColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEnterFullSizeForCell:(UIView *)cell
{
    
}

#pragma mark -

#define MENU_ENTREE_DEFAULT_IMAGE @"entree-default.jpg"
#define MENU_ENTREE_FORM_ID @"MENU_ENTREE_PARALLAX_TOP"

- (IBAction) addMoreItem:(id)sender
{
    // Example: adding object at the last position
    if([self.depthView isPresenting]){
        [self.depthView dismissPresentedViewInView:self.view animated:YES];
        self.parallaxViewController = nil;
    } else {

        UIBarButtonItem *picButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                                   target:self
                                                                                   action:@selector(takePicture:)];
        
        
        // JOHN: updated with new controller
        self.formNavController = [self.storyboard instantiateViewControllerWithIdentifier:MENU_ENTREE_FORM_ID];
        
        
        PVMenuEntreeFormWithParallaxVC *entreeForm = (PVMenuEntreeFormWithParallaxVC *)[self.formNavController topViewController];
        [entreeForm setNavigationController:self.formNavController];
        [entreeForm.navigationItem setRightBarButtonItem:picButton];
        [entreeForm setTitle:DEPTH_VIEW_ADD_MENU];
        [entreeForm setDelegate:self];
        [entreeForm setDatasource:self];
        
         
        [self.depthView setDelegate:self];
        [self.depthView presentViewController:self.formNavController inView:self.view animated:YES];
    }
}


- (void)takePicture:(id)sender
{
    if(self.popupController)
        [self.popupController dismissPopoverAnimated:YES];
    
    PVMenuEntreeFormWithParallaxVC *entreeForm = (PVMenuEntreeFormWithParallaxVC *)[self.formNavController topViewController];
    
   if(entreeForm){
        // Present the popover
        [self.popupController presentPopoverFromBarButtonItem:entreeForm.navigationItem.rightBarButtonItem
                                     permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
}

#pragma mark - JF Depth View delegate methods

- (void)willPresentDepthView:(JFDepthView*)depthView
{
    [self.addButton setEnabled:NO];
    
    PVCameraViewController *cameraViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraController"];
    [cameraViewController setCameraDelegate:self];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:cameraViewController];
    self.popupController = [[UIPopoverController alloc] initWithContentViewController:navController];
    [self.popupController setPopoverContentSize:CGSizeMake(150 * 3, 100 * 3) animated:YES];
    [self.popupController setDelegate:self];
    
    //[self.navigationItem setRightBarButtonItems:@[self.addButton, self.pictureButton] animated:YES];
}

- (void)didDismissDepthView:(JFDepthView*)depthView
{
    [self.addButton setEnabled:YES];
    [self.navigationItem setRightBarButtonItems:@[self.addButton] animated:YES];
}

///////////////////////////////////////////
#pragma mark - Depth View methods
///////////////////////////////////////////
// Here is the simple dismissal method called from the tap recognizer passed into init method of JFDepthView
- (void)dismiss {
    [self.depthView dismissPresentedViewInView:self.view animated:YES];
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if([self.imagesLoaded count] > 0){
        PVMenuEntreeFormWithParallaxVC *entreeForm = (PVMenuEntreeFormWithParallaxVC *)[self.formNavController topViewController];
        [entreeForm setImages:self.imagesLoaded];
    }
}

- (void) uploadedImages:(NSArray *)images
{
    DDLogVerbose(@"The images are %@", images);
    
    if(self.imagesLoaded == nil){
        self.imagesLoaded = [NSMutableArray array];
    }
    
    NSArray *temp = [self.imagesLoaded copy];
    
    temp = [temp arrayByAddingObjectsFromArray:images];
    
    self.imagesLoaded = [[PVGlobals deduplicateImages:temp] mutableCopy];

}

#pragma mark - Delegate

#define MENU_ENTREE_NAME_KEY @"name"
#define MENU_ENTREE_PROC_MANUAL_KEY @"procedureManual"
#define MENU_ENTREE_CTRL_NUMBER_KEY @"controlNumber"
#define MENU_ENTREE_USED_IN_LOCS_KEY @"usedInLocations"
#define MENU_ENTREE_CONTAIN_INGREDIENTS_KEY @"containIngredients"

#define MENU_ENTREE_PREP_PROC_KEY @"preparationProcedure"
#define MENU_ENTREE_ORDR_PROC_KEY @"orderingProcedure"
#define MENU_ENTREE_PRES_KEY @"presentation"
#define MENU_ENTREE_COMMENTS_KEY @"comments"

-(void) saveButtonIsClicked:(id)mainObj withObject:(id)newEntreeWithLocationAndIngredientsOnly
{
    if([mainObj isKindOfClass:[MenuEntree class]]){
 
        self.managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
        
        MenuEntree *forPersistence = [NSEntityDescription insertNewObjectForEntityForName:@"MenuEntree"
                                                                    inManagedObjectContext:self.managedObjectContext];
        MenuEntree *entree = (MenuEntree *) mainObj;
        MenuEntree *withLocationAndIngredients = nil;
        
        if(newEntreeWithLocationAndIngredientsOnly != nil){
            withLocationAndIngredients = (MenuEntree *)newEntreeWithLocationAndIngredientsOnly;
        }
        
        
        
        [forPersistence setValue:[entree name ] forKey:MENU_ENTREE_NAME_KEY];
        [forPersistence setValue:[entree preparationProcedure] forKey:MENU_ENTREE_PREP_PROC_KEY];
        [forPersistence setValue:[entree orderingProcedure] forKey:MENU_ENTREE_ORDR_PROC_KEY];
        [forPersistence setValue:[entree presentation] forKey:MENU_ENTREE_PRES_KEY];
        [forPersistence setValue:[entree comments] forKey:MENU_ENTREE_COMMENTS_KEY];
        [forPersistence setValue:[entree controlNumber] forKey:MENU_ENTREE_CTRL_NUMBER_KEY];
        
        
        if(withLocationAndIngredients){
            if([withLocationAndIngredients valueForKey:@"usedInLocations"] != nil){
                NSMutableOrderedSet *setLocations = [NSMutableOrderedSet orderedSetWithCapacity:[entree.usedInLocations count]];
                
                for(NSManagedObject * obj in [withLocationAndIngredients usedInLocations] ){
                    
                    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
                    
                    
                    [request setPredicate:[NSPredicate predicateWithFormat:@"SELF = %@", obj]];
                    
                    NSError *error = nil;
                    NSArray *fromFetch = [self.managedObjectContext executeFetchRequest:request error:&error];
                    
                    
                    if(error){
                        DDLogError(@"%s = The error : %@",__PRETTY_FUNCTION__, error);
                    } else {
                    
                        DDLogVerbose(@"%s : The location %@",__PRETTY_FUNCTION__, obj);

                        Location *newLocation = [fromFetch objectAtIndex:0];

                        [newLocation setOfferEntrees:[NSOrderedSet orderedSetWithArray:@[forPersistence]]];
                        
                        [setLocations addObject:newLocation];
                    }
                }
                
                [forPersistence setUsedInLocations:[NSOrderedSet orderedSetWithOrderedSet:setLocations]];
            }
            
            if([withLocationAndIngredients containIngredients] != nil){
                NSMutableOrderedSet *setIngredients = [NSMutableOrderedSet orderedSetWithCapacity:[entree.containIngredients count]];
                for(NSManagedObject *obj in [withLocationAndIngredients containIngredients]){
                                    
                    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
                    
                    [request setPredicate:[NSPredicate predicateWithFormat:@"SELF = %@", obj]];
                    NSError *error = nil;
                    NSArray *fromFetch = [self.managedObjectContext executeFetchRequest:request error:&error];

                    if(error){
                        DDLogError(@"%s = The error : %@", __PRETTY_FUNCTION__, error);
                    } else {
                        
                        DDLogVerbose(@"%s : The ingredient %@", __PRETTY_FUNCTION__, obj);
                        
                        Item *newIngredient = [fromFetch objectAtIndex:0];  

                        [newIngredient setUsedInMenuEntree:[NSOrderedSet orderedSetWithArray:@[forPersistence]]];
                        
                        [setIngredients addObject:newIngredient];

                    }

                }
                [forPersistence setContainIngredients:[NSOrderedSet orderedSetWithOrderedSet:setIngredients]];
            }
        }
    
        NSMutableOrderedSet *referencePhotoData = nil;
        
        if(self.imagesLoaded){
            referencePhotoData =  [NSMutableOrderedSet orderedSetWithCapacity:[self.imagesLoaded count]];

            for(NSObject *object in self.imagesLoaded){
                
                if([object isKindOfClass:[UIImage class]]){
                    
                    UIImage *image = (UIImage *)object;
                    
                    ReferencePhotoData *photo = [NSEntityDescription insertNewObjectForEntityForName:@"ReferencePhotoData"
                                                                              inManagedObjectContext:self.managedObjectContext];
                    
                    NSData *imageData = UIImagePNGRepresentation(image);
                    NSData *bannerData = UIImageJPEGRepresentation(image, 1.0f);
                    NSString *photoDigest = [PVGlobals generateHashForImageData:imageData];
                    
                    [photo setPhotoData:imageData];
                    [photo setBannerPhotoData:bannerData];
                    [photo setPhotoDigest:photoDigest];
                                        
                    [referencePhotoData addObject:photo];
                }
            }
        }
        
        if(referencePhotoData)
            [forPersistence setContainPhotos:referencePhotoData];
        
        // perform saving here..
        [self.managedObjectContext performBlockAndWait:^{

            NSError *error = nil;
            BOOL saved = [self.managedObjectContext save:&error];
            
            if (!saved) {
                // do some real error handling
                DDLogWarn(@"Could not save Date due to %@", error);
            }
            
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
            //[[SDSyncEngine sharedEngine] startSync];
            //[self loadRecordsFromCoreData];
                        
            [self.currentData addObject:forPersistence];
            
            NSUInteger index = [self.currentData indexOfObject:forPersistence];
            
            [self.gridView insertObjectAtIndex:index                             withAnimation:GMGridViewItemAnimationFade | GMGridViewItemAnimationScroll];

            // JOHN: clear this after every transaction.
            [self setImagesLoaded:[NSMutableArray new]];

        }];
        
        [HUDController showCustomView:self clipTo:self withLabel:@"Menu entree saved.." delayFor:1];
    }
    
    [self dismiss];
}

#pragma mark - Menu Entree form datasource

- (NSManagedObjectContext *) useManagedObjectContext
{
    return [self managedObjectContext];
}

@end
