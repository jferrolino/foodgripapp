//
//  PVSimpleHUDController.m
//  FoodGrip
//
//  Created by ENG002 on 1/16/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVSimpleHUDController.h"

@interface PVSimpleHUDController(){
    MBProgressHUD *HUD;
}

@end

@implementation PVSimpleHUDController

- (void) showCustomView:(id)sender clipTo:(UIViewController *)base withLabel:(NSString *)label delayFor:(NSInteger)delay
{
        HUD = [[MBProgressHUD alloc] initWithView:base.view];
        [base.view addSubview:HUD];
        
        // The sample image is based on the work by http://www.pixelpressicons.com, http://creativecommons.org/licenses/by/2.5/ca/
        // Make the customViews 37 by 37 pixels for best results (those are the bounds of the build-in progress indicators)
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
        
        // Set custom view mode
        HUD.mode = MBProgressHUDModeCustomView;
        
        HUD.delegate = sender;
        HUD.labelText = label;
        
        [HUD show:YES];
        [HUD hide:YES afterDelay:delay];
}

- (void)showWithLabelMixed:(id)sender clipTo:(UIViewController *)base withLabel:(NSString *)label
{
	HUD = [[MBProgressHUD alloc] initWithView:base.view];
	[base.view addSubview:HUD];
	
	HUD.delegate = sender;
	HUD.labelText = label;
	HUD.minSize = CGSizeMake(135.f, 135.f);
	
	//[HUD showWhileExecuting:@selector(myMixedTask) onTarget:self withObject:nil animated:YES];
}



///////////////////////////////////////////////////
#pragma mark - Delegate methods
///////////////////////////////////////////////////
- (void)hudWasHidden:(MBProgressHUD *)hud {
	// Remove HUD from screen when the HUD was hidded
	[HUD removeFromSuperview];
	//[HUD release];
	HUD = nil;
}

@end
