//
//  PVTouchAwareParallaxPicturesVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/10/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "GKLParallaxPicturesViewController.h"

@interface PVTouchAwareParallaxPicturesVC : GKLParallaxPicturesViewController
{
    CGFloat tx; // x translation
    CGFloat ty; // y translation
    CGFloat scale;  // zoom scala
    CGFloat theta;  // rotation angle
}

@property (nonatomic, strong) UINavigationController *navigationController;

- (id)initWithImages:(NSArray *)images andContentViewController:(UIViewController *)viewController
withNavigationController:(UINavigationController *)navigationController;

- (void)setImages:(NSArray *)newImages;

@end
