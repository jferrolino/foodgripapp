//
//  PVCyclesTVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/7/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "Cycle.h"

@interface PVCyclesTVC : UITableViewController

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableArray *cycles;
@property (nonatomic, strong) NSString *entityName;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addCycleButton;

- (IBAction)revealMenu:(id)sender;
- (IBAction)addCycle:(id)sender;

@end
