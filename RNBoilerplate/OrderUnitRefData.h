//
//  OrderUnitRefData.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ReferenceData.h"
#import "PVOrderUnitRefDataInterface.h"

@interface OrderUnitRefData : ReferenceData <PVOrderUnitRefDataInterface>

@end

@interface OrderUnitRefData (CoreDataGeneratedAccessors)

- (void)addEquivToObject:(NSManagedObject *)value;
- (void)removeEquivToObject:(NSManagedObject *)value;
- (void)addEquivTo:(NSSet *)values;
- (void)removeEquivTo:(NSSet *)values;

@end
