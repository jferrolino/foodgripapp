//
//  PVLocationCoverflowVC.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVLocationCoverflowVC.h"
#import "iCarousel.h"
#import "Location.h"

@interface PVLocationCoverflowVC ()<iCarouselDataSource, iCarouselDelegate>

@end

@implementation PVLocationCoverflowVC


- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    DDLogVerbose(@"The items size : %i", [self.items count]);
    return [self.items count];
}

- (void) viewDidLoad
{
    //[super viewDidLoad];
    
    self.carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0,0,self.view.bounds.size.width, self.view.bounds.size.height)];
    self.carousel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.carousel.type = iCarouselTypeWheel;
	self.carousel.dataSource = self;
    self.carousel.contentOffset = CGSizeMake(0.0f, 150.0f);
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self.carousel action:@selector(didPan:)];
    panGesture.delegate = (id <UIGestureRecognizerDelegate>)self;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self.carousel action:@selector(didTap:)];
    tapGesture.delegate = (id <UIGestureRecognizerDelegate>)self;
    
     self.panGestureRecognizer = panGesture;
    self.tapGestureRecognizer = tapGesture;
    
    [self.view addSubview:self.carousel];
}



- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 500.0f, 500.0f)];
        
        NSManagedObject *managed = [self.items objectAtIndex:index];
        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        if([managed isKindOfClass:[Location class]]){
            //Location *location = [self.items objectAtIndex:index];
            
            /*if([location containPhotos] != nil && [[location containPhotos] count] > 0){
                NSData *imageData = [location containPhotos][0];
                if(imageData){
                    ((UIImageView *)view).image = [UIImage imageWithData:imageData];
                }
            }*/
            
            
        }
        
        view.contentMode = UIViewContentModeCenter;
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [NSString stringWithFormat:@"%i",index + 1];
    return view;
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    DDLogVerbose(@"%s = > index %i", __PRETTY_FUNCTION__, index);
}

//- (void)car

@end
