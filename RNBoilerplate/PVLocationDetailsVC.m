//
//  PVLocationDetailsVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVLocationDetailsVC.h"
#import "PVDetailsInputView.h"


@implementation PVLocationDetailsVC

@synthesize depthViewReference = _depthViewReference;
@synthesize topView = _topView;
@synthesize parallaxViewController = _parallaxViewController;

- (void)setDepthViewReference:(JFDepthView *)depthViewReference
{
    DDLogWarn(@"Im here..");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Override point for customization after application launch.
    PVDetailsInputView *testContentView = [[[UINib nibWithNibName:@"PVDetailsInputView" bundle:nil] instantiateWithOwner:nil options:nil] objectAtIndex:0];
    
    
    GKLParallaxPicturesViewController *paralaxViewController = [[GKLParallaxPicturesViewController alloc] initWithImages:[NSArray arrayWithObjects:@"http://media-cdn.tripadvisor.com/media/photo-s/00/14/0f/98/pension-bacolod-restaurant.jpg",nil] andContentView:testContentView];
    
    
    self.parallaxViewController = paralaxViewController;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeView:(id)sender {
    [self.depthViewReference dismissPresentedViewInView:self.presentedInView animated:YES];
}

@end
