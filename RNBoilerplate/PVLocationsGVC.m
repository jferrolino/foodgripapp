//
//  PVLocationsGVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVLocationsGVC.h"
#import "ECSlidingViewController.h"
#import "GMGridView.h"
#import "JFDepthView.h"
#import "PVLocationDetailsVC.h"
#import "GKLParallaxPicturesViewController.h"
#import "PVDetailsInputView.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "PVLocationFormTVC.h"
#import "UIView+Glow.h"
#import "PVTouchAwareParallaxPicturesVC.h"
#import "PVCameraViewController.h"


#import "GMGridViewLayoutStrategies.h"
#import "PVSimpleHUDController.h"
#import "PVLocationCoverflowVC.h"

#import "ReferencePhotoData.h"

#define NUMBER_ITEMS_ON_LOAD 10
#define TABLE_HEADER_VIEW_HEIGHT 310.0f

@interface PVLocationsGVC() <GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewTransformationDelegate, GMGridViewActionDelegate,UIActionSheetDelegate, JFDepthViewDelegate, LocationFormDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate, CameraDelegate, parallaxDelegate>{
    
    JFDepthView *depthView;
    PVSimpleHUDController *HUDController;
}

@property (nonatomic, weak) GMGridView *gridView;
@property (nonatomic, strong) JFDepthView *depthView;
@property (nonatomic, strong) PVLocationDetailsVC *locDetailsVC;
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic) NSInteger lastDeleteItemIndexAsked;

@property (nonatomic, strong) UIPopoverController *popupController;
@property (nonatomic,strong) PVCameraViewController *camController;

@property (nonatomic, strong) PVTouchAwareParallaxPicturesVC *parallaxViewController;

// JOHN: future proofing this feature (supporting multi image upload)
@property (nonatomic, strong) NSMutableArray *imagesLoaded;

@property (nonatomic, strong) UIBarButtonItem *addButton;
@property (nonatomic, strong) UIBarButtonItem *pictureButton;

- (void)addMoreItem;
- (void)removeItem;
- (void)refreshItem;
- (void)presentInfo;
- (void)presentOptions:(UIBarButtonItem *)barButton;
- (void)optionsDoneAction;
- (void)dataSetChange:(UISegmentedControl *)control;

@end

@implementation PVLocationsGVC

@synthesize managedObjectContext = _managedObjectContext;
@synthesize entityName = _entityName;

@synthesize currentData = _currentData;
@synthesize gridView = _gridView;
@synthesize depthView = _depthView;
@synthesize locDetailsVC = _locDetailsVC;
@synthesize navController = _navController;
@synthesize addButton;
@synthesize pictureButton;

@synthesize imagesLoaded = _imagesLoaded;
@synthesize lastDeleteItemIndexAsked = _lastDeleteItemIndexAsked;
@synthesize parallaxViewController = _parallaxViewController;

- (id)init
{
    self = [super init];
    if(self){
        
        // Add button
        self.addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                                       target:self
                                                                       action:@selector(addMoreItem)];
        
        self.navigationItem.rightBarButtonItem = self.addButton;
        
        [self loadRecordsFromCoreData];
    }
    return self;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        self = [self init];
    }
    return self;
}
- (NSString *)entityName
{
    if(_entityName == nil){
        _entityName = @"Location";
    }
    return _entityName;
}

- (NSManagedObjectContext *)managedObjectContext
{
    if(_managedObjectContext ==nil){
        _managedObjectContext = [[SDCoreDataController sharedInstance] newManagedObjectContext];
    }
    return _managedObjectContext;
}


- (void)loadRecordsFromCoreData {
    
    [self.managedObjectContext performBlockAndWait:^{
        [self.managedObjectContext reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:self.entityName];
        
        [request setSortDescriptors:[NSArray arrayWithObject:
                                     [NSSortDescriptor sortDescriptorWithKey:@"address" ascending:YES]]];
        
        [request setPredicate:[NSPredicate predicateWithFormat:@"syncStatus != %d", SDObjectDeleted]];
        
        self.currentData = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];

    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *menuIdentifier = [NSString stringWithFormat:@"%@%@",MENU_ITEM_THE_MENU_KEY, SUFFIX_FOR_MENU_ITEMS];
    self.slidingViewController.underLeftViewController  = [self.storyboard instantiateViewControllerWithIdentifier:menuIdentifier];
    [self.slidingViewController setAnchorRightRevealAmount:280.0f];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"SDSyncEngineSyncCompleted" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self loadRecordsFromCoreData];
        [self.gridView reloadData];
    }];
    [[SDSyncEngine sharedEngine] addObserver:self forKeyPath:@"syncInProgress" options:NSKeyValueObservingOptionNew context:nil];
}

#define DIVIDE_BY 3

- (void) viewDidLoad
{
    [super viewDidLoad];
    [self setTitle:MENU_ITEM_RESTAURANT_LOCATION];

    self.gridView.mainSuperView = [self.navigationController view];
    HUDController = [[PVSimpleHUDController alloc]init];
    
    [self.navigationItem.leftBarButtonItem setCustomView:[PVGlobals menuBarButtonThemeFromTarget:self withAction:@selector(revealMenu:)]];
    
    self.locDetailsVC = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"LocDetails"];
    
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    
    self.depthView = [[JFDepthView alloc] initWithGestureRecognizer:tapRec];
    [self.depthView setDelegate:self];
}

- (IBAction)revealMenu:(id)sender
{
    // Add logic here..
    [self.slidingViewController anchorTopViewTo:ECRight];
}

//////////////////////////////////////////////////////////////
#pragma mark controller events
//////////////////////////////////////////////////////////////

- (void)loadView
{
    [super loadView];
    self.view.backgroundColor = [PVGlobals commonViewBackgroundColor];
    
    NSInteger spacing = INTERFACE_IS_PHONE ? 10 : 15;
    
    CGRect frame = CGRectMake(0, 0, self->_contentView.bounds.size.width, self->_contentView.bounds.size.height);
    GMGridView *gmGridView = [[GMGridView alloc] initWithFrame:frame];
    gmGridView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    gmGridView.backgroundColor = [UIColor clearColor];
    
    _gridView = gmGridView;
    
    _gridView.style = GMGridViewStylePush;
    _gridView.itemSpacing = spacing;
    _gridView.minEdgeInsets = UIEdgeInsetsMake(spacing, spacing, spacing, spacing);
    _gridView.centerGrid = YES;
    _gridView.actionDelegate = self;
    _gridView.sortingDelegate = self;
    _gridView.transformDelegate = self;
    _gridView.dataSource = self;
    _gridView.enableEditOnLongPress = YES;
    _gridView.disableEditOnEmptySpaceTap= YES;
    _gridView.layoutStrategy = [GMGridViewLayoutStrategyFactory strategyFromType:GMGridViewLayoutHorizontalPagedLTR];
    
    _gridView.backgroundColor = [PVGlobals commonViewBackgroundColor];
    
    
    _imageScroller  = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _imageScroller.backgroundColor                  = [UIColor clearColor];
    _imageScroller.showsHorizontalScrollIndicator   = NO;
    _imageScroller.showsVerticalScrollIndicator     = NO;
    _imageScroller.pagingEnabled                    = YES;
    
    NSArray *images = [self prepareLocationBanner];
    _imageViews = [NSMutableArray arrayWithCapacity:[images count]];
    
    [self addImages:images];
    
    _transparentScroller = [[UIScrollView alloc] initWithFrame:CGRectZero];
    _transparentScroller.backgroundColor                = [UIColor clearColor];
    _transparentScroller.delegate                       = self;
    _transparentScroller.bounces                        = NO;
    _transparentScroller.pagingEnabled                  = YES;
    _transparentScroller.showsVerticalScrollIndicator   = YES;
    _transparentScroller.showsHorizontalScrollIndicator = NO;
    
    _contentScrollView = [[UIScrollView alloc] init];
    _contentScrollView.backgroundColor              = [UIColor clearColor];
    _contentScrollView.delegate                     = self;
    _contentScrollView.showsVerticalScrollIndicator = NO;
    
    
    _pageControl = [[UIPageControl alloc] init];
    _pageControl.currentPage = 0;
    [_pageControl setHidesForSinglePage:YES];
    
    [_contentScrollView addSubview:_gridView];
    [_contentScrollView addSubview:_pageControl];
    [_contentScrollView addSubview:_transparentScroller];
    
    _contentView = _gridView;
    [self.view addSubview:_imageScroller];
    [self.view addSubview:_contentScrollView];
 
}

- (NSArray *) prepareLocationBanner
{
    
    NSMutableArray *images = [[NSMutableArray alloc] initWithCapacity:[self.currentData count]];
        
    for( NSManagedObject *obj in self.currentData){
        
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        UIGraphicsBeginImageContext(view.frame.size);
        
        if( obj && [obj isKindOfClass:[Location class]]){
            Location *location = (Location *)obj;
            ReferencePhotoData *photoData = nil;
                        
            for(ReferencePhotoData *photo in [location containPhotos]){
                if(photo){
                    photoData = photo;
                }
            }
            
            if(photoData){
                UIImage *locationPhoto = [UIImage imageWithData:[photoData bannerPhotoData]];
                
                if(locationPhoto){
                    [locationPhoto drawInRect:view.bounds];
                }
            }
        }
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        [images addObject:image];
    
    }

    return (images != nil && [images count] > 0) ? [images copy] : nil;
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewDataSource
//////////////////////////////////////////////////////////////

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [self.currentData count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(170, 135);
        }
        else
        {
            return CGSizeMake(140, 110);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(285, 205);
        }
        else
        {
            return CGSizeMake(230, 175);
        }
    }
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    GMGridViewCell *cell = [gridView dequeueReusableCell];
    
    UIImage *image = nil;

    Location *location = [self.currentData objectAtIndex:index];
    NSString * name = [location name];
    
    cell = [[GMGridViewCell alloc] init];
    cell.deleteButtonIcon = [UIImage imageNamed:@"close_x.png"];
    cell.deleteButtonOffset = CGPointMake(-15, -15);
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    
        UIGraphicsBeginImageContext(view.frame.size);
        ReferencePhotoData *photoData = nil;
        
        if(location.containPhotos != nil && [location.containPhotos count] > 0){
            for(ReferencePhotoData *first in location.containPhotos){
                if(first){
                    photoData = first;
                    break;
                }
            }
        }
        
        if(photoData){
            UIImage *locAvatar = [UIImage imageWithData:[photoData photoData]];
            
            if(locAvatar){
                [locAvatar drawInRect:view.bounds];
            }
        }
        
        image = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    
    if(image)
        view.backgroundColor = [UIColor colorWithPatternImage:image];
    view.layer.masksToBounds = NO;
    view.layer.cornerRadius = 8;
    
    cell.contentView = view;

    [[cell.contentView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(name) [cell.contentView addSubview:[PVGlobals generateGridContentView:cell.contentView.bounds withLabels:@[name]]];
    
    return cell;
}

- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return YES;
}


//////////////////////////////////////////////////////////////
#pragma mark GMGridViewActionDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position
{
    DDLogWarn(@"Did tap at index %d", position);
    
    // Pass in the view controller you want to present (self.topViewController)
    // and the view you want it to be displayed within (self.view)
    [self.depthView presentViewController:self.locDetailsVC inView:self.view animated:YES];
    
    // Optionally, if you don't care about rotation support, you can just pass in
    // the view you want to present (self.topViewController.view)
    // and the view you want it to be displayed within (self.view)
    //[self.depthView presentView:self.navController.view inView:self.view];
}


- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    DDLogWarn(@"Tap on empty space");
}

- (void)GMGridView:(GMGridView *)gridView processDeleteActionForItemAtIndex:(NSInteger)index
{
    [[[UIAlertView alloc] initWithTitle:@"Confirm"
                                message:@"Are you sure you want to delete this item?"
                               delegate:self
                      cancelButtonTitle:@"Cancel"
                      otherButtonTitles:@"Delete", nil] show];
    
   _lastDeleteItemIndexAsked = index;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // invoked
    if (buttonIndex == 1)
    {
        NSManagedObject *itemToRemove = [self.currentData objectAtIndex:_lastDeleteItemIndexAsked];
        [self.managedObjectContext performBlockAndWait:^{
            if ([[itemToRemove valueForKey:@"objectId"] isEqualToString:@""] || [itemToRemove valueForKey:@"objectId"] == nil) {
                [self.managedObjectContext deleteObject:itemToRemove];
            } else {
                [itemToRemove setValue:[NSNumber numberWithInt:SDObjectDeleted] forKey:@"syncStatus"];
            }
            NSError *error = nil;
            BOOL saved = [self.managedObjectContext save:&error];
            if (!saved) {
                DDLogError(@"Error saving main context: %@", error);
            }
            
            
            [[SDCoreDataController sharedInstance] saveMasterContext];
            //[[SDSyncEngine sharedEngine] startSync];
            [self loadRecordsFromCoreData];
            
            [_gridView removeObjectAtIndex:_lastDeleteItemIndexAsked withAnimation:GMGridViewItemAnimationFade];
        }];
    }
}

//////////////////////////////////////////////////////////////
#pragma mark GMGridViewSortingDelegate
//////////////////////////////////////////////////////////////

- (void)GMGridView:(GMGridView *)gridView didStartMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor orangeColor];
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil
     ];
}

- (void)GMGridView:(GMGridView *)gridView didEndMovingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor redColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil
     ];
}

- (BOOL)GMGridView:(GMGridView *)gridView shouldAllowShakingBehaviorWhenMovingCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    return YES;
}

- (void)GMGridView:(GMGridView *)gridView moveItemAtIndex:(NSInteger)oldIndex toIndex:(NSInteger)newIndex
{
    NSObject *object = [self.currentData objectAtIndex:oldIndex];
    [self.currentData removeObject:object];
    [self.currentData insertObject:object atIndex:newIndex];
}

- (void)GMGridView:(GMGridView *)gridView exchangeItemAtIndex:(NSInteger)index1 withItemAtIndex:(NSInteger)index2
{
    [self.currentData exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
}


//////////////////////////////////////////////////////////////
#pragma mark DraggableGridViewTransformingDelegate
//////////////////////////////////////////////////////////////

- (CGSize)GMGridView:(GMGridView *)gridView sizeInFullSizeForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index inInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    if (INTERFACE_IS_PHONE)
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(320, 210);
        }
        else
        {
            return CGSizeMake(300, 310);
        }
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape(orientation))
        {
            return CGSizeMake(700, 530);
        }
        else
        {
            return CGSizeMake(600, 500);
        }
    }
}

- (UIView *)GMGridView:(GMGridView *)gridView fullSizeViewForCell:(GMGridViewCell *)cell atIndex:(NSInteger)index
{
    UIView *fullView = [[UIView alloc] init];
    fullView.backgroundColor = [UIColor yellowColor];
    fullView.layer.masksToBounds = NO;
    fullView.layer.cornerRadius = 8;
    
    CGSize size = [self GMGridView:_gridView sizeInFullSizeForCell:cell atIndex:index inInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    fullView.bounds = CGRectMake(0, 0, size.width, size.height);
    
    UILabel *label = [[UILabel alloc] initWithFrame:fullView.bounds];
    label.text = [NSString stringWithFormat:@"Fullscreen View for cell at index %d", index];
    //label.textAlignment = UITextAlignmentCenter;
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (INTERFACE_IS_PHONE)
    {
        label.font = [UIFont boldSystemFontOfSize:15];
    }
    else
    {
        label.font = [UIFont boldSystemFontOfSize:20];
    }
    
    [fullView addSubview:label];
    
    
    return fullView;
}

- (void)GMGridView:(GMGridView *)gridView didStartTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor blueColor];
                         cell.contentView.layer.shadowOpacity = 0.7;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEndTransformingCell:(GMGridViewCell *)cell
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         cell.contentView.backgroundColor = [UIColor redColor];
                         cell.contentView.layer.shadowOpacity = 0;
                     }
                     completion:nil];
}

- (void)GMGridView:(GMGridView *)gridView didEnterFullSizeForCell:(UIView *)cell
{
    
}
//////////////////////////////////////////////////////////////
#pragma mark private methods
//////////////////////////////////////////////////////////////

#define TITLE_CREATE_NEW_LOCATION @"Create a location"

- (void)addMoreItem
{
    // Example: adding object at the last position
    if([self.depthView isPresenting]){
        [self.depthView dismissPresentedViewInView:self.view animated:YES];
        self.parallaxViewController = nil;
    } else {
        
        self.pictureButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                            target:self
                                                                            action:@selector(takePicture:)];
        
    
        
        PVLocationFormTVC *locationFormController =  [self.storyboard instantiateViewControllerWithIdentifier:@"LocationForm"];
        locationFormController.view.frame = self.depthView.mainView.bounds;
        locationFormController.view.backgroundColor = [PVGlobals commonViewBackgroundColor];
        locationFormController.delegate = self;
        
        
        self.parallaxViewController = [[PVTouchAwareParallaxPicturesVC alloc] initWithImages:@[[UIImage imageNamed:@"restaurant-facade.jpg"]] andContentView:locationFormController.view];
        
        [self.parallaxViewController setParallaxDelegate:self];
        [self.parallaxViewController setTitle:TITLE_CREATE_NEW_LOCATION];
        [self.parallaxViewController.navigationItem setRightBarButtonItem:pictureButton];
        
        UINavigationController *navLocationForm = [[UINavigationController alloc]initWithRootViewController:self.parallaxViewController];
        [navLocationForm.view setBackgroundColor:[PVGlobals commonViewBackgroundColor]];
        
        [self.depthView setDelegate:self];
        [self.depthView presentViewController:navLocationForm inView:self.view animated:YES];
    }
}

- (void)removeItem
{
    // Example: removing last item
    if ([self.currentData count] > 0)
    {
        NSInteger index = [self.currentData count] - 1;
        
        [_gridView removeObjectAtIndex:index withAnimation:GMGridViewItemAnimationFade | GMGridViewItemAnimationScroll];
        [self.currentData removeObjectAtIndex:index];
    }
}

- (void)refreshItem
{
    // Example: reloading last item
    if ([self.currentData count] > 0)
    {
        int index = [self.currentData count] - 1;
        
        NSString *newMessage = [NSString stringWithFormat:@"%d", (arc4random() % 1000)];
        
        [self.currentData replaceObjectAtIndex:index withObject:newMessage];
        [_gridView reloadObjectAtIndex:index withAnimation:GMGridViewItemAnimationFade | GMGridViewItemAnimationScroll];
    }
}

- (void)presentInfo
{
    NSString *info = @"Long-press an item and its color will change; letting you know that you can now move it around. \n\nUsing two fingers, pinch/drag/rotate an item; zoom it enough and you will enter the fullsize mode";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:info
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    
    [alertView show];
}

- (void)dataSetChange:(UISegmentedControl *)control
{
    [_gridView reloadData];
}

- (void)presentOptions:(UIBarButtonItem *)barButton
{
    if (INTERFACE_IS_PHONE)
    {
        //[self presentModalViewController:_optionsNav animated:YES];
    }
    else
    {
        /*if(![_optionsPopOver isPopoverVisible])
        {
            if (!_optionsPopOver)
            {
                _optionsPopOver = [[UIPopoverController alloc] initWithContentViewController:_optionsNav];
            }
            
            [_optionsPopOver presentPopoverFromBarButtonItem:barButton permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
        }
        else
        {*/
            [self optionsDoneAction];
       // }
    }
}

- (void)optionsDoneAction
{
    if (INTERFACE_IS_PHONE)
    {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    else
    {
        DDLogWarn(@"No implementation yet");
    }
}


///////////////////////////////////////////
#pragma mark - Depth View methods
///////////////////////////////////////////
// Here is the simple dismissal method called from the tap recognizer passed into init method of JFDepthView
- (void)dismiss {
    [self.depthView dismissPresentedViewInView:self.view animated:YES];
}



///////////////////////////////////////////
#pragma mark - Location Form delegate method implementation
///////////////////////////////////////////
- (void) saveButtonIsClicked:(id)sender withObject:(id)location
{
    DDLogVerbose(@"Saving location object with content :%@", location);
    Location *newItem = (Location *)location;
    Location *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:self.managedObjectContext];
    
    [newLocation setName:[newItem name]];
    [newLocation setAddress:[newItem address]];
    [newLocation setPhoneNumber:[newItem phoneNumber]];
    [newLocation setRestaurantManager:[newItem restaurantManager]];
    
    NSMutableOrderedSet *referencePhotoData = nil;
    
    if(self.imagesLoaded){
        
        referencePhotoData = [[NSMutableOrderedSet alloc] init];
        
        for(NSObject *object in self.imagesLoaded){
            
            if([object isKindOfClass:[UIImage class]]){
                
                UIImage *image = (UIImage *)object;
            
                ReferencePhotoData *photo = [NSEntityDescription insertNewObjectForEntityForName:@"ReferencePhotoData" inManagedObjectContext:self.managedObjectContext];
             
                NSData *imageData = UIImagePNGRepresentation(image);
                NSData *forBanner = UIImageJPEGRepresentation(image,1);

                NSString *photoDigest = [PVGlobals generateHashForImageData:imageData];
                
                [photo setPhotoData:imageData];
                [photo setBannerPhotoData:forBanner];
                [photo setPhotoDigest:photoDigest];
                
                // JOHN: maybe adding the bounding box coordinates as well
                /*
                 [photo setLatMax:0.0];
                 [photo setLonMax:0.0];
                 [photo setLatMin:0.0];
                 [photo setLonMin:0.0];
                 */
                
                // JOHN : Maybe we want to add a logic here that searches the
                // database with the same digest for deduplicating images in the db.
                //[referencePhotoData addObject:photo];
                
                [referencePhotoData addObject:photo];
            }
        }
    }
    
    [newLocation setContainPhotos:referencePhotoData];
    [newLocation setValue:[NSNumber numberWithInt:SDObjectCreated] forKey:@"syncStatus"];
     
    // perform saving here..
    [self.managedObjectContext performBlockAndWait:^{
        NSError *error = nil;
        
        BOOL saved = [self.managedObjectContext save:&error];
        if (!saved) {
            // do some real error handling
            DDLogWarn(@"Could not save Date due to %@", error);
        }
        
        [[SDCoreDataController sharedInstance] saveMasterContext];
        //[[SDSyncEngine sharedEngine] startSync]; // let user manually sync it
        
        
        [self.currentData addObject:newLocation];
        
        NSUInteger index = [self.currentData indexOfObject:newLocation];
        
        DDLogVerbose(@"The index of the new location is %i", index);
        
        [_gridView insertObjectAtIndex:index withAnimation:GMGridViewItemAnimationFade | GMGridViewItemAnimationScroll];
        
        // JOHN: clear this after every transaction.
        [self setImagesLoaded:[NSMutableArray new]];
        //[self.contentView setNeedsDisplay];
    }];
    
    [HUDController showCustomView:self clipTo:self withLabel:@"Location saved.." delayFor:1];
    [self dismiss];
}

//
#pragma mark - UIPopupController Delegate method
// =-=-=-=-=-=-=-==-=-==-=-=-=-==-=
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if([self.imagesLoaded count] > 0){
        DDLogVerbose(@"Adding image to parallax view controller");

        [self.parallaxViewController setImages:self.imagesLoaded];
    }
}

#pragma mark - Picture button clicked

- (void) takePicture:(id)sender

{
    if(self.popupController)
        [self.popupController dismissPopoverAnimated:YES];
   
    // Present the popover
    [self.popupController presentPopoverFromBarButtonItem:self.parallaxViewController.navigationItem.rightBarButtonItem permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


//////////////////////////////////////////
#pragma mark - parallax method
/////////////////////////////////////////
- (void) imageTapped:(UIImage *)image atPoint:(CGPoint)point
{
    CGRect rect = CGRectMake(0, 0,10 ,50);
    [self.popupController presentPopoverFromRect:rect inView:self.depthView.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex){
        case 0:
            break;
        case 1:
            break;
        default:
            break;
    }
}
//////////////////////////////////////////////////
#pragma mark -
//////////////////////////////////////////////////
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self checkSyncStatus];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"SDSyncEngineSyncCompleted" object:nil queue:nil usingBlock:^(NSNotification *note) {
        [self loadRecordsFromCoreData];
        [self.gridView setNeedsDisplay];
    }];
    [[SDSyncEngine sharedEngine] addObserver:self forKeyPath:@"syncInProgress" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"SDSyncEngineSyncCompleted" object:nil];
    [[SDSyncEngine sharedEngine] removeObserver:self forKeyPath:@"syncInProgress"];
}

- (void)checkSyncStatus {
    if ([[SDSyncEngine sharedEngine] syncInProgress]) {
        [self replaceRefreshButtonWithActivityIndicator];
    } else {
        [self removeActivityIndicatorFromRefreshButon];
    }
}

////////////////////////////////////////
#pragma mark - Activity Indicator
////////////////////////////////////////
- (void)replaceRefreshButtonWithActivityIndicator {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [activityIndicator setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin)];
    [activityIndicator startAnimating];
    UIBarButtonItem *activityItem = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:[self.navigationItem leftBarButtonItem], activityItem,nil]];
}

- (void)removeActivityIndicatorFromRefreshButon {
    UIBarButtonItem *menuButton = [self.navigationItem.leftBarButtonItems objectAtIndex:0];
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:menuButton,nil]];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"syncInProgress"]) {
        [self checkSyncStatus];
    }
}

#pragma mark - JF Depth View delegate methods

- (void)willPresentDepthView:(JFDepthView*)depthView
{
    [self.addButton setEnabled:NO];
    
    PVCameraViewController *cameraViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CameraController"];
    [cameraViewController setCameraDelegate:self];

    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:cameraViewController];
    self.popupController = [[UIPopoverController alloc] initWithContentViewController:navController];

    [self.popupController setPopoverContentSize:CGSizeMake(150 * 3, 100 * 3) animated:YES];
    [self.popupController setDelegate:self];
}

- (void)didDismissDepthView:(JFDepthView*)depthView
{
    [self.addButton setEnabled:YES];
    [self setImagesLoaded:nil];
}

#pragma mark - Delegate method of PVCameraViewController

- (void) uploadedImage:(UIImage *)image
{
    DDLogVerbose(@"The image is %@", image);
}

- (void) uploadedImages:(NSArray *)images
{
    DDLogVerbose(@"The images are %@", images);
    
    if(self.imagesLoaded == nil){
        self.imagesLoaded = [NSMutableArray array];
    }
    
    NSArray *temp = [self.imagesLoaded copy];
    temp = [temp arrayByAddingObjectsFromArray:images];
    
    self.imagesLoaded = [[PVGlobals deduplicateImages:temp] mutableCopy];//[[self deduplicateImages:temp] mutableCopy];
}

@end
