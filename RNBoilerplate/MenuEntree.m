//
//  MenuEntree.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "MenuEntree.h"
#import "Item.h"
#import "Location.h"
#import "ReferencePhotoData.h"


@implementation MenuEntree

@dynamic controlNumber;
@dynamic name;
@dynamic sellingPrice;
@dynamic objectId;
@dynamic syncStatus;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic preparationProcedure;
@dynamic cookingProcedure;
@dynamic orderingProcedure;
@dynamic presentation;
@dynamic comments;
@dynamic containIngredients;
@dynamic containPhotos;
@dynamic usedInLocations;

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
	self = (MenuEntree *)entity;
	if(self){
		if(context){
			[context deleteObject:self];
		}
	}
	return self;
}

@end
