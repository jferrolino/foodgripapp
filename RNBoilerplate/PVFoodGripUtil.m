//
//  PVFoodGripUtil.m
//  FoodGrip
//
//  Created by ENG002 on 1/18/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVFoodGripUtil.h"

@implementation PVFoodGripUtil

+ (void) initItemCategoriesToDefaults:(NSManagedObjectContext *)context
{
    NSManagedObjectContext *moc = context;//[[SDCoreDataController sharedInstance] newManagedObjectContext];
    
    [moc performBlockAndWait:^{
        [moc reset];
        NSError *error = nil;
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Item"];
        
        [request setResultType:NSDictionaryResultType];
        [request setReturnsDistinctResults:YES];
        [request setPropertiesToFetch:@[@"category"]];
        
        NSArray *categories = [moc executeFetchRequest:request error:&error];
        NSMutableArray *holder = [NSMutableArray array];
        for (int i =0 ; i < [categories count]; i++){
            
            NSString *category = [(NSDictionary *)[categories objectAtIndex:i] objectForKey:@"category"];
            [holder addObject:category];
        }
        
        // Set all categories to USEr Defaults.
        if([holder count] > 0){
            [[NSUserDefaults standardUserDefaults] setObject:holder forKey:ITEM_CATEGORIES];
        }
    }];
}


@end
