//
//  PVMultiSelectionItemsTVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/14/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMultiSelectionTVC.h"
#import "Item.h"


@interface PVMultiSelectItemsTVC : PVMultiSelectionTVC <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<MultiSelectionDelegate> delegate;

@end
