//
//  PVCameraViewController.h
//  FoodGrip
//
//  Created by ENG002 on 1/10/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PVUtilities.h"
#import "PVGeometry.h"
#import "PVCameraImageHelper.h"

@protocol CameraDelegate <NSObject>

@optional
#pragma deprecated
- (void) populatedImageView:(UIImageView *)imageView ;

- (void) uploadedImage:(UIImage *)image;
- (void) uploadedImages:(NSArray *) images;

@end

@interface PVCameraViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UIImageView *imageView;
    PVCameraImageHelper *helper;
    
    UIPopoverController *popoverController;
    UIImagePickerController *imagePickerController;
    UISwitch *editSwitch;
}

@property (nonatomic, weak) id<CameraDelegate> cameraDelegate;

// Testing
@property (nonatomic, strong) UICollectionViewController *imageCollectionController;

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *photos;

- (id)  initWithFrame:(CGRect) frame;

@end
