//
//  ReferenceData.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "PVReferenceDataInterface.h"

@interface ReferenceData : NSManagedObject <PVReferenceDataInterface>

/*
@property (nonatomic, retain) NSString * code;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * type;
 */

@end
