//
//  PVLocationDetailsVC.h
//  FoodGrip
//
//  Created by ENG002 on 1/2/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JFDepthView.h"
#import "GKLParallaxPicturesViewController.h"

@interface PVLocationDetailsVC : UIViewController

@property (weak,nonatomic) GKLParallaxPicturesViewController *parallaxViewController;

@property (strong, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) JFDepthView* depthViewReference;
@property (weak, nonatomic) UIView* presentedInView;

- (IBAction)closeView:(id)sender;

@end
