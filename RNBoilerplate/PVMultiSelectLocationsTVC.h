//
//  PVMultiSelectLocationsTVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/14/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMultiSelectionTVC.h"
#import "Location.h"
#import "ReferencePhotoData.h"

@interface PVMultiSelectLocationsTVC : PVMultiSelectionTVC <UITableViewDataSource, UITableViewDelegate>

@end
