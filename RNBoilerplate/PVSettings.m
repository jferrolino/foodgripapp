//
//  PVSettings.m
//  FoodGrip
//
//  Created by ENG002 on 1/15/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVSettings.h"

@implementation PVSettings

@synthesize inventoryCycleType;
@synthesize toShowTotal;
@synthesize contactEmail;
@synthesize language;
@synthesize currency;
@synthesize syncDropbox;

////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *)description {
    /*
    return [NSString stringWithFormat:
            @"name = %@, toShowTotal = %@, contactEmail = %@, language = %@, currency = %@, syncDropbox = %@,
            self.name, [sel], self.releaseDate, self.numberOfActor, self.suitAllAges, self.genre, self.password, self.shortName, self.choice, self.rate];*/
    
    return @"";
}

@end
