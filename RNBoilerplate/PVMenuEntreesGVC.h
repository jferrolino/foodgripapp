//
//  PVMenusGVC.h
//  FoodGrip
//
//  Created by ENG002-JRF on 2/18/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ECSlidingViewController.h"
#import "SDCoreDataController.h"
#import "SDSyncEngine.h"
#import "MenuEntree.h"
#import "GMGridView.h"
#import "GMGridViewLayoutStrategies.h"
#import "GKLParallaxPicturesViewController.h"
#import "JFDepthView.h"
#import "PVMenuEntreeFormTVC.h"
#import "ReferencePhotoData.h"

#import "PVTouchAwareParallaxPicturesVC.h"
#import "PVSimpleHUDController.h"

#import "PVCameraViewController.h"
#import "PVMenuEntreeFormWithParallaxVC.h"

#import "Location.h"
#import "Item.h"

@interface PVMenuEntreesGVC : GKLParallaxPicturesViewController <GMGridViewDataSource, GMGridViewSortingDelegate, GMGridViewTransformationDelegate, GMGridViewActionDelegate,UIActionSheetDelegate, JFDepthViewDelegate, MenuEntreeFormDelegate, MenuEntreeFormDataSource, UIAlertViewDelegate, UIPopoverControllerDelegate, UIImagePickerControllerDelegate,CameraDelegate>

@property (nonatomic,strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSMutableArray *currentData;
@property (nonatomic,strong) NSString *entityName;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;

- (IBAction)revealMenu:(id)sender;


@end
