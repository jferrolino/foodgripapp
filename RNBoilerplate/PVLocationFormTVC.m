//
//  PVLocationFormTVC.m
//  FoodGrip
//
//  Created by ENG002 on 1/4/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVLocationFormTVC.h"
#import "SDCoreDataController.h"
#import "FormKit.h"
#import "SDSyncEngine.h"

@interface PVLocationFormTVC (){
    FKFormMapping *_formMapping;
}

@end

@implementation PVLocationFormTVC

@synthesize formModel;
@synthesize delegate;
@synthesize location = _location;
@synthesize managedObjectContext = _managedObjectContext;


#pragma mark - View lifecycle


////////////////////////////////////////////////////////////////////////////////////////////////////
- (id)init {
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
    }
    return self;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.managedObjectContext = [[SDCoreDataController sharedInstance] backgroundManagedObjectContext];
    
    self.formModel = [FKFormModel formTableModelForTableView:self.tableView
                                        navigationController:self.navigationController];
    
    Location *location = [[Location alloc]initWithNonManagedEntity:[NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:self.managedObjectContext] insertIntoManagedObjectContext:self.managedObjectContext];
    
    self.location = location;
    
    [FKFormMapping mappingForClass:[Location class] block:^(FKFormMapping *formMapping) {
        [formMapping sectionWithTitle:@"Location Info" footer:@"Thank you!" identifier:@"Foodgrip info"];
        
        [formMapping mapAttribute:@"name" title:@"Name" type:FKFormAttributeMappingTypeText];
        [formMapping mapAttribute:@"address" title:@"Address" type:FKFormAttributeMappingTypeText];
        
        
        [formMapping mapAttribute:@"phoneNumber" title:@"Phone Number" type:FKFormAttributeMappingTypeText keyboardType:UIKeyboardTypeNumbersAndPunctuation];
        [formMapping mapAttribute:@"restaurantManager" title:@"Restaurant Manager" type:FKFormAttributeMappingTypeText];
        

        
        [formMapping buttonSave:@"Save" handler:^{
            
            [self.formModel save];
            
            // call delegate only when successful
            [self.delegate saveButtonIsClicked:self.formModel withObject:self.formModel.object];
        }];
        [self.formModel registerMapping:formMapping];
        _formMapping = formMapping;
    }];
    
    [self.formModel setDidChangeValueWithBlock:^(id object, id value, NSString *keyPath) {
        //DDLogWarn(@"Did change model value");
        
        DDLogVerbose(@"The object:%@ ; value:%@ ; keyPath:%@", object, value, keyPath);
    }];
    
    [self.formModel loadFieldsWithObject:self.location];
}

////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidUnload {
    [super viewDidUnload];
    
    self.formModel = nil;
    self.location = nil;
}


////////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}
@end
