//
//  Location.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/19/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Location.h"
#import "MenuEntree.h"
#import "ReferencePhotoData.h"


@implementation Location

@dynamic address;
@dynamic createdAt;
@dynamic latMax;
@dynamic latMin;
@dynamic lonMax;
@dynamic lonMin;
@dynamic name;
@dynamic objectId;
@dynamic phoneNumber;
@dynamic restaurantManager;
@dynamic syncStatus;
@dynamic updatedAt;
@dynamic containPhotos;
@dynamic offerEntrees;

- (id) initWithNonManagedEntity:(NSEntityDescription *)entity insertIntoManagedObjectContext:(NSManagedObjectContext *)context
{
	self = (Location *)entity;
	if(self){
		if(context){
			[context deleteObject:self];
		}
	}
	return self;
}


- (NSDictionary *)JSONToCreateObjectOnServer
{
	NSString *jsonString = nil;
	NSDictionary *jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    self.name, @"name",
                                    self.address, @"address",
                                    self.syncStatus, @"syncStatus", nil];
    
	NSError *error = nil;
    
	NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:jsonDictionary
                        options:NSJSONWritingPrettyPrinted
                        error:&error];
    
	if(!jsonData){
		NSLog(@"Error creating jsonData: %@", error);
        
	} else {
		jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	}
    
	return jsonDictionary;
}

@end
