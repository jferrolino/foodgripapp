//
//  PVDropboxSelectionAlertTableView.m
//  FoodGrip
//
//  Created by ENG002 on 12/18/12.
//  Copyright (c) 2012 Persevera Inc. All rights reserved.
//

#import "PVDropboxSelectionAlertTableView.h"

@implementation PVDropboxSelectionAlertTableView

@synthesize myDelegate;

- (id)initWithCaller:(id)_caller data:(NSArray *)_data title:(NSString *)_title andContext:(id)_context
{
    self = [super initWithCaller:_caller data:_data title:_title andContext:_context];
    if(self){
        [self setMyDelegate:_caller];
    }
    return self;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"ABC"];
    
    if(cell == nil) {
        //cell = [[UITableViewCell alloc]initWithFrame:CGRectZero reuseIdentifier:@"ABC"];
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.textLabel.font = [UIFont boldSystemFontOfSize:14];
    }
    
    // John: This is to accomodate Dropbox DBMetadata object.
    DBMetadata *item = [data objectAtIndex:indexPath.row];
    cell.textLabel.text = [item filename];    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissWithClickedButtonIndex:0 animated:YES];
    
    // from super class
    //[self.caller didSelectRowAtIndex:indexPath.row withContext:self.context];
    
    // own delegate method
    [self.myDelegate didSelectRowWithDBMetadata:[data objectAtIndex:indexPath.row] withContext:self.context];
}

@end
