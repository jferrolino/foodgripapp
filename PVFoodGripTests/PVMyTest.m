//
//  PVMyTest.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//
#import <GHUnitIOS/GHUnit.h>

@interface PVMyTest : GHTestCase { }
@end

@implementation PVMyTest

- (void) testStrings {
    NSString *string1 = @"a string";
    GHTestLog(@"I can log to the GHUnit test console:%@", string1);

    // Assert string1 is not NULL, with no custom error description
    GHAssertNotNULL(@"a string", nil);
    //GHAssertNotNULL(string1, nil);
    //GHAssertNotNULL(string1, nil);
    
    // Assert equal objects, add custom error description
    NSString *string2 = @"a string";
    GHAssertEqualObjects(string1, string2, @"A custom error message. string1 should be equal to: %@.", string2);
}

@end
