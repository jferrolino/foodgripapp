//
//  PVAppDelegate.h
//  PVFoodGripTests
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
