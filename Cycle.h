//
//  Cycle.h
//  FoodGrip
//
//  Created by ENG002 on 1/17/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CyclePerInventoryItem;

@interface Cycle : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * cycleStatus;
@property (nonatomic, retain) NSDate * endedAt;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSDate * startedAt;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) CyclePerInventoryItem *have;

@end
