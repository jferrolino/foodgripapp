//
//  Item.h
//  FoodGrip
//
//  Created by ENG002 on 1/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cycle;

@interface Item : NSManagedObject

@property (nonatomic, retain) NSDecimalNumber * amount;
@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSDecimalNumber * cost;
@property (nonatomic, retain) NSNumber * costPercentage;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * itemNumber;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * numberSold;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSDecimalNumber * priceSold;
@property (nonatomic, retain) NSDecimalNumber * profit;
@property (nonatomic, retain) NSNumber * rank;
@property (nonatomic, retain) NSDecimalNumber * sales;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSSet *in;
@end

@interface Item (CoreDataGeneratedAccessors)

- (void)addInObject:(Cycle *)value;
- (void)removeInObject:(Cycle *)value;
- (void)addIn:(NSSet *)values;
- (void)removeIn:(NSSet *)values;

@end
