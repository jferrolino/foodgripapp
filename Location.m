//
//  Location.m
//  FoodGrip
//
//  Created by ENG002 on 1/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Location.h"
#import "Cycle.h"


@implementation Location

@dynamic address;
@dynamic createdAt;
@dynamic name;
@dynamic objectId;
@dynamic photo;
@dynamic syncStatus;
@dynamic updatedAt;
@dynamic lat;
@dynamic lon;
@dynamic has;

@end
