//
//  Cycle.m
//  FoodGrip
//
//  Created by ENG002 on 1/17/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Cycle.h"
#import "CyclePerInventoryItem.h"


@implementation Cycle

@dynamic createdAt;
@dynamic cycleStatus;
@dynamic endedAt;
@dynamic objectId;
@dynamic startedAt;
@dynamic syncStatus;
@dynamic updatedAt;
@dynamic have;

@end
