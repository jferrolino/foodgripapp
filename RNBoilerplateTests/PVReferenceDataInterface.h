//
//  PVReferenceDataInterface.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PVReferenceDataInterface <NSObject>

@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *type;

@end
