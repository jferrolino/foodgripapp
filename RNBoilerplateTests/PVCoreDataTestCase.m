//
//  PVReferenceDataTestCase.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVCoreDataTestCase.h"
#import "ReferenceData.h"

@implementation PVCoreDataTestCase{
    NSManagedObjectModel *_mom;
    NSPersistentStoreCoordinator *_psc;
    NSManagedObjectContext *_moc;
    NSPersistentStore *_store;
}

@synthesize managedObjectContext = _moc;

-(void) setUp
{
    [super setUp];
    NSArray *bundles = [NSArray arrayWithObject:[NSBundle bundleForClass:[self class]]];
    
    _mom = [NSManagedObjectModel mergedModelFromBundles:bundles];
    _psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_mom];
    
    _store = [_psc addPersistentStoreWithType:NSInMemoryStoreType configuration:nil URL:nil options:nil error:NULL];
    STAssertNotNil(_store,@"Unable to create in-memory store");
    
    _moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_moc setPersistentStoreCoordinator:_psc];
}

- (id<PVReferenceDataInterface> *) retrieveRefDataWithType:(NSString *) type
{
    id<PVReferenceDataInterface> data = nil;
    
    // Get from persistence store
    
    return data;
}



- (void)tearDown {
    [super tearDown];
    _mom = nil; _psc = nil; _moc = nil; _store = nil;
}

@end
