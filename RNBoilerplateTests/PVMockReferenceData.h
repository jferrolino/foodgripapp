//
//  PVMockReferenceData.h
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PVReferenceDataInterface.h"

@interface PVMockReferenceData : NSObject<PVReferenceDataInterface>
{
    NSString *code;
    NSString *desc;
    NSString *type;
}

@property (nonatomic, retain) NSString *code;
@property (nonatomic, retain) NSString *desc;
@property (nonatomic, retain) NSString *type;

@end
