//
//  PVMockReferenceData.m
//  FoodGrip
//
//  Created by ENG002-JRF on 3/1/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "PVMockReferenceData.h"

@implementation PVMockReferenceData

@synthesize code = _code;
@synthesize desc = _desc;
@synthesize type = _type;

@end
