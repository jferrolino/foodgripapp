//
//  CyclePerInventoryItem.m
//  FoodGrip
//
//  Created by ENG002 on 1/22/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "CyclePerInventoryItem.h"
#import "Cycle.h"
#import "Item.h"


@implementation CyclePerInventoryItem

@dynamic purchaseCount;
@dynamic quantityCount;
@dynamic syncStatus;
@dynamic createdAt;
@dynamic updatedAt;
@dynamic by;
@dynamic for_item;

@end
