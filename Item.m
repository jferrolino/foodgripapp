//
//  Item.m
//  FoodGrip
//
//  Created by ENG002 on 1/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import "Item.h"
#import "Cycle.h"


@implementation Item

@dynamic amount;
@dynamic category;
@dynamic cost;
@dynamic costPercentage;
@dynamic createdAt;
@dynamic id;
@dynamic itemNumber;
@dynamic name;
@dynamic numberSold;
@dynamic objectId;
@dynamic priceSold;
@dynamic profit;
@dynamic rank;
@dynamic sales;
@dynamic syncStatus;
@dynamic type;
@dynamic updatedAt;
@dynamic in;

@end
