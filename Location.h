//
//  Location.h
//  FoodGrip
//
//  Created by ENG002 on 1/8/13.
//  Copyright (c) 2013 Persevera Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Cycle;

@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSNumber * syncStatus;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSDecimalNumber * lat;
@property (nonatomic, retain) NSDecimalNumber * lon;
@property (nonatomic, retain) NSSet *has;
@end

@interface Location (CoreDataGeneratedAccessors)

- (void)addHasObject:(Cycle *)value;
- (void)removeHasObject:(Cycle *)value;
- (void)addHas:(NSSet *)values;
- (void)removeHas:(NSSet *)values;

@end
